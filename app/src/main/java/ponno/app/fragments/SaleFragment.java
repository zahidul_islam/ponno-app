package ponno.app.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.adapters.ProductSaleAdapter;
import ponno.app.model.Product;
import ponno.app.model.Sale;
import ponno.app.presenter.SalePresenter;
import ponno.app.utils.ApplicationUtils;
import ponno.app.utils.SalesUtils;
import ponno.app.utils.SwipeListenerImpl;
import ponno.app.view.SaleFragmentView;

public class SaleFragment extends Fragment implements SaleFragmentView {
    private Context context;

    @BindView(R.id.ll_products_list)
    LinearLayout ll_products_list;
    @BindView(R.id.rv_products)
    RecyclerView rv_products;
    @BindView(R.id.tv_invoice_number)
    TextView tv_invoice_number;
    @BindView(R.id.tv_total_cost)
    TextView tv_total_cost;
    @BindView(R.id.btn_complete_sale)
    Button btn_complete_sale;
    @BindView(R.id.btn_cancel_sale)
    Button btn_cancel_sale;

    private SwipeListenerImpl swipeListener;

    private ProductSaleAdapter productSaleAdapter;
    private ArrayList<Product> saleProducts;
    private Product selectedProduct;
    private double total;
    private String invoice_ID;

    private SalePresenter salePresenter;
    private SalesUtils salesUtils;

    public SaleFragment() {
        // Required empty public constructor
    }

    public static SaleFragment newInstance(Product product, SalePresenter salePresenter) {
        SaleFragment fragment = new SaleFragment();
        fragment.initializePresenter(product, salePresenter);
        return fragment;
    }

    private void initializePresenter(Product product, SalePresenter salePresenter) {
        this.selectedProduct = product;
        this.salePresenter = salePresenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        salesUtils = new SalesUtils(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sale, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        tv_invoice_number.setText(generateInvoiceID());
        setUpRecycler();
        if (selectedProduct != null)
            openQuantityAndPriceSelectionDialog(selectedProduct);
        btn_complete_sale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
                String date = simpleDateTimeFormat.format(Calendar.getInstance().getTime());
                salePresenter.completeSale(new Sale(invoice_ID, saleProducts, total, getString(R.string.sale_on_process), date));
            }
        });
        btn_cancel_sale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salePresenter.removeInvoice();
            }
        });
    }

    private String generateInvoiceID() {
        invoice_ID = getString(R.string.invoice_no) + " " + System.currentTimeMillis();
        return invoice_ID;
    }

    private void setUpRecycler() {
        rv_products.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_products.setLayoutManager(linearLayoutManager);
        swipeListener = new SwipeListenerImpl(context, this, rv_products);
        swipeListener.addSwipeListener();
        swipeListener.setUpAnimationDecoratorHelper();
    }

    @Override
    public void calculateAndSetTotalCost() {
        total = 0.0;
        for (Product product :
                saleProducts) {
            double productTotal = ApplicationUtils.convertToInt(product.getQuantity()) * ApplicationUtils.convertToDouble(product.getUnit_price());
            total += productTotal;
        }
        tv_total_cost.setText(ApplicationUtils.currencyFormat(total));
    }

    @Override
    public void openQuantityAndPriceSelectionDialog(Product product) {
//        QuantityPriceDialogFragment quantityPriceDialogFragment = QuantityPriceDialogFragment.newInstance(product, this);
//        quantityPriceDialogFragment.setCancelable(true);
//        quantityPriceDialogFragment.show(getChildFragmentManager(), QuantityPriceDialogFragment.class.getSimpleName());
    }

    @Override
    public void addProduct(Product product) {
        if (saleProducts == null) {
            saleProducts = new ArrayList<>();
            saleProducts.add(product);
        } else {
            boolean isAdded = false;
            for (Product aProduct :
                    saleProducts) {
                if (aProduct.getId().equalsIgnoreCase(product.getId())) {
                    aProduct.setQuantity(product.getQuantity());
                    aProduct.setUnit_price(product.getUnit_price());
                    isAdded = true;
                    break;
                }
            }
            if (!isAdded)
                saleProducts.add(product);
        }
        if (productSaleAdapter != null) {
            productSaleAdapter.notifyDataSetChanged();
        } else {
//            productSaleAdapter = new ProductSaleAdapter(saleProducts, context, this);
//            rv_products.setAdapter(productSaleAdapter);
        }
        calculateAndSetTotalCost();
    }

    @Override
    public void removeInvoice() {
        salePresenter.removeInvoice();
    }

    @Override
    public void onSwipeRight(int position) {

    }

    @Override
    public void onSwipeLeft(int position) {
        productSaleAdapter.pendingRemoval(position);
    }
}
