package ponno.app.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.adapters.DueBookAdapter;
import ponno.app.model.DueBook;
import ponno.app.model.Sale;
import ponno.app.presenter.HomePresenter;
import ponno.app.utils.ApplicationUtils;
import ponno.app.utils.SalesUtils;

public class DueBookFragment extends Fragment {
    private Context context;

    @BindView(R.id.ll_due_info)
    LinearLayout ll_due_info;
    @BindView(R.id.rv_due_book)
    RecyclerView rv_dues;
    @BindView(R.id.tv_number_of_dues)
    TextView tv_number_of_dues;
    @BindView(R.id.tv_total_due)
    TextView tv_total_due;

    private ArrayList<DueBook> dueBooks;
    private HomePresenter homePresenter;

    public DueBookFragment() {
        // Required empty public constructor
    }

    public static DueBookFragment newInstance(ArrayList<DueBook> dueBooks, HomePresenter homePresenter) {
        DueBookFragment fragment = new DueBookFragment();
        fragment.initializePresenter(dueBooks, homePresenter);
        return fragment;
    }

    private void initializePresenter(ArrayList<DueBook> dueBooks, HomePresenter homePresenter) {
        this.dueBooks = dueBooks;
        this.homePresenter = homePresenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_due_book, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        rv_dues.setHasFixedSize(true);
        rv_dues.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        tv_number_of_dues.setText(getString(R.string.number_of_dues) + " " + ApplicationUtils.getBengaliNumber(dueBooks.size() + ""));
        tv_total_due.setText(getString(R.string.total_due)+" "+getTotalDue());
        rv_dues.setAdapter(new DueBookAdapter(dueBooks, context, homePresenter));
    }

    private String getTotalDue() {
        double totalDue = 0.0;
        for (DueBook dueBook :
                dueBooks) {
            totalDue += dueBook.getPaymentDue();
        }
        return ApplicationUtils.currencyFormat(totalDue);
    }

}
