package ponno.app.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.InvoiceActivity;
import ponno.app.R;
import ponno.app.adapters.SaleProductsAdapter;
import ponno.app.data.local.DatabaseHelper;
import ponno.app.model.Sale;
import ponno.app.presenter.HomePresenter;
import ponno.app.utils.ApplicationUtils;
import ponno.app.utils.SalesUtils;

import static android.app.Activity.RESULT_OK;
import static ponno.app.SaleActivity.INVOICE_ACTIVITY_RESULT_CODE;

public class SaleDetailsFragment extends Fragment {
    private Context context;

    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.tv_invoice_number)
    TextView tv_invoice_number;
    @BindView(R.id.tv_total_sale)
    TextView tv_total_sale;
    @BindView(R.id.tv_cash)
    TextView tv_cash;
    @BindView(R.id.tv_discount)
    TextView tv_discout;
    @BindView(R.id.tv_cash_back)
    TextView tv_cash_back;
    @BindView(R.id.rv_products)
    RecyclerView rv_products;
    @BindView(R.id.ib_edit_due)
    ImageButton ib_edit_due;

    private Sale sale;

    private HomePresenter homePresenter;
    private DatabaseHelper databaseHelper;

    public SaleDetailsFragment() {
        // Required empty public constructor
    }

    public static SaleDetailsFragment newInstance(Sale aSale, HomePresenter homePresenter) {
        SaleDetailsFragment fragment = new SaleDetailsFragment();
        fragment.initializePresenter(aSale, homePresenter);
        return fragment;
    }

    private void initializePresenter(Sale aSale, HomePresenter homePresenter) {
        this.sale = aSale;
        this.homePresenter = homePresenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        databaseHelper = DatabaseHelper.getInstance(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sale_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setFragmentData();

        ib_edit_due.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nIntent = new Intent(getActivity(), InvoiceActivity.class);
                nIntent.putExtra(InvoiceActivity.INVOICE_ID, sale.getInvoiceID());
                startActivityForResult(nIntent, INVOICE_ACTIVITY_RESULT_CODE);
            }
        });
    }

    private void setFragmentData() {
        tv_invoice_number.setText(sale.getInvoiceID());
        tv_date.setText(getString(R.string.date) + ": " + ApplicationUtils.getDateInBengali(sale.getDate()));
        tv_total_sale.setText(getString(R.string.in_total) + ": " + ApplicationUtils.currencyFormat(sale.getTotalCost()));
        tv_cash.setText(getString(R.string.cash) + ": " + ApplicationUtils.currencyFormat(sale.getCashPaid()));
        String discount = sale.getDiscount() == null ? "০৳" : sale.getDiscount();
        tv_discout.setText(getString(R.string.discount_) + ": " + discount);
        tv_cash_back.setText(getString(R.string.cash_back) + ": " + ApplicationUtils.currencyFormat(sale.getCashBack()));
        setUpRecycler();
        if(sale.getStatus().equalsIgnoreCase(getString(R.string.sale_on_due)))
            ib_edit_due.setVisibility(View.VISIBLE);
        else
            ib_edit_due.setVisibility(View.GONE);
    }

    private void setUpRecycler() {
        rv_products.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_products.setLayoutManager(linearLayoutManager);
        SaleProductsAdapter saleProductsAdapter = new SaleProductsAdapter(sale.getProducts(), context);
        rv_products.setAdapter(saleProductsAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == INVOICE_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                Log.e("Sale Change","Sale Changes Successfully "+sale.getPaymentDue());
                sale = databaseHelper.getSale(sale.getInvoiceID());
                if(sale.getStatus().equalsIgnoreCase(getString(R.string.sale_completed))){
                    homePresenter.openDueBook();
                }else{
                    setFragmentData();
                }

            }
        }
    }
}
