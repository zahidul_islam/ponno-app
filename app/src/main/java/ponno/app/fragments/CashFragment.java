package ponno.app.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ponno.app.R;
import ponno.app.model.Sale;
import ponno.app.presenter.InvoicePresenter;
import ponno.app.utils.ApplicationUtils;
import ponno.app.view.CommonFragmentView;


public class CashFragment extends Fragment implements CommonFragmentView {
    @BindView(R.id.tiet_discount)
    TextInputEditText tiet_discount;
    @BindView(R.id.spinner_discount)
    Spinner spinner_discount;
    @BindView(R.id.tiet_cash)
    TextInputEditText tiet_cash;
    @BindView(R.id.tiet_back)
    TextInputEditText tiet_back;

    private Unbinder unbinder;
    private Sale sale;

    private Context context;
    private InvoicePresenter invoicePresenter;

    private DecimalFormat decimalFormat = new DecimalFormat("#.#");

    public CashFragment() {
        // Required empty public constructor
    }

    public static CashFragment newInstance(Sale sale, InvoicePresenter invoicePresenter) {
        CashFragment fragment = new CashFragment();
        fragment.initialize(sale, invoicePresenter);
        return fragment;
    }

    public void initialize(Sale sale, InvoicePresenter invoicePresenter) {
        this.sale = sale;
        this.invoicePresenter = invoicePresenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context = getActivity();
        return inflater.inflate(R.layout.fragment_cash, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        initUI(view);
        setListeners();
    }

    @Override
    public void initUI(View view) {
        if(sale.getDiscountedCost()>0){
            tiet_discount.setText(sale.getDiscount());
            tiet_discount.setEnabled(false);
            tiet_discount.setClickable(false);
            spinner_discount.setVisibility(View.GONE);
        }else {
            tiet_discount.setEnabled(true);
            tiet_discount.setClickable(true);
            spinner_discount.setVisibility(View.VISIBLE);
            spinner_discount.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.discount_types)));
        }
        tiet_back.setFocusable(false);
        tiet_back.setClickable(false);
    }

    @OnClick(R.id.btn_save)
    public void saveInvoice() {
        invoicePresenter.completeInvoice(tiet_cash.getText().toString(), tiet_back.getText().toString());
    }

    @Override
    public void setListeners() {
        tiet_cash.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    invoicePresenter.updateSaleInfo(ApplicationUtils.convertToDouble(s.toString()));
//                    setBack(ApplicationUtils.convertToDouble(s.toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tiet_discount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    invoicePresenter.setDiscount(ApplicationUtils.convertToInt(s.toString()), spinner_discount.getSelectedItem().toString());
                } else {
                    invoicePresenter.setDiscount(0, spinner_discount.getSelectedItem().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        spinner_discount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!TextUtils.isEmpty(tiet_discount.getText())) {
                    int discount = ApplicationUtils.convertToInt(tiet_discount.getText().toString());
                    invoicePresenter.setDiscount(discount, parent.getItemAtPosition(position).toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setBack(double cash) {
        double back;
        sale.setPaymentDue(0);
        if (sale.getCashPaid() > 0)
            sale.setCashPaid(sale.getCashPaid() + cash);
        else
            sale.setCashPaid(cash);
        if (TextUtils.isEmpty(tiet_discount.getText())) {
            back = cash > sale.getTotalCost() ? cash - sale.getTotalCost() : 0;
            if (back > 0)
                sale.setCashBack(back);
            else
                sale.setPaymentDue(sale.getTotalCost() - sale.getCashPaid());
        } else {
            back = cash > sale.getDiscountedCost() ? cash - sale.getDiscountedCost() : 0;
            if (back > 0)
                sale.setCashBack(back);
            else
                sale.setPaymentDue(sale.getDiscountedCost() - sale.getCashPaid());
        }
        tiet_back.setText(decimalFormat.format(back) + " " + getString(R.string.currency));
//        invoicePresenter.updateSale(sale);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void updateCashback(String cashback) {
        tiet_back.setText(cashback);
    }

    public void reviewSaleInfo() {
        if (!TextUtils.isEmpty(tiet_cash.getText()))
            invoicePresenter.updateSaleInfo(ApplicationUtils.convertToDouble(tiet_cash.getText().toString()));
        else
            invoicePresenter.updateSaleInfo(0);
    }
}
