package ponno.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.data.local.DatabaseHelper;
import ponno.app.model.Product;
import ponno.app.presenter.SalePresenter;
import ponno.app.presenter.SalePresenterImpl;
import ponno.app.utils.ApplicationUtils;
import ponno.app.view.QuantityPriceView;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuantityPriceDialogFragment extends DialogFragment implements QuantityPriceView {

    private Context context;
    private DatabaseHelper databaseHelper;

    @BindView(R.id.et_quantity)
    EditText et_quantity;
    @BindView(R.id.et_unit_price)
    EditText et_unit_price;
    @BindView(R.id.tv_quantity_label)
    TextView tv_quantity_label;
    @BindView(R.id.tv_unit_price_label)
    TextView tv_unit_price_label;
    @BindView(R.id.tv_confirm)
    TextView tv_confirm;

    private Product product;

    private InputMethodManager imm;

    private SalePresenter salePresenter;

    public QuantityPriceDialogFragment() {
        // Required empty public constructor
    }

    public void initializer(Product product, SalePresenter salePresenter) {
        this.product = product;
        this.salePresenter = this.salePresenter;
    }

    public static QuantityPriceDialogFragment newInstance(Product product, SalePresenterImpl salePresenter) {
        QuantityPriceDialogFragment quantityPriceDialogFragment = new QuantityPriceDialogFragment();
        quantityPriceDialogFragment.initializer(product, this.salePresenter);
        return quantityPriceDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context = getActivity();
        databaseHelper = DatabaseHelper.getInstance(context);
        imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        return inflater.inflate(R.layout.fragment_quantity_price_dialog, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        if (product != null) {
            initUI(view);
            setListeners();
        } else {
            dismiss();
        }

    }

    @Override
    public void initUI(View view) {
        et_quantity.requestFocus();
//        imm.showSoftInputFromInputMethod(view.getWindowToken(), InputMethodManager.SHOW_FORCED);
        et_unit_price.setHint(product.getUnit_price());
        if (product.getQuantity() == null) {
            et_quantity.setHint("10");
        } else {
            et_quantity.setHint(product.getQuantity());
        }
    }

    @Override
    public void setListeners() {
//        et_quantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean hasFocus) {
//                if (hasFocus) {
//                    tv_quantity_label.setTextColor(getResources().getColor(R.color.green));
////                    ApplicationUtils.setTextColor(tv_quantity_label, context, R.color.green);
//                } else {
//                    tv_quantity_label.setTextColor(getResources().getColor(R.color.gray));
////                    ApplicationUtils.setTextColor(tv_quantity_label, context, R.color.gray);
//                }
//            }
//        });
//        et_unit_price.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean hasFocus) {
//                if (hasFocus) {
//                    tv_unit_price_label.setTextColor(getResources().getColor(R.color.green));
////                    ApplicationUtils.setTextColor(tv_unit_price_label, context, R.color.green);
//                } else {
//                    tv_unit_price_label.setTextColor(getResources().getColor(R.color.gray));
////                    ApplicationUtils.setTextColor(tv_unit_price_label, context, R.color.gray);
//                }
//            }
//        });
        et_unit_price.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.hasFocus()) {
                    tv_unit_price_label.setTextColor(getResources().getColor(R.color.green));
//                    ApplicationUtils.setTextColor(tv_unit_price_label, context, R.color.green);
                } else {
                    tv_unit_price_label.setTextColor(getResources().getColor(R.color.gray));
//                    ApplicationUtils.setTextColor(tv_unit_price_label, context, R.color.gray);
                }
                return false;
            }
        });
        et_quantity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.hasFocus()) {
                    tv_quantity_label.setTextColor(getResources().getColor(R.color.green));
//                    ApplicationUtils.setTextColor(tv_unit_price_label, context, R.color.green);
                } else {
                    tv_quantity_label.setTextColor(getResources().getColor(R.color.gray));
//                    ApplicationUtils.setTextColor(tv_unit_price_label, context, R.color.gray);
                }
                return false;
            }
        });
        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String quantity = TextUtils.isEmpty(et_quantity.getText()) ? et_quantity.getHint().toString() : et_quantity.getText().toString();
                product.setQuantity(quantity);
                checkUnitPrice();
                salePresenter.addProduct(product);
                dismiss();
            }
        });
        et_unit_price.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    String quantity = TextUtils.isEmpty(et_quantity.getText()) ? et_quantity.getHint().toString() : et_quantity.getText().toString();
                    product.setQuantity(quantity);
                    checkUnitPrice();
                    salePresenter.addProduct(product);
                    dismiss();
                    return true;
                }
                return false;
            }
        });
    }

    private void checkUnitPrice() {
        String unit_price = TextUtils.isEmpty(et_unit_price.getText()) ? et_unit_price.getHint().toString() : et_unit_price.getText().toString();
        double unitPrice = ApplicationUtils.convertToDouble(unit_price);
        double productUnitPrice = ApplicationUtils.convertToDouble(product.getUnit_price());
        double difference = productUnitPrice - unitPrice;
        if (difference != 0.0) {
            DecimalFormat decimalFormat = new DecimalFormat(".##");
            product.setUnit_price(decimalFormat.format(unitPrice));
            databaseHelper.updateProductTable(product);
        }
    }
//
//    @Override
//    public void dismiss() {
//        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
//        super.dismiss();
//    }
//
//    @Override
//    public void onCancel(DialogInterface dialog) {
//        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
//        super.onCancel(dialog);
//    }
}
