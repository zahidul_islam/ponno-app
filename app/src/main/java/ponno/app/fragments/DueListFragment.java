package ponno.app.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.adapters.DueBookAdapter;
import ponno.app.adapters.SalesAdapter;
import ponno.app.model.DueBook;
import ponno.app.model.Sale;
import ponno.app.presenter.HomePresenter;
import ponno.app.utils.ApplicationUtils;

public class DueListFragment extends Fragment {
    private Context context;

    @BindView(R.id.ll_due_info)
    LinearLayout ll_due_info;
    @BindView(R.id.rv_due_book)
    RecyclerView rv_dues;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_number_of_dues)
    TextView tv_number_of_dues;
    @BindView(R.id.tv_total_due)
    TextView tv_total_due;

    private ArrayList<Sale> salesOnDue;
    private HomePresenter homePresenter;

    public DueListFragment() {
        // Required empty public constructor
    }

    public static DueListFragment newInstance(ArrayList<Sale> salesOnDue, HomePresenter homePresenter) {
        DueListFragment fragment = new DueListFragment();
        fragment.initializePresenter(salesOnDue, homePresenter);
        return fragment;
    }

    private void initializePresenter(ArrayList<Sale> salesOnDue, HomePresenter homePresenter) {
        this.salesOnDue = salesOnDue;
        this.homePresenter = homePresenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_due_book, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        rv_dues.setHasFixedSize(true);
        rv_dues.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        tv_title.setText(processMobileNumber(salesOnDue.get(0).getCustomerMobileNumber()));
        tv_number_of_dues.setText(getString(R.string.number_of_dues) + " " + ApplicationUtils.getBengaliNumber(salesOnDue.size() + ""));
        tv_total_due.setText(getString(R.string.total_due)+" "+getTotalDue());
        rv_dues.setAdapter(new SalesAdapter(salesOnDue, context, homePresenter));
    }

    private String processMobileNumber(String mobile) {
        if(mobile!=null && !TextUtils.isEmpty(mobile)){
            if(mobile.contains("+88")||mobile.contains("0088")){
                return mobile;
            }else{
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a", ApplicationUtils.getBangleLocale());
                    return dateFormat.format(Long.parseLong(mobile));
                }catch (Exception exception){
                    return context.getString(R.string.anonymous);
                }
            }
        }else {
            return context.getString(R.string.anonymous);
        }
    }

    private String getTotalDue() {
        double totalDue = 0.0;
        for (Sale sale :
                salesOnDue) {
            totalDue += sale.getPaymentDue();
        }
        return ApplicationUtils.currencyFormat(totalDue);
    }

}
