package ponno.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.presenter.HomePresenter;
import ponno.app.utils.ApplicationUtils;
import ponno.app.utils.SalesUtils;
import ponno.app.view.HomeView;

import static ponno.app.utils.SalesUtils.A_MONTH;
import static ponno.app.utils.SalesUtils.SEVEN_DAY;


public class SaleInfoFragment extends Fragment {
    private Context context;
    private SalesUtils salesUtils;

    private HomePresenter homePresenter;
    private HomeView homeView;

    @BindView(R.id.tv_today_sale_label)
    TextView tv_today_sale_label;
    @BindView(R.id.tv_total_sale_today)
    TextView tv_total_sale_today;
    @BindView(R.id.tv_sale_this_week)
    TextView tv_sale_this_week;
    @BindView(R.id.tv_sale_this_month)
    TextView tv_sale_this_month;
    @BindView(R.id.cv_sale_monthly)
    CardView cv_sale_monthly;
    @BindView(R.id.cv_sale_weekly)
    CardView cv_sale_weekly;

    private String saleToday, saleThisWeek, saleThisMonth;

    private SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());

    public SaleInfoFragment() {
        // Required empty public constructor
    }

    public static SaleInfoFragment newInstance(HomePresenter homePresenter) {
        SaleInfoFragment fragment = new SaleInfoFragment();
        fragment.initializePresenter(homePresenter);
        return fragment;
    }

    private void initializePresenter(HomePresenter homePresenter) {
        this.homePresenter = homePresenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        homeView = (HomeView) getActivity();
        salesUtils = new SalesUtils(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sale_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (homeView != null) {
            homeView.showProgress(getString(R.string.loading_progress));
            new Thread(new Runnable() {
                @Override
                public void run() {
                    saleToday = ApplicationUtils.currencyFormat(salesUtils.getTotalSaleDuring(ApplicationUtils.getStartingDateTime(simpleDateTimeFormat.format(System.currentTimeMillis())), ApplicationUtils.getEndingDateTime(simpleDateTimeFormat.format(System.currentTimeMillis()))));
                    saleThisWeek = ApplicationUtils.currencyFormat(salesUtils.getTotalSaleDuring(simpleDateTimeFormat.format(System.currentTimeMillis() - SEVEN_DAY), simpleDateTimeFormat.format(System.currentTimeMillis())));
                    saleThisMonth = ApplicationUtils.currencyFormat(salesUtils.getTotalSaleDuring(simpleDateTimeFormat.format(System.currentTimeMillis() - A_MONTH), simpleDateTimeFormat.format(System.currentTimeMillis())));
                    if (getActivity() != null && isAdded())
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv_total_sale_today.setText(saleToday);
                                tv_sale_this_week.setText(saleThisWeek);
                                tv_sale_this_month.setText(saleThisMonth);
                                homeView.hideProgress();
                            }
                        });
                }
            }).start();
        }
    }

    private void setListeners() {
        tv_total_sale_today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.showSaleAt(simpleDateTimeFormat.format(System.currentTimeMillis()));
            }
        });
        tv_today_sale_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.showSaleAt(simpleDateTimeFormat.format(System.currentTimeMillis()));
            }
        });
        cv_sale_weekly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.showSalesDuring(simpleDateTimeFormat.format(System.currentTimeMillis() - SEVEN_DAY), simpleDateTimeFormat.format(System.currentTimeMillis()), getString(R.string.sale_last_week));
            }
        });
        cv_sale_monthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.showSalesDuring(simpleDateTimeFormat.format(System.currentTimeMillis() - A_MONTH), simpleDateTimeFormat.format(System.currentTimeMillis()), getString(R.string.sale_last_month));
            }
        });
    }
}
