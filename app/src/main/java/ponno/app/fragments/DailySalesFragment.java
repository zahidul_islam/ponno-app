package ponno.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.adapters.DailySalesAdapter;
import ponno.app.model.DailySale;
import ponno.app.presenter.HomePresenter;
import ponno.app.utils.ApplicationUtils;
import ponno.app.utils.SalesUtils;

public class DailySalesFragment extends Fragment {

    @BindView(R.id.tv_date_label)
    TextView tv_date_label;
    @BindView(R.id.tv_number_of_invoice)
    TextView tv_number_of_invoice;
    @BindView(R.id.tv_total_sale)
    TextView tv_total_sale;
    @BindView(R.id.rv_sales)
    RecyclerView rv_sales;

    private Context context;

    private HomePresenter homePresenter;
    private ArrayList<DailySale> dailySales;
    private String tagLine;

    public static DailySalesFragment newInstance(HomePresenter homePresenter, String tagLine, ArrayList<DailySale> dailySales) {
        DailySalesFragment fragment = new DailySalesFragment();
        fragment.initializeSalesPresenter(homePresenter, tagLine, dailySales);
        return fragment;
    }

    private void initializeSalesPresenter(HomePresenter homePresenter, String tagLine, ArrayList<DailySale> dailySales) {
        this.homePresenter = homePresenter;
        this.tagLine = tagLine;
        this.dailySales = dailySales;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sales, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        tv_date_label.setText(tagLine);
        tv_number_of_invoice.setText(getString(R.string.number_of_invoice) + " " + ApplicationUtils.getBengaliNumber(countSales()+""));
        tv_total_sale.setText(getString(R.string.total_sale_) + " " + ApplicationUtils.currencyFormat(SalesUtils.getTotalSaleDuring(dailySales)));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_sales.setLayoutManager(linearLayoutManager);
//        Collections.reverse(dailySales);
        if (dailySales != null && dailySales.size() > 0) {
            DailySalesAdapter dailySalesAdapter = new DailySalesAdapter(dailySales, context, homePresenter);
            rv_sales.setAdapter(dailySalesAdapter);
        }
    }

    private int countSales() {
        int counter = 0;
        for (DailySale dailySale :
                dailySales) {
            counter += dailySale.getNumber_of_invoice();
        }
        return counter;
    }
}
