package ponno.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.adapters.SalesAdapter;
import ponno.app.model.Sale;
import ponno.app.presenter.HomePresenter;
import ponno.app.utils.ApplicationUtils;
import ponno.app.utils.SalesUtils;

public class SalesFragment extends Fragment {

    @BindView(R.id.tv_date_label)
    TextView tv_date_label;
    @BindView(R.id.tv_number_of_invoice)
    TextView tv_number_of_invoice;
    @BindView(R.id.tv_total_sale)
    TextView tv_total_sale;
    @BindView(R.id.rv_sales)
    RecyclerView rv_sales;

    private Context context;
    private SalesUtils salesUtils;

    private HomePresenter homePresenter;
    private ArrayList<Sale> sales;
    private String tagLine;

    public static SalesFragment newInstance(HomePresenter homePresenter, String tagLine, ArrayList<Sale> sales) {
        SalesFragment fragment = new SalesFragment();
        fragment.initializeSalesPresenter(homePresenter, tagLine, sales);
        return fragment;
    }

    private void initializeSalesPresenter(HomePresenter homePresenter, String tagLine, ArrayList<Sale> sales) {
        this.homePresenter = homePresenter;
        this.tagLine = tagLine;
        this.sales = sales;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sales, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        tv_date_label.setText(tagLine);
        tv_number_of_invoice.setText(getString(R.string.number_of_invoice) + " " + ApplicationUtils.getBengaliNumber(sales.size() + ""));
        tv_total_sale.setText(getString(R.string.total_sale_) + " " + ApplicationUtils.currencyFormat(SalesUtils.getTotalSale(sales)));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_sales.setLayoutManager(linearLayoutManager);
        if (sales != null && sales.size() > 0) {
            rv_sales.setAdapter(new SalesAdapter(sales, context, homePresenter));
        }
    }

    public void updateSales(ArrayList<Sale> sales) {
        if (sales != null && sales.size() > 0) {
            rv_sales.setAdapter(new SalesAdapter(sales, context, homePresenter));
        }
    }
}
