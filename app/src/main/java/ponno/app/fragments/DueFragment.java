package ponno.app.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.internal.cache.DiskLruCache;
import ponno.app.R;
import ponno.app.customcontrols.InstantAutoComplete;
import ponno.app.model.DueBook;
import ponno.app.model.Sale;
import ponno.app.presenter.InvoicePresenter;
import ponno.app.utils.ApplicationUtils;
import ponno.app.utils.SalesUtils;
import ponno.app.view.InvoiceView;

public class DueFragment extends Fragment {
    @BindView(R.id.autocomplete_name)
    InstantAutoComplete autocomplete_name;
    @BindView(R.id.autocomplete_mobile)
    InstantAutoComplete autocomplete_mobile;
    @BindView(R.id.tiet_due)
    TextInputEditText tiet_due;
    @BindView(R.id.til_name)
    TextInputLayout til_name;
    @BindView(R.id.til_mobile)
    TextInputLayout til_mobile;

    private Unbinder unbinder;

    private Context context;
    private Sale sale;
    private InvoicePresenter invoicePresenter;
    private InvoiceView invoiceView;
    private SalesUtils salesUtils;
    private ArrayList<DueBook> dueBooks;
    private ArrayList<String> dueNumbers, dueNames;

    public DueFragment() {
        // Required empty public constructor
    }

    public static DueFragment newInstance(Sale sale, InvoicePresenter invoicePresenter) {
        DueFragment fragment = new DueFragment();
        fragment.initialize(sale, invoicePresenter);
        return fragment;
    }

    public void initialize(Sale sale, InvoicePresenter invoicePresenter) {
        this.sale = sale;
        this.invoicePresenter = invoicePresenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        invoiceView = (InvoiceView) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_due, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        salesUtils = new SalesUtils(context);

        tiet_due.setText(ApplicationUtils.currencyFormat(sale.getPaymentDue()));
        if (!TextUtils.isEmpty(sale.getCustomerMobileNumber())) {
            autocomplete_mobile.setText(processMobileNumber(sale.getCustomerMobileNumber()));
            autocomplete_name.setText(sale.getCustomerName());
            autocomplete_name.setEnabled(false);
            autocomplete_mobile.setEnabled(false);
        } else {
            invoiceView.showProgress(context.getString(R.string.loading_progress));
            autocomplete_mobile.setSelection(2);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    dueBooks = salesUtils.getDueBooks();
                    dueNumbers = salesUtils.dueMobileNumbers(dueBooks);
                    dueNames = salesUtils.dueNames(dueBooks);
                    if (getActivity() != null)
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                autocomplete_mobile.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, dueNumbers));
                                autocomplete_name.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, dueNames));
                                invoiceView.hideProgress();
                            }
                        });
                }
            }).start();
        }

        autocomplete_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    til_mobile.setError(null);
                    autocomplete_name.setEnabled(true);
                    autocomplete_name.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        autocomplete_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    til_name.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        autocomplete_mobile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String mobileSelected = ApplicationUtils.addCountryPrefix((String) parent.getItemAtPosition(position));
                for (DueBook dueBook :
                        dueBooks) {
                    if (dueBook.getMobile().equalsIgnoreCase(mobileSelected)) {
                        autocomplete_name.setText(dueBook.getName());
                        autocomplete_name.setEnabled(false);
                        break;
                    }
                }
            }
        });
        autocomplete_mobile.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_NEXT||i==EditorInfo.IME_ACTION_DONE){
                    String mobileSelected = ApplicationUtils.addCountryPrefix(autocomplete_mobile.getText().toString());
                    boolean changed = false;
                    for (DueBook dueBook :
                            dueBooks) {
                        if (dueBook.getMobile().equalsIgnoreCase(mobileSelected)) {
                            autocomplete_name.setText(dueBook.getName());
                            autocomplete_name.setEnabled(false);
                            changed = true;
                            break;
                        }
                    }
                    if(!changed) {
                        autocomplete_name.setEnabled(true);
                        autocomplete_name.setText("");
                    }
                }
                return false;
            }
        });
    }

    private String processMobileNumber(String mobile) {
        if (mobile != null && !TextUtils.isEmpty(mobile)) {
            if (mobile.contains("88")) {
                return mobile;
            } else {
                return context.getString(R.string.anonymous);
            }
        } else {
            return context.getString(R.string.anonymous);
        }
    }

    @OnClick(R.id.btn_save)
    public void updateDueBook() {
        boolean isMobileValid = true;
        boolean isNameValid = true;
        isMobileValid = checkMobileNumber(autocomplete_mobile.getText().toString(), autocomplete_name.getText().toString());
        isNameValid = checkName(autocomplete_mobile.getText().toString(), autocomplete_name.getText().toString());
        if (isMobileValid && isNameValid)
            invoicePresenter.updateDueBook(autocomplete_name.getText().toString(), autocomplete_mobile.getText().toString());
//        if (!TextUtils.isEmpty(autocomplete_mobile.getText()) || !TextUtils.isEmpty(autocomplete_name.getText())) {
//            if ((TextUtils.isEmpty(autocomplete_name.getText()) && autocomplete_mobile.getText().length() == 2) ||
//                    (!TextUtils.isEmpty(autocomplete_name.getText()) && autocomplete_mobile.getText().length() == 11)) {
//                invoicePresenter.updateDueBook(autocomplete_name.getText().toString(), autocomplete_mobile.getText().toString());
//            } else if (TextUtils.isEmpty(autocomplete_name.getText()) && autocomplete_mobile.getText().length() == 11) {
//                til_name.setError(getString(R.string.name_error));
//            }
//        }
//        if (!TextUtils.isEmpty(autocomplete_name.getText()) && TextUtils.isEmpty(autocomplete_mobile.getText())) {
//            til_mobile.setError(getString(R.string.mobile_number_needed));
//        } else if (!TextUtils.isEmpty(autocomplete_mobile.getText())) {
//            if (TextUtils.isEmpty(autocomplete_name.getText()) && (autocomplete_mobile.getText().length() > 2))
//                til_name.setError(getString(R.string.name_error));
//            else
//                checkMobileNumber(autocomplete_mobile.getText().toString());
//        } else {
//            invoicePresenter.updateDueBook(autocomplete_name.getText().toString(), autocomplete_mobile.getText().toString());
//        }
    }

    private boolean checkName(String mobile, String name) {
        if (!TextUtils.isEmpty(sale.getCustomerMobileNumber())) {
            return true;
        } else {
            if (mobile.length() < 11 && TextUtils.isEmpty(name)) {
                return true;
            } else if (mobile.length() == 11 && !TextUtils.isEmpty(name)) {
                return true;
            } else {
                til_name.setError(getString(R.string.name_error));
                return false;
            }
        }
    }

    private boolean checkMobileNumber(String mobile, String name) {
        if (!TextUtils.isEmpty(sale.getCustomerMobileNumber())) {
            return true;
        } else {
            if (mobile.length() > 11) {
                til_mobile.setError(getString(R.string.invalid_mobile_number));
                return false;
            } else {
                if (!TextUtils.isEmpty(name) && mobile.length() < 11) {
                    til_mobile.setError(getString(R.string.mobile_number_needed));
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void updateSale(Sale sale) {
        this.sale = sale;
        if (tiet_due != null)
            tiet_due.setText(ApplicationUtils.currencyFormat(sale.getPaymentDue()));

    }
}
