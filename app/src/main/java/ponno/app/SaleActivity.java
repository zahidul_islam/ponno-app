package ponno.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.adapters.ProductAutoCompleteAdapter;
import ponno.app.adapters.ProductSaleAdapter;
import ponno.app.adapters.SuggestionsAdapter;
import ponno.app.customcontrols.InstantAutoComplete;
import ponno.app.model.Product;
import ponno.app.model.Sale;
import ponno.app.presenter.SalePresenter;
import ponno.app.presenter.SalePresenterImpl;
import ponno.app.utils.SwipeListenerImpl;
import ponno.app.view.SaleView;

import static android.view.View.INVISIBLE;

public class SaleActivity extends AppCompatActivity implements SaleView {
    private Context context;
    private SalePresenter salePresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_cancel_selection)
    ImageView iv_cancel_selection;
    @BindView(R.id.auto_complete_product)
    InstantAutoComplete auto_complete_product;
    @BindView(R.id.ll_suggestions)
    LinearLayout ll_suggestions;
    @BindView(R.id.tv_recent_sales_label)
    TextView tv_recent_sales_label;
    @BindView(R.id.tv_most_sales_label)
    TextView tv_most_sales_label;
    @BindView(R.id.rv_recent_sales)
    RecyclerView rv_recent_sales;
    @BindView(R.id.rv_most_sales)
    RecyclerView rv_most_sales;
    @BindView(R.id.ll_products_list)
    LinearLayout ll_products_list;
    @BindView(R.id.rv_products)
    RecyclerView rv_products;
    @BindView(R.id.tv_invoice_number)
    TextView tv_invoice_number;
    @BindView(R.id.tv_total_cost)
    TextView tv_total_cost;
    @BindView(R.id.btn_complete_sale)
    Button btn_complete_sale;
    @BindView(R.id.btn_cancel_sale)
    Button btn_cancel_sale;

    private SwipeListenerImpl swipeListener;

    private ProductSaleAdapter productSaleAdapter;
    private ArrayList<Product> saleProducts;
    private Product selectedProduct;
    private double total;
    private String invoice_ID;

    private ProgressDialog progressDialog;

    public static final int INVOICE_ACTIVITY_RESULT_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale);
        ButterKnife.bind(this);
        context = this;
        salePresenter = new SalePresenterImpl(context, this, getSupportFragmentManager());
    }

    @Override
    protected void onResume() {
        super.onResume();
        salePresenter.initializePresenter();
    }

    @Override
    public void initUI() {
        auto_complete_product.setThreshold(3);
        ll_suggestions.setVisibility(View.GONE);
        rv_recent_sales.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rv_most_sales.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        tv_invoice_number.setText(generateInvoiceID());
        setUpRecycler();
        if (selectedProduct != null)
            salePresenter.openQuantityAndPriceSelectionDialog(selectedProduct);
        btn_complete_sale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
                String date = simpleDateTimeFormat.format(Calendar.getInstance().getTime());
                salePresenter.completeSale(new Sale(invoice_ID, saleProducts, total, getString(R.string.sale_on_process), date));
            }
        });
        btn_cancel_sale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salePresenter.removeInvoice();
            }
        });
    }

    private String generateInvoiceID() {
        invoice_ID = getString(R.string.invoice_no) + " " + System.currentTimeMillis();
        return invoice_ID;
    }

    private void setUpRecycler() {
        rv_products.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_products.setLayoutManager(linearLayoutManager);
        swipeListener = new SwipeListenerImpl(context, this, rv_products);
        swipeListener.addSwipeListener();
        swipeListener.setUpAnimationDecoratorHelper();
    }

    @Override
    public void initActionbar() {
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setListeners() {
        iv_cancel_selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auto_complete_product.invalidate();
                auto_complete_product.setText("");
                showSuggestions(true);
            }
        });
        auto_complete_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSuggestions(true);
            }
        });
        auto_complete_product.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                if (s == null && s.length() == 0)
//                    checkSearchProgress(true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0 && iv_cancel_selection.getVisibility() == INVISIBLE) {
                    iv_cancel_selection.setVisibility(View.VISIBLE);
                } else if (s == null || s.length() == 0) {
                    iv_cancel_selection.setVisibility(INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        auto_complete_product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                salePresenter.productSelected((Product) parent.getItemAtPosition(position));
                auto_complete_product.setText("");
            }
        });
    }

    @Override
    public void setAutoCompleteAdapter(ProductAutoCompleteAdapter autoCompleteAdapter) {
        auto_complete_product.setAdapter(new ProductAutoCompleteAdapter(context, new ArrayList<Product>()));
    }

    @Override
    public void gotoInvoice(String invoiceNumber) {
        Intent nIntent = new Intent(this, InvoiceActivity.class);
        nIntent.putExtra(InvoiceActivity.INVOICE_ID, invoiceNumber);
        startActivityForResult(nIntent, INVOICE_ACTIVITY_RESULT_CODE);
    }

    @Override
    public void checkSearchProgress(boolean showProgress) {
        if (showProgress) {
            iv_cancel_selection.setImageResource(R.drawable.progress_animation);
        } else {
            iv_cancel_selection.setImageResource(R.drawable.ic_action_cancel_selector);
        }
    }

    @Override
    public void showSuggestions(boolean show) {
        if (show) {
            ll_suggestions.setVisibility(View.VISIBLE);
        } else {
            ll_suggestions.setVisibility(View.GONE);
        }
    }

    @Override
    public void setSuggestions(ArrayList<Product> recentSales, ArrayList<Product> mostPurchased) {
        ll_suggestions.setVisibility(View.VISIBLE);
        if (recentSales == null || recentSales.size() == 0) {
            tv_recent_sales_label.setVisibility(View.GONE);
            rv_recent_sales.setVisibility(View.GONE);
        } else {
            tv_recent_sales_label.setVisibility(View.VISIBLE);
            rv_recent_sales.setVisibility(View.VISIBLE);
            rv_recent_sales.setAdapter(new SuggestionsAdapter(recentSales, salePresenter));
        }
        if (mostPurchased == null || mostPurchased.size() == 0) {
            tv_most_sales_label.setVisibility(View.GONE);
            rv_most_sales.setVisibility(View.GONE);
        } else {
            tv_most_sales_label.setVisibility(View.VISIBLE);
            rv_most_sales.setVisibility(View.VISIBLE);
            rv_most_sales.setAdapter(new SuggestionsAdapter(mostPurchased, salePresenter));
        }
    }

    @Override
    public void setSaleAdapter(ProductSaleAdapter productSaleAdapter) {

    }

    @Override
    public void updateTotalCost(String totalCost) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INVOICE_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }

    @Override
    public void showProgress(final String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && context != null)
            progressDialog.dismiss();
    }

    @Override
    public void onSwipeRight(int position) {

    }

    @Override
    public void onSwipeLeft(int position) {

    }
}
