package ponno.app.data.utils;

import java.io.IOException;

import ponno.app.R;
import ponno.app.base.PonnoApplication;

/**
 * Created by Fahim Ahmed on 21-12-16.
 */

public class NoConnectivityException extends IOException {
    @Override
    public String getMessage() {
        return PonnoApplication.getApplication().getString(R.string.conn_failed);
    }
}
