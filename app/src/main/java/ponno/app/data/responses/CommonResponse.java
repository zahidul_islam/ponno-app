package ponno.app.data.responses;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public class CommonResponse {
    private String message;
    private int success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
