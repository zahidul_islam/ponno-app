package ponno.app.data.responses;

import java.util.ArrayList;
import java.util.List;

import ponno.app.model.Product;

/**
 * Created by iGeorge on 9/21/17.
 */

public class ProductResponseHelper {
    private ArrayList<Product> data;

    public ArrayList<Product> getData() {
        return data;
    }

    public void setData(ArrayList<Product> data) {
        this.data = data;
    }
}
