package ponno.app.data.responses;

/**
 * Created by iGeorge on 9/21/17.
 */

public class ProductResponse extends CommonResponse {
    private ProductResponseHelper products;

    public ProductResponseHelper getProducts() {
        return products;
    }

    public void setProducts(ProductResponseHelper products) {
        this.products = products;
    }
}
