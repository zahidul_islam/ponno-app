package ponno.app.data.local;

import android.content.ContentValues;

/**
 * Created by Zahidul_Islam_George on 24-October-2016.
 */
public class DatabaseHelperUtils {

    public static String getSelectQuery(String table_name, String[] select, String[] column_name, String[] values) {
        String query = "SELECT  ";
        if (select.length > 0) {
            query += select[0];
            query = setSelections(query, select);
        } else {
            query += "*";
        }
        query += " FROM " + table_name;
        if (column_name.length > 0) {
            query += " WHERE " + column_name[0] + " = '" + values[0] + "'";
            if (column_name.length > 1) {
                query = setArgument(query, column_name, values);
            }
        }
        query += ";";
        return query;
    }

    public static String getSelectDistinctQuery(String table_name, String select, String[] column_name, String[] values) {
        String query = "SELECT DISTINCT "+select;
        query += " FROM " + table_name;
        if (column_name.length > 0) {
            query += " WHERE " + column_name[0] + " = '" + values[0] + "'";
            if (column_name.length > 1) {
                query = setArgument(query, column_name, values);
            }
        }
        query += ";";
        return query;
    }

    private static String setArgument(String query, String[] column_name, String[] values) {
        for (int i = 1; i < column_name.length; i++) {
            query += " AND " + column_name[i] + " = '" + values[i]+"'";
        }
        return query;
    }

    private static String setSelections(String query, String[] select) {
        for (int i = 1; i < select.length; i++) {
            query += " ," + select[i];
        }
        return query;
    }

    public static ContentValues addStringContentValues(ContentValues values, String[] columns, String[] datas) {
        for (int i = 0; i < columns.length; i++) {
            values.put(columns[i], datas[i]);
        }
        return values;
    }

//    public static Item itemSetter(Cursor cursor){
//        Item item = new Item();
//        item.setId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_ID)));
//        item.setProduct_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_ID)));
//        item.setCategory(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_CATEGORY)));
//        item.setSub_category(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_SUB_CATEGORY)));
//        item.setName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_NAME)));
//        item.setPackage_size(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_PCKG_SIZE)));
//        item.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_DESCRIPTION)));
//        item.setManufacturer(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_MANUFACTURER)));
//        item.setSupplier(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_SUPPLIER)));
//        item.setCode(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_CODE)));
//        item.setPrice(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_PRICE)));
//        item.setIs_direct(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_DIRECT)));
//        item.setOrder_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_ORDER_DATE)));
//        item.setPaid(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_IS_PAID)));
//        item.setSelling_price(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_SELL_PRICE)));
//        item.setOriginal_price(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_ORIG_PRICE)));
//        item.setThreshold(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_THRESHOLD)));
//        item.setAmount(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PROD_AMOUNT)));
//        return item;
//    }

}
