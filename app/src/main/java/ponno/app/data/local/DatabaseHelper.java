package ponno.app.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import ponno.app.R;
import ponno.app.model.DueBook;
import ponno.app.model.Product;
import ponno.app.model.Sale;
import ponno.app.utils.ApplicationUtils;

/**
 * Created by wali on 5/16/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper sInstance;

    //  Database name
    static String DATABASE_NAME = "ponno_DB";
    SQLiteDatabase db;

    //  Table For Cart
    public static final String PRODUCT_TABLE = "product_table";
    public static final String SALE_TABLE = "sale_table";
    public static final String DUE_TABLE = "due_table";
//    public static final String PURCHASE_TABLE = "purchase_table";

    //  fields for Product table
    private static final String COLUMN_PROD_ID = "prod_id";
    private static final String COLUMN_CAT_ID = "prod_category_id";
    private static final String COLUMN_VENDOR_ID = "prod_vendor_id";
    private static final String COLUMN_MED_PCK_ID = "prod_medicine_package_id";
    private static final String COLUMN_MED_DOS_ID = "prod_medicine_dosage_id";
    private static final String COLUMN_MED_STRNGTH_ID = "prod_medicine_strength_id";
    private static final String COLUMN_MED_ID = "prod_medicine_id";
    private static final String COLUMN_CATEGORY = "prod_category";
    private static final String COLUMN_COM_CODE = "prod_company_code";
    private static final String COLUMN_COMPANY = "prod_company";
    private static final String COLUMN_DOS_CODE = "prod_dosage_code";
    private static final String COLUMN_DOSAGE = "prod_dosage";
    private static final String COLUMN_PCKGE_CODE = "prod_package_code";
    private static final String COLUMN_PCKGE_SIZE = "prod_package_size";
    private static final String COLUMN_STRENGTH = "prod_strength";
    private static final String COLUMN_GENERIC_NAME = "prod_generic_name";
    private static final String COLUMN_MED_NAME = "prod_medicine_name";
    private static final String COLUMN_DAR_NO = "prod_dar_no";
    private static final String COLUMN_UNIT_PRICE = "prod_unit_price";
    private static final String COLUMN_PURCHASE_COUNTER = "prod_purchase_counter";

    // fields for Account table
    private static final String COLUMN_SALE_INVOICE_ID = "sale_invoice_id";
    private static final String COLUMN_SALE_TOTAL_COST = "sale_total_cost";
    private static final String COLUMN_SALE_DATE = "sale_date";
    private static final String COLUMN_SALE_PRODUCTS = "sale_products";
    private static final String COLUMN_SALE_STATUS = "sale_status";
    private static final String COLUMN_SALE_DISCOUNT = "sale_discount";
    private static final String COLUMN_SALE_DISCOUNTED_COST = "sale_discounted_cost";
    private static final String COLUMN_SALE_CASH_PAID = "sale_cash_paid";
    private static final String COLUMN_SALE_CASH_BACK = "sale_cash_back";
    private static final String COLUMN_SALE_PAYMENT_DUE = "sale_payment_due";
    private static final String COLUMN_SALE_CUSTOMER_NAME = "sale_customer_name";
    private static final String COLUMN_SALE_CUSTOMER_MOBILE = "sale_customer_mobile";
    // fields for Due table
    private static final String COLUMN_DUE_AMOUNT = "due_amount";
    private static final String COLUMN_DUE_CUSTOMER_NAME = "due_customer_name";
    private static final String COLUMN_DUE_CUSTOMER_MOBILE = "due_customer_mobile";
    private static final String COLUMN_DUE_ID = "due_id";

    //  Table create
    private static final String
            PRODUCT_TABLE_CREATE = "CREATE TABLE " + PRODUCT_TABLE + "(" + COLUMN_PROD_ID
            + " INTEGER PRIMARY KEY autoincrement, " + COLUMN_CAT_ID
            + " TEXT, " + COLUMN_VENDOR_ID
            + " TEXT, " + COLUMN_MED_PCK_ID
            + " TEXT, " + COLUMN_MED_DOS_ID
            + " TEXT, " + COLUMN_MED_STRNGTH_ID
            + " TEXT, " + COLUMN_MED_ID
            + " TEXT, " + COLUMN_CATEGORY
            + " TEXT, " + COLUMN_COM_CODE
            + " TEXT, " + COLUMN_COMPANY
            + " TEXT, " + COLUMN_DOS_CODE
            + " TEXT, " + COLUMN_DOSAGE
            + " TEXT, " + COLUMN_PCKGE_CODE
            + " TEXT, " + COLUMN_PCKGE_SIZE
            + " TEXT, " + COLUMN_STRENGTH
            + " TEXT, " + COLUMN_GENERIC_NAME
            + " TEXT, " + COLUMN_MED_NAME
            + " TEXT, " + COLUMN_DAR_NO
            + " TEXT, " + COLUMN_UNIT_PRICE
            + " INTEGER, " + COLUMN_PURCHASE_COUNTER
            + " TEXT)";
    private static final String
            SALE_TABLE_CREATE = "CREATE TABLE " + SALE_TABLE + "(" + COLUMN_SALE_INVOICE_ID
            + " TEXT PRIMARY KEY, " + COLUMN_SALE_TOTAL_COST
            + " TEXT, " + COLUMN_SALE_DATE
            + " TEXT, " + COLUMN_SALE_PRODUCTS
            + " TEXT, " + COLUMN_SALE_STATUS
            + " TEXT, " + COLUMN_SALE_DISCOUNT
            + " TEXT, " + COLUMN_SALE_DISCOUNTED_COST
            + " TEXT, " + COLUMN_SALE_CASH_PAID
            + " TEXT, " + COLUMN_SALE_CASH_BACK
            + " TEXT, " + COLUMN_SALE_PAYMENT_DUE
            + " TEXT, " + COLUMN_SALE_CUSTOMER_NAME
            + " TEXT, " + COLUMN_SALE_CUSTOMER_MOBILE
            + " TEXT)";
    private static final String
            DUE_TABLE_CREATE = "CREATE TABLE " + DUE_TABLE + "(" + COLUMN_DUE_ID
            + " INTEGER PRIMARY KEY autoincrement, " + COLUMN_DUE_CUSTOMER_MOBILE
            + " TEXT, " + COLUMN_DUE_CUSTOMER_NAME
            + " TEXT, " + COLUMN_DUE_AMOUNT
            + " TEXT)";

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PRODUCT_TABLE_CREATE);
        db.execSQL(SALE_TABLE_CREATE);
        db.execSQL(DUE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query1 = "DROP TABLE IF EXIST " + PRODUCT_TABLE;
        String query2 = "DROP TABLE IF EXIST " + SALE_TABLE;
        String query3 = "DROP TABLE IF EXIST " + DUE_TABLE;
        db.execSQL(query1);
        db.execSQL(query2);
        db.execSQL(query3);
        this.onCreate(db);

    }

    public void insertDataFromCSV(Context context) {
        try {
            InputStream is = context.getAssets().open("drug_list_edited.csv");
            BufferedReader buffer = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String columns = COLUMN_COM_CODE + ", " + COLUMN_COMPANY + ", " + COLUMN_MED_NAME + ", " + COLUMN_GENERIC_NAME + ", " +
                    COLUMN_STRENGTH + ", " + COLUMN_DOSAGE + ", " + COLUMN_DAR_NO + ", " + COLUMN_UNIT_PRICE + ", " + COLUMN_PURCHASE_COUNTER;
            String str1 = "INSERT INTO " + PRODUCT_TABLE + " (" + columns + ") values(";
            db = this.getWritableDatabase();
            db.beginTransaction();
//            int index = 0;
            while (true) {
                if (buffer.readLine() == null) {
                    break;
                }
//                index++;
                String line = buffer.readLine();
                StringBuilder sb = new StringBuilder(str1);
                if (!TextUtils.isEmpty(line)) {
                    line = line.replace("\"", "").replace("'", "''");
                    try {
                        String[] str = line.split(",");
                        sb.append("'").append(str[0]).append("','");
                        sb.append(str[1]).append("','");
                        sb.append(str[2]).append("','");
                        sb.append(str[3]).append("','");
                        sb.append(str[4]).append("','");
                        sb.append(str[5]).append("','");
                        sb.append(str[6]).append("',0.0,0);");
//                    sb.append(str[7]).append("','");
//                    sb.append(str[8]).append("','");
//                    sb.append(str[9]).append("','");
//                    sb.append(str[10]).append("', 0);");
                        db.execSQL(sb.toString());
                    }catch (ArrayIndexOutOfBoundsException e){
//                        Log.e("IndexOutOfBound", "The Line >> "+line+" >> "+index);
                    }
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void insertProductInCart(Product product) {
//        deletePreviousData();
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_PROD_ID, product.getId());
            values.put(COLUMN_CAT_ID, product.getCategory_id());
            values.put(COLUMN_VENDOR_ID, product.getVendor_id());
            values.put(COLUMN_MED_PCK_ID, product.getMedicine_package_id());
            values.put(COLUMN_MED_DOS_ID, product.getMedicine_dosage_id());
            values.put(COLUMN_MED_STRNGTH_ID, product.getMedicine_strength_id());
            values.put(COLUMN_MED_ID, product.getMedicine_id());
            values.put(COLUMN_CATEGORY, product.getCategory());
            values.put(COLUMN_COM_CODE, product.getCompany_code());
            values.put(COLUMN_COMPANY, product.getCompany());
            values.put(COLUMN_DOS_CODE, product.getDosage_code());
            values.put(COLUMN_DOSAGE, product.getDosage());
            values.put(COLUMN_PCKGE_CODE, product.getPackage_code());
            values.put(COLUMN_PCKGE_SIZE, product.getPackage_size());
            values.put(COLUMN_STRENGTH, product.getStrength());
            values.put(COLUMN_GENERIC_NAME, product.getGeneric_name());
            values.put(COLUMN_MED_NAME, product.getMedicine_name());
            values.put(COLUMN_DAR_NO, product.getDar_no());
            values.put(COLUMN_UNIT_PRICE, product.getUnit_price());
            values.put(COLUMN_PURCHASE_COUNTER, product.getPurchase_count());
            db.insert(PRODUCT_TABLE, null, values);
//        Log.e("INFO", "One rowinserter " + count);

        } catch (Exception ignored) {
        }
    }

    public void updateProductTable(Product product) {
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_PROD_ID, product.getId());
            values.put(COLUMN_CAT_ID, product.getCategory_id());
            values.put(COLUMN_VENDOR_ID, product.getVendor_id());
            values.put(COLUMN_MED_PCK_ID, product.getMedicine_package_id());
            values.put(COLUMN_MED_DOS_ID, product.getMedicine_dosage_id());
            values.put(COLUMN_MED_STRNGTH_ID, product.getMedicine_strength_id());
            values.put(COLUMN_MED_ID, product.getMedicine_id());
            values.put(COLUMN_CATEGORY, product.getCategory());
            values.put(COLUMN_COM_CODE, product.getCompany_code());
            values.put(COLUMN_COMPANY, product.getCompany());
            values.put(COLUMN_DOS_CODE, product.getDosage_code());
            values.put(COLUMN_DOSAGE, product.getDosage());
            values.put(COLUMN_PCKGE_CODE, product.getPackage_code());
            values.put(COLUMN_PCKGE_SIZE, product.getPackage_size());
            values.put(COLUMN_STRENGTH, product.getStrength());
            values.put(COLUMN_GENERIC_NAME, product.getGeneric_name());
            values.put(COLUMN_MED_NAME, product.getMedicine_name());
            values.put(COLUMN_DAR_NO, product.getDar_no());
            values.put(COLUMN_UNIT_PRICE, product.getUnit_price());
            db.update(PRODUCT_TABLE, values,
                    COLUMN_PROD_ID + " = ?",
                    new String[]{product.getId()});

        } catch (Exception ignored) {
        }
    }

    //    public int getCartCount(String product_id) {
//        String selectQuery = "SELECT * FROM " + CART_TABLE + " WHERE " + COLUMN_PROD_ID + " =" + product_id;
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        int count = 0;
//        if (cursor.moveToFirst()) {
//            count = cursor.getInt(cursor.getColumnIndex(COLUMN_PROD_AMOUNT));
//        }
//
//        return count;
//    }
//
    public boolean checkProductsTable() {
        String selectQuery = "SELECT * FROM " + PRODUCT_TABLE;
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            int count = cursor.getCount();

            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ignored) {
            return true;
        }
    }
//
//    public int getCartCount() {
//        String selectQuery = "SELECT * FROM " + CART_TABLE;
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//        int count = cursor.getCount();
//
//        return count;
//    }

//    public ArrayList<Product> getProducts() {
//        ArrayList<Product> products = new ArrayList<>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + PRODUCT_TABLE;
//
//        db = getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Product product = new Product();
//                product.setId(cursor.getString(cursor.getColumnIndex(COLUMN_PROD_ID)));
//                product.setCategory_id(cursor.getString(cursor.getColumnIndex(COLUMN_CAT_ID)));
//                product.setVendor_id(cursor.getString(cursor.getColumnIndex(COLUMN_VENDOR_ID)));
//                product.setMedicine_package_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_PCK_ID)));
//                product.setMedicine_dosage_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_DOS_ID)));
//                product.setMedicine_strength_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_STRNGTH_ID)));
//                product.setMedicine_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_ID)));
//                product.setCategory(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY)));
//                product.setCompany_code(cursor.getString(cursor.getColumnIndex(COLUMN_COM_CODE)));
//                product.setCompany(cursor.getString(cursor.getColumnIndex(COLUMN_COMPANY)));
//                product.setDosage_code(cursor.getString(cursor.getColumnIndex(COLUMN_DOS_CODE)));
//                product.setDosage(cursor.getString(cursor.getColumnIndex(COLUMN_DOSAGE)));
//                product.setPackage_code(cursor.getString(cursor.getColumnIndex(COLUMN_PCKGE_CODE)));
//                product.setPackage_size(cursor.getString(cursor.getColumnIndex(COLUMN_PCKGE_SIZE)));
//                product.setStrength(cursor.getString(cursor.getColumnIndex(COLUMN_STRENGTH)));
//                product.setGeneric_name(cursor.getString(cursor.getColumnIndex(COLUMN_GENERIC_NAME)));
//                product.setMedicine_name(cursor.getString(cursor.getColumnIndex(COLUMN_MED_NAME)));
//                product.setDar_no(cursor.getString(cursor.getColumnIndex(COLUMN_DAR_NO)));
//                product.setUnit_price(cursor.getString(cursor.getColumnIndex(COLUMN_UNIT_PRICE)));
//
//                // Adding contact to list
//                products.add(product);
//            } while (cursor.moveToNext());
//        }
//
//        // return contact list
//        return products;
//    }

    /*select * from (
            SELECT 1 as relevance, * FROM wallpapers
WHERE tags LIKE '%New York%' OR name LIKE '%New York%')
order by relevance asc*/

    public ArrayList<Product> getProductsByKey(String key) {
        ArrayList<Product> products = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM (SELECT 5 AS RELEVANCE, * FROM " + PRODUCT_TABLE + " WHERE " +
                COLUMN_MED_NAME + " LIKE '" + key + "%') ORDER BY RELEVANCE DESC";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Product product = new Product();
                    product.setId(cursor.getString(cursor.getColumnIndex(COLUMN_PROD_ID)));
                    product.setCategory_id(cursor.getString(cursor.getColumnIndex(COLUMN_CAT_ID)));
                    product.setVendor_id(cursor.getString(cursor.getColumnIndex(COLUMN_VENDOR_ID)));
                    product.setMedicine_package_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_PCK_ID)));
                    product.setMedicine_dosage_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_DOS_ID)));
                    product.setMedicine_strength_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_STRNGTH_ID)));
                    product.setMedicine_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_ID)));
                    product.setCategory(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY)));
                    product.setCompany_code(cursor.getString(cursor.getColumnIndex(COLUMN_COM_CODE)));
                    product.setCompany(cursor.getString(cursor.getColumnIndex(COLUMN_COMPANY)));
                    product.setDosage_code(cursor.getString(cursor.getColumnIndex(COLUMN_DOS_CODE)));
                    product.setDosage(cursor.getString(cursor.getColumnIndex(COLUMN_DOSAGE)));
                    product.setPackage_code(cursor.getString(cursor.getColumnIndex(COLUMN_PCKGE_CODE)));
                    product.setPackage_size(cursor.getString(cursor.getColumnIndex(COLUMN_PCKGE_SIZE)));
                    product.setStrength(cursor.getString(cursor.getColumnIndex(COLUMN_STRENGTH)));
                    product.setGeneric_name(cursor.getString(cursor.getColumnIndex(COLUMN_GENERIC_NAME)));
                    product.setMedicine_name(cursor.getString(cursor.getColumnIndex(COLUMN_MED_NAME)));
                    product.setDar_no(cursor.getString(cursor.getColumnIndex(COLUMN_DAR_NO)));
                    product.setUnit_price(cursor.getString(cursor.getColumnIndex(COLUMN_UNIT_PRICE)));

                    // Adding contact to list
                    products.add(product);
                } while (cursor.moveToNext());
            }

        } catch (Exception ignored) {
        }
        // return contact list
        return products;
    }


    public Product getProduct(String product_id) {
        Product product = new Product();
        // Select All Query
        String selectQuery = "SELECT * FROM " + PRODUCT_TABLE + " WHERE " + COLUMN_PROD_ID + " = '" + product_id + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                product.setId(cursor.getString(cursor.getColumnIndex(COLUMN_PROD_ID)));
                product.setCategory_id(cursor.getString(cursor.getColumnIndex(COLUMN_CAT_ID)));
                product.setVendor_id(cursor.getString(cursor.getColumnIndex(COLUMN_VENDOR_ID)));
                product.setMedicine_package_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_PCK_ID)));
                product.setMedicine_dosage_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_DOS_ID)));
                product.setMedicine_strength_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_STRNGTH_ID)));
                product.setMedicine_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_ID)));
                product.setCategory(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY)));
                product.setCompany_code(cursor.getString(cursor.getColumnIndex(COLUMN_COM_CODE)));
                product.setCompany(cursor.getString(cursor.getColumnIndex(COLUMN_COMPANY)));
                product.setDosage_code(cursor.getString(cursor.getColumnIndex(COLUMN_DOS_CODE)));
                product.setDosage(cursor.getString(cursor.getColumnIndex(COLUMN_DOSAGE)));
                product.setPackage_code(cursor.getString(cursor.getColumnIndex(COLUMN_PCKGE_CODE)));
                product.setPackage_size(cursor.getString(cursor.getColumnIndex(COLUMN_PCKGE_SIZE)));
                product.setStrength(cursor.getString(cursor.getColumnIndex(COLUMN_STRENGTH)));
                product.setGeneric_name(cursor.getString(cursor.getColumnIndex(COLUMN_GENERIC_NAME)));
                product.setMedicine_name(cursor.getString(cursor.getColumnIndex(COLUMN_MED_NAME)));
                product.setDar_no(cursor.getString(cursor.getColumnIndex(COLUMN_DAR_NO)));
                product.setUnit_price(cursor.getString(cursor.getColumnIndex(COLUMN_UNIT_PRICE)));
                product.setPurchase_count(cursor.getInt(cursor.getColumnIndex(COLUMN_PURCHASE_COUNTER)));
            } else {
                product = null;
            }

        } catch (Exception ignored) {
        }
        // return contact list
        return product;
    }

    public void updatePurchaseCount(int counter, String product_id) {
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_PURCHASE_COUNTER, counter);
            db.update(PRODUCT_TABLE, values,
                    COLUMN_PROD_ID + " = ?",
                    new String[]{product_id});

        } catch (Exception ignored) {
        }
    }

    public ArrayList<String> getStrengths(String medicine_code) {
        ArrayList<String> strengths = new ArrayList<>();

        String selectQuery = "SELECT DISTINCT " + COLUMN_STRENGTH + " FROM " + PRODUCT_TABLE + " WHERE " + COLUMN_MED_NAME + " = '" + medicine_code + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    strengths.add(cursor.getString(cursor.getColumnIndex(COLUMN_STRENGTH)));
                } while (cursor.moveToNext());
            }

        } catch (Exception ignored) {
        }
        return strengths;
    }

    public ArrayList<String> getDosages(String medicine_code) {
        ArrayList<String> dosages = new ArrayList<>();

        String selectQuery = "SELECT DISTINCT " + COLUMN_DOSAGE + " FROM " + PRODUCT_TABLE + " WHERE " + COLUMN_MED_NAME + " = '" + medicine_code + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    dosages.add(cursor.getString(cursor.getColumnIndex(COLUMN_DOSAGE)));
                } while (cursor.moveToNext());
            }

        } catch (Exception ignored) {
        }
        return dosages;
    }


    public boolean checkProductNameCount(String product_name) {
        String selectQuery = "SELECT * FROM " + PRODUCT_TABLE + " WHERE " + COLUMN_MED_NAME + " = '" + product_name + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            int count = cursor.getCount();

            if (count > 1) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ignored) {
            return false;
        }
    }

    public Product getSelectedProduct(String productName, String productStrength, String productDosage) {
        ArrayList<Product> products = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + PRODUCT_TABLE + " WHERE " + COLUMN_MED_NAME + " = '" + productName + "' AND " + COLUMN_STRENGTH + " = '" + productStrength +
                "' AND " + COLUMN_DOSAGE + " = '" + productDosage + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Product product = new Product();
                    product.setId(cursor.getString(cursor.getColumnIndex(COLUMN_PROD_ID)));
                    product.setCategory_id(cursor.getString(cursor.getColumnIndex(COLUMN_CAT_ID)));
                    product.setVendor_id(cursor.getString(cursor.getColumnIndex(COLUMN_VENDOR_ID)));
                    product.setMedicine_package_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_PCK_ID)));
                    product.setMedicine_dosage_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_DOS_ID)));
                    product.setMedicine_strength_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_STRNGTH_ID)));
                    product.setMedicine_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_ID)));
                    product.setCategory(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY)));
                    product.setCompany_code(cursor.getString(cursor.getColumnIndex(COLUMN_COM_CODE)));
                    product.setCompany(cursor.getString(cursor.getColumnIndex(COLUMN_COMPANY)));
                    product.setDosage_code(cursor.getString(cursor.getColumnIndex(COLUMN_DOS_CODE)));
                    product.setDosage(cursor.getString(cursor.getColumnIndex(COLUMN_DOSAGE)));
                    product.setPackage_code(cursor.getString(cursor.getColumnIndex(COLUMN_PCKGE_CODE)));
                    product.setPackage_size(cursor.getString(cursor.getColumnIndex(COLUMN_PCKGE_SIZE)));
                    product.setStrength(cursor.getString(cursor.getColumnIndex(COLUMN_STRENGTH)));
                    product.setGeneric_name(cursor.getString(cursor.getColumnIndex(COLUMN_GENERIC_NAME)));
                    product.setMedicine_name(cursor.getString(cursor.getColumnIndex(COLUMN_MED_NAME)));
                    product.setDar_no(cursor.getString(cursor.getColumnIndex(COLUMN_DAR_NO)));
                    product.setUnit_price(cursor.getString(cursor.getColumnIndex(COLUMN_UNIT_PRICE)));

                    // Adding contact to list
                    products.add(product);
                } while (cursor.moveToNext());
            }

        } catch (Exception ignored) {
        }
        // return contact list
        return products.get(0);
    }

    public void createASale(Sale sale) {
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_SALE_INVOICE_ID, sale.getInvoiceID());
            values.put(COLUMN_SALE_PRODUCTS, new Gson().toJson(sale.getProducts()));
            values.put(COLUMN_SALE_STATUS, sale.getStatus());
            values.put(COLUMN_SALE_DATE, sale.getDate());
            values.put(COLUMN_SALE_TOTAL_COST, sale.getTotalCost());
            db.insert(SALE_TABLE, null, values);

        } catch (Exception exception) {
            if (exception instanceof SQLiteConstraintException)
                updateSale(sale);
            exception.printStackTrace();
        }
    }

    public void updateSale(Sale sale) {
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_SALE_STATUS, sale.getStatus());
            values.put(COLUMN_SALE_DISCOUNT, sale.getDiscount());
            values.put(COLUMN_SALE_DISCOUNTED_COST, sale.getDiscountedCost());
            values.put(COLUMN_SALE_CASH_PAID, sale.getCashPaid());
            values.put(COLUMN_SALE_CASH_BACK, sale.getCashBack());
            values.put(COLUMN_SALE_PAYMENT_DUE, sale.getPaymentDue());
//            if(sale.getStatus().equalsIgnoreCase(Resources.getSystem().getString(R.string.sale_on_due))){
            values.put(COLUMN_SALE_CUSTOMER_MOBILE, sale.getCustomerMobileNumber() == null ? System.currentTimeMillis() + "" : sale.getCustomerMobileNumber());
            values.put(COLUMN_SALE_CUSTOMER_NAME, sale.getCustomerName() == null ? "Anonymous" : sale.getCustomerName());
//            }

            db.update(SALE_TABLE, values,
                    COLUMN_SALE_INVOICE_ID + " = ?",
                    new String[]{sale.getInvoiceID()});

        } catch (Exception ignored) {
        }
    }

    public boolean checkInvoice(String invoice_id) {
        String selectQuery = "SELECT * FROM " + SALE_TABLE + " WHERE " + COLUMN_SALE_INVOICE_ID + " = '" + invoice_id + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            int count = cursor.getCount();

            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ignored) {
            return false;
        }
    }

    public Sale getSale(String invoiceID) {
        String selectQuery = "SELECT * FROM " + SALE_TABLE + " WHERE " + COLUMN_SALE_INVOICE_ID + " = '" + invoiceID + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                Sale sale = new Sale(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_INVOICE_ID)),
                        new ArrayList<>(Arrays.asList(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PRODUCTS)), Product[].class))),
                        ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_TOTAL_COST))), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DISCOUNT)),
                        ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DISCOUNTED_COST))), ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CASH_PAID))),
                        ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CASH_BACK))), ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PAYMENT_DUE))),
                        cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CUSTOMER_NAME)), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CUSTOMER_MOBILE)), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_STATUS)),
                        cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DATE)));
                return sale;
            }
        } catch (Exception ignored) {
        }
        // return contact list
        return null;
    }

    public ArrayList<Sale> getSaleOnProcess(Context context) {
        ArrayList<Sale> sales = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + SALE_TABLE + " WHERE " + COLUMN_SALE_STATUS + " = '" + context.getString(R.string.sale_on_process) + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Sale aSale = new Sale(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_INVOICE_ID)),
                            context.getString(R.string.sale_on_process), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DATE)),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_TOTAL_COST))),
                            new ArrayList<>(Arrays.asList(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PRODUCTS)), Product[].class))));
                    sales.add(aSale);
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }
        // return contact list
        return sales;
    }

    public ArrayList<Sale> getSaleOnDue(DueBook dueBook) {
        ArrayList<Sale> sales = new ArrayList<>();
        // Select All Query
        String selectQuery;
        if (dueBook.getName().equalsIgnoreCase("Anonymous"))
            selectQuery = "SELECT  * FROM " + SALE_TABLE + " WHERE " + COLUMN_SALE_CUSTOMER_NAME + " = '" + dueBook.getName() + "' AND " + COLUMN_SALE_STATUS + " = 'in_due'";
        else
            selectQuery = "SELECT  * FROM " + SALE_TABLE + " WHERE " + COLUMN_SALE_CUSTOMER_MOBILE + " = '" + dueBook.getMobile() + "' AND " + COLUMN_SALE_STATUS + " = 'in_due'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Sale aSale = new Sale(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_INVOICE_ID)),
                            new ArrayList<>(Arrays.asList(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PRODUCTS)), Product[].class))),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_TOTAL_COST))), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DISCOUNT)),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DISCOUNTED_COST))), ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CASH_PAID))),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CASH_BACK))), ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PAYMENT_DUE))),
                            cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CUSTOMER_NAME)), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CUSTOMER_MOBILE)), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_STATUS)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DATE)));
                    sales.add(aSale);
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }
        // return contact list
        return sales;
    }

    public ArrayList<Sale> getCompleteSales(Context context) {
        ArrayList<Sale> sales = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + SALE_TABLE + " WHERE " + COLUMN_SALE_STATUS + " = '" + context.getString(R.string.sale_completed) + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Sale aSale = new Sale(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_INVOICE_ID)), context.getString(R.string.sale_completed),
                            cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DATE)), ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_TOTAL_COST))),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DISCOUNTED_COST))));
                    sales.add(aSale);
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }
        // return contact list
        return sales;
    }

    public ArrayList<Sale> getSalesDuring(Context context, String startDT, String endDT) {
        ArrayList<Sale> sales = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + SALE_TABLE + " WHERE (" + COLUMN_SALE_STATUS + " = '" + context.getString(R.string.sale_completed) + "' OR " + COLUMN_SALE_STATUS + " = '" + context.getString(R.string.sale_on_due) +
                "') AND " + COLUMN_SALE_DATE + " >= Datetime('" + startDT + "') AND " + COLUMN_SALE_DATE + " <= Datetime('" + endDT + "')";
//        String selectQuery = "SELECT  * FROM " + SALE_TABLE + " WHERE " + COLUMN_SALE_STATUS + " = '" + context.getString(R.string.sale_completed) + "' AND " + COLUMN_SALE_DATE + " LIKE '" + date + "%'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Sale aSale = new Sale(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_INVOICE_ID)), new ArrayList<>(Arrays.asList(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PRODUCTS)), Product[].class))),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_TOTAL_COST))), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DISCOUNT)),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DISCOUNTED_COST))), ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CASH_PAID))),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CASH_BACK))), ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PAYMENT_DUE))),
                            cursor.getString(cursor.getColumnIndex(COLUMN_SALE_STATUS)), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DATE)));
                    sales.add(aSale);
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }
        // return contact list
        return sales;
    }

    public ArrayList<String> getSaleDatesDuring(String startDT, String endDT, Context context) {
        ArrayList<String> dateTimes = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT " + COLUMN_SALE_DATE + " FROM " + SALE_TABLE + " WHERE (" + COLUMN_SALE_STATUS + " = '" + context.getString(R.string.sale_completed) + "' OR " + COLUMN_SALE_STATUS + " = '" + context.getString(R.string.sale_on_due) +
                "') AND " + COLUMN_SALE_DATE + " >= Datetime('" + startDT + "') AND " + COLUMN_SALE_DATE + " <= Datetime('" + endDT + "')";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    dateTimes.add(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DATE)));
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }
        // return contact list
        return dateTimes;
    }

    public ArrayList<Sale> getRecentSales() {
        ArrayList<Sale> sales = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + SALE_TABLE + " ORDER BY " + COLUMN_SALE_INVOICE_ID + " DESC LIMIT 5";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Sale sale = new Sale(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_INVOICE_ID)), new ArrayList<>(Arrays.asList(new Gson().fromJson(
                            cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PRODUCTS)), Product[].class))));
                    sales.add(sale);
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }
        // return contact list

        return sales;
    }

    public ArrayList<Product> getPurchasedProducts() {
        ArrayList<Product> products = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + PRODUCT_TABLE + " ORDER BY " + COLUMN_PURCHASE_COUNTER + " DESC LIMIT 10";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Product product = new Product();
                    product.setId(cursor.getString(cursor.getColumnIndex(COLUMN_PROD_ID)));
                    product.setCategory_id(cursor.getString(cursor.getColumnIndex(COLUMN_CAT_ID)));
                    product.setVendor_id(cursor.getString(cursor.getColumnIndex(COLUMN_VENDOR_ID)));
                    product.setMedicine_package_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_PCK_ID)));
                    product.setMedicine_dosage_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_DOS_ID)));
                    product.setMedicine_strength_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_STRNGTH_ID)));
                    product.setMedicine_id(cursor.getString(cursor.getColumnIndex(COLUMN_MED_ID)));
                    product.setCategory(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY)));
                    product.setCompany_code(cursor.getString(cursor.getColumnIndex(COLUMN_COM_CODE)));
                    product.setCompany(cursor.getString(cursor.getColumnIndex(COLUMN_COMPANY)));
                    product.setDosage_code(cursor.getString(cursor.getColumnIndex(COLUMN_DOS_CODE)));
                    product.setDosage(cursor.getString(cursor.getColumnIndex(COLUMN_DOSAGE)));
                    product.setPackage_code(cursor.getString(cursor.getColumnIndex(COLUMN_PCKGE_CODE)));
                    product.setPackage_size(cursor.getString(cursor.getColumnIndex(COLUMN_PCKGE_SIZE)));
                    product.setStrength(cursor.getString(cursor.getColumnIndex(COLUMN_STRENGTH)));
                    product.setGeneric_name(cursor.getString(cursor.getColumnIndex(COLUMN_GENERIC_NAME)));
                    product.setMedicine_name(cursor.getString(cursor.getColumnIndex(COLUMN_MED_NAME)));
                    product.setDar_no(cursor.getString(cursor.getColumnIndex(COLUMN_DAR_NO)));
                    product.setUnit_price(cursor.getString(cursor.getColumnIndex(COLUMN_UNIT_PRICE)));

                    // Adding contact to list
                    products.add(product);
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }
        // return contact list
        return products;
    }


    public ArrayList<Sale> getSales(Context context, String invoiceID) {
        ArrayList<Sale> sales = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + SALE_TABLE + " WHERE " + COLUMN_SALE_INVOICE_ID + " LIKE '%" + invoiceID + "%'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Sale aSale = new Sale(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_INVOICE_ID)), new ArrayList<>(Arrays.asList(new Gson().fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PRODUCTS)), Product[].class))),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_TOTAL_COST))), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DISCOUNT)),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DISCOUNTED_COST))), ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CASH_PAID))),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CASH_BACK))), ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PAYMENT_DUE))), context.getString(R.string.sale_completed), cursor.getString(cursor.getColumnIndex(COLUMN_SALE_DATE)));
                    sales.add(aSale);
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }
        // return contact list

        return sales;
    }

    public boolean checkDueNumber(String customerMobileNumber) {
        String selectQuery = "SELECT * FROM " + DUE_TABLE + " WHERE " + COLUMN_DUE_CUSTOMER_MOBILE + " = '" + customerMobileNumber + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            int count = cursor.getCount();

            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ignored) {
            return true;
        }
    }

    public void updateDue(DueBook dueBook) {
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_DUE_AMOUNT, dueBook.getPaymentDue());
            db.update(DUE_TABLE, values,
                    COLUMN_DUE_ID + " = ?",
                    new String[]{dueBook.getId()});
        } catch (Exception ignored) {
        }

    }

    public void addSaleToDueTable(Sale sale) {
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_DUE_CUSTOMER_MOBILE, sale.getCustomerMobileNumber());
            values.put(COLUMN_DUE_CUSTOMER_NAME, sale.getCustomerName());
            values.put(COLUMN_DUE_AMOUNT, sale.getPaymentDue());
            db.insert(DUE_TABLE, null, values);
        } catch (Exception ignored) {
        }
    }

    public ArrayList<String> getDueNumbers() {
        ArrayList<String> dueNumbers = new ArrayList<>();
        String selectQuery = "SELECT DISTINCT " + COLUMN_DUE_CUSTOMER_MOBILE + " FROM " + DUE_TABLE;
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    dueNumbers.add(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_CUSTOMER_MOBILE)));
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }

        return dueNumbers;
    }

    public ArrayList<String> getDueNames() {
        ArrayList<String> dueNumbers = new ArrayList<>();
        String selectQuery = "SELECT DISTINCT " + COLUMN_DUE_CUSTOMER_NAME + " FROM " + DUE_TABLE;
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    dueNumbers.add(cursor.getString(cursor.getColumnIndex(COLUMN_DUE_CUSTOMER_NAME)));
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }

        return dueNumbers;
    }

    public ArrayList<DueBook> getDueBook() {
        ArrayList<DueBook> dues = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + DUE_TABLE;
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    DueBook dueBook = new DueBook(cursor.getString(cursor.getColumnIndex(COLUMN_DUE_ID)), cursor.getString(cursor.getColumnIndex(COLUMN_DUE_CUSTOMER_NAME)), cursor.getString(cursor.getColumnIndex(COLUMN_DUE_CUSTOMER_MOBILE)),
                            ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_DUE_AMOUNT))));
                    dues.add(dueBook);
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }
        // return contact list

        return dues;
    }

    public DueBook getDue(String customerMobileNumber, String customerName) {
        String selectQuery;
        if (!TextUtils.isEmpty(customerName) && customerName.equalsIgnoreCase("Anonymous"))
            selectQuery = "SELECT * FROM " + DUE_TABLE + " WHERE " + COLUMN_DUE_CUSTOMER_NAME + " = '" + customerName + "'";
        else
            selectQuery = "SELECT * FROM " + DUE_TABLE + " WHERE " + COLUMN_DUE_CUSTOMER_MOBILE + " = '" + customerMobileNumber + "'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                DueBook dueBook = new DueBook(cursor.getString(cursor.getColumnIndex(COLUMN_DUE_ID)), cursor.getString(cursor.getColumnIndex(COLUMN_DUE_CUSTOMER_NAME)), cursor.getString(cursor.getColumnIndex(COLUMN_DUE_CUSTOMER_MOBILE)),
                        ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_DUE_AMOUNT))));
                return dueBook;
            }
        } catch (Exception ignored) {
        }
        return null;
    }

    public double getDueAmount(String customerMobileNumber) {
        double totalDue = 0;
        String selectQuery = "SELECT * FROM " + SALE_TABLE + " WHERE " + COLUMN_SALE_CUSTOMER_MOBILE + " = '" + customerMobileNumber + "' AND " + COLUMN_SALE_STATUS + " = 'in_due'";
        try {
            db = getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    totalDue += ApplicationUtils.convertToDouble(cursor.getString(cursor.getColumnIndex(COLUMN_SALE_PAYMENT_DUE)));
                } while (cursor.moveToNext());
            }
        } catch (Exception ignored) {
        }
        return totalDue;
    }
}
