package ponno.app.data.server;

import okhttp3.MultipartBody;
import ponno.app.data.responses.ProductResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIServiceInterface {

    /**
     * Login & LogOut
     */
//    @FormUrlEncoded
//    @POST("login")
//    Call<LoginResponse> loginUser(@Field("email") String identity, @Field("password") String password, @Field("device_token") String device_token);
//
//    @GET("logout")
//    Call<CommonResponse> logOut(@Query("token") String token, @Query("device_token") String device_token);

    /**
     * Inventory
     */
    @GET("product")
    Call<ProductResponse> getAllProducts();

    /*@GET("inventory/all")
    Call<StockResponse> getAllProducts(@Query("offset") int offset);

    @GET("inventory/new_arrivals")
    Call<StockResponse> getNewArrivedProducts(@Query("offset") int offset);

    @GET("inventory/best_sellers")
    Call<StockResponse> getBestSellerProducts(@Query("offset") int offset);

    @GET("inventory/most_purchased")
    Call<StockResponse> getMostPurchasedProducts(@Query("offset") int offset);

    @GET("inventory/most_searched")
    Call<StockResponse> getMostSearchedProducts(@Query("offset") int offset);

    @GET("inventory/discount_products")
    Call<StockResponse> getDiscountedProducts();

    @GET("inventory/{product_filter}")
    Call<StockResponse> getFilteredProducts(@Path("product_filter") String product_filter, @Query("offset") int offset);

    @GET("inventory")
    Call<StockResponse> getCategorizedProducts(@Query("category_id") String id, @Query("offset") int offset);

    @POST("inventory/add")
    Call<AddProductResponse> addProductInInventory(@Query("token") String token, @Body MultipartBody body);

    @POST("inventory/update/{id}")
    Call<AddProductResponse> updateProductInInventory(@Path("id") String id, @Query("token") String token, @Body MultipartBody body);

    @GET("inventory/delete/{id}")
    Call<CommonResponse> deleteProductFromInventory(@Path("id") String id, @Query("token") String token);

    @GET("inventory/delete/{product_id}/{image_id}")
    Call<CommonResponse> deleteImageFromInventory(@Path("product_id") String productId, @Path("image_id") String imageId, @Query("token") String token);

    @FormUrlEncoded
    @POST("inventory/discount/{id}/make")
    Call<AddProductResponse> upsertDiscountInProduct(@Path("id") String productId, @Query("token") String token, @Field("type") String discountType, @Field("amount") String discountAmount,
                                                     @Field("text") String discountText, @Field("notify") String enablePush, @Field("message") String pushMessage);

    @GET("inventory/discount/{id}/delete")
    Call<CommonResponse> deleteDiscountFromProduct(@Path("id") String productId, @Query("token") String token);

    @GET("purchase_list")
    Call<PurchaseListResponse> getPurchaseList(@Query("token") String token);

    @POST("purchase/{id}")
    Call<CommonResponse> updatePurchaseStatus(@Path("id") String productId, @Query("token") String token);

    *//**
     * Category
     *//*
    @GET("categories")
    Call<AllCategoryResponse> getAllCategories(@Query("key") String key);

    @POST("categories")
    Call<CreateCategoryResponse> createCategory(@Query("token") String token, @Body MultipartBody body);

    @POST("categories/{id}")
    Call<CreateCategoryResponse> updateCategory(@Path("id") String id, @Query("token") String token, @Body MultipartBody body);

    @POST("categories/{id}")
    Call<CommonResponse> deleteCategory(@Path("id") String id, @Query("token") String token, @Query("_method") String method);

    *//**
     * Home
     *//*
    @GET("home/slider/{type}")
    Call<SliderResponse> getSliders(@Path("type") String type);

    @POST("home/slider/{type}/addImage")
    Call<CommonResponse> addImagesInSlider(@Body MultipartBody body, @Path("type") String type, @Query("token") String token);

    @GET("home/slider/delete/{type}/{id}")
    Call<CommonResponse> deleteSliderImage(@Path("type") String type, @Path("id") String id, @Query("token") String token);

    @GET("home/settings")
    Call<SettingsResponse> getSettings(@Query("device_token") String device_token);

    @POST("home/settings/edit")
    Call<CommonResponse> saveSettings(@Query("token") String token, @Body MultipartBody body);

    @GET("home/app_user")
    Call<AppUserResponse> getAppUserInfo();

    @FormUrlEncoded
    @POST("home/app_user")
    Call<CommonResponse> updateAppUserInfo(@Query("token") String token, @Field("shop_name") String shop_name, @Field("owner") String owner, @Field("address") String address,
                                           @Field("zip_code") String zip_code, @Field("city") String city, @Field("phone") String phone, @Field("email") String email, @Field("mollie_id") String mollie_id);

    *//** Overview *//*

    *//**
     * Order
     **//*
    @GET("orders")
    Call<OrderListResponse> getAllOrders(@Query("token") String token, @Query("offset") int offset, @Query("status") String status);

    @GET("purchase_schedule")
    Call<ScheduleResponse> getSchedule();

    @FormUrlEncoded
    @POST("packages")
    Call<PackagesResponse> getPackages(@Field("products") String products);

    @FormUrlEncoded
    @POST("package_schedule")
    Call<CommonResponse> saveChangesSchedule(@Query("token") String token, @Field(value = "schedules") String schedule);

    @GET("package_schedule/{schedule_id}/{id}")
    Call<CommonResponse> deletePackageSchedule(@Path("schedule_id") String schedule_id, @Path("id") String id, @Query("token") String token);

    @FormUrlEncoded
    @POST("orders")
    Call<OrderCreateResponse> createOrder(@Field("client_name") String client_name, @Field("phone") String phone, @Field("email") String email,
                                          @Field("comments") String comments, @Field("address") String address, @Field("products") String products,
                                          @Field("method") String method, @Field("purchase_package") String purchase_package);

    @GET("orders/deliver/{order_id}")
    Call<CommonResponse> deliverOrder(@Path("order_id") String order_id, @Query("token") String token);*/

}
