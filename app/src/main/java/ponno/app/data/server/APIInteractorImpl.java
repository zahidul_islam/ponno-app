package ponno.app.data.server;

import android.content.Context;


import okhttp3.OkHttpClient;
import ponno.app.R;
import ponno.app.data.responses.CommonResponse;
import ponno.app.data.responses.ProductResponse;
import ponno.app.utils.ApplicationUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public class APIInteractorImpl implements APIInteractor {
    private Retrofit retrofit;
    private APIServiceInterface apiServiceInterface;
    private Context context;
//    private UserManager userManager;
    private String token;

    //    private static final String BASE_URL = "http://fangoo.montorlabs.com/api/";
    private static final String BASE_URL = "http://35.184.146.247/api/";

    public APIInteractorImpl(Context context) {
        this.context = context;
//        userManager = new UserManager(context);
        initAPIService(BASE_URL);
    }

    private void initAPIService(String url) {

        OkHttpClient client = ApplicationUtils.getClient(context);
        retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiServiceInterface = retrofit.create(APIServiceInterface.class);
    }

    private boolean isResponseValid(Response<?> response) {
        if (response == null) {
            return false;
        } else {
            if (response.body() == null) {
                return false;
            } else {
                return true;
            }
        }
    }

    private String prepareFailedMessage(Response<?> response) {
        if (isResponseValid(response)) {
            CommonResponse commonResponse = (CommonResponse) response.body();
            return commonResponse.getMessage();
        } else {
            return context.getString(R.string.conn_failed);
        }
    }

    /*@Override
    public void userLogin(final LoginListener loginListener, String email, String password) {
        Call<LoginResponse> call = apiServiceInterface.loginUser(email, password, userManager.getDeviceTOken());
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    loginListener.onLoginSuccess(response.body().getUser());
                } else {
                    loginListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginListener.onFailed(context.getString(R.string.conn_failed));
            }
        });
    }


    @Override
    public void logOut(final LogOutListener logOutListener) {
        Call<CommonResponse> call = apiServiceInterface.logOut(userManager.getUser().getToken(), userManager.getDeviceTOken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    logOutListener.onLogOutSuccess();
                } else {
                    logOutListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                logOutListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getAllProducts(final GetProductListener getStockListener, int offset) {
        Call<ProductResponse> call = apiServiceInterface.getAllProducts(offset);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    getStockListener.onProductLoaded(response.body().getProducts());
                } else {
                    getStockListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                getStockListener.onFailed(t.getMessage());
            }
        });
    }*/

    @Override
    public void getAllProducts(final GetProductListener getProductListener) {
        Call<ProductResponse> call = apiServiceInterface.getAllProducts();
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    getProductListener.onProductLoaded(response.body().getProducts().getData());
                } else {
                    getProductListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                getProductListener.onFailed(t.getMessage());
            }
        });
    }

    /*@Override
    public void getSliders(final GetSliderListener getSlidersListener) {
        final Call<SliderResponse> sliderResponseCall = apiServiceInterface.getSliders("1");
        sliderResponseCall.enqueue(new Callback<SliderResponse>() {
            @Override
            public void onResponse(Call<SliderResponse> call, Response<SliderResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    getSlidersListener.onSliderLoaded(response.body());
                } else {
                    getSlidersListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<SliderResponse> call, Throwable t) {
                getSlidersListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getMiniSliders(final GetMiniSliderListener getMiniSlidersListener) {
        final Call<SliderResponse> sliderResponseCall = apiServiceInterface.getSliders("2");
        sliderResponseCall.enqueue(new Callback<SliderResponse>() {
            @Override
            public void onResponse(Call<SliderResponse> call, Response<SliderResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    getMiniSlidersListener.onMiniSliderLoaded(response.body());
                } else {
                    getMiniSlidersListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<SliderResponse> call, Throwable t) {
                getMiniSlidersListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void addImageInSlider(final SliderImageAddedListener sliderImageAddedListener, ArrayList<String> localImageList, final String type) {
        MultipartBody.Builder requestBodyBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        for (int i = 0; i < localImageList.size(); i++) {
            File imageFile = new File("" + localImageList.get(i));
            requestBodyBuilder.addFormDataPart("images[]", imageFile.getName(),
                    RequestBody.create(MediaType.parse("image/jpeg"), imageFile));
        }

        Call<CommonResponse> call = apiServiceInterface.addImagesInSlider(requestBodyBuilder.build(), type, userManager.getUser().getToken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    sliderImageAddedListener.onImageAdded(type);
                } else {
                    sliderImageAddedListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                sliderImageAddedListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void addImageInSlider(final SliderImageAddedListener sliderImageAddedListener, ArrayList<String> localImageList, final String type, String ref_product_id) {
        MultipartBody.Builder requestBodyBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        for (int i = 0; i < localImageList.size(); i++) {
            File imageFile = new File("" + localImageList.get(i));
            requestBodyBuilder.addFormDataPart("images[]", imageFile.getName(),
                    RequestBody.create(MediaType.parse("image/jpeg"), imageFile));
        }

        requestBodyBuilder.addFormDataPart("ref_product_id", null, RequestBody.create(MediaType.parse("text/plain"), ref_product_id));
        Call<CommonResponse> call = apiServiceInterface.addImagesInSlider(requestBodyBuilder.build(), type, userManager.getUser().getToken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    sliderImageAddedListener.onImageAdded(type);
                } else {
                    sliderImageAddedListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                sliderImageAddedListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void deleteImageFromSlider(final DeleteSliderImageListener deleteSliderImageListener, final String type, final String id) {
        final Call<CommonResponse> call = apiServiceInterface.deleteSliderImage(type, id, userManager.getUser().getToken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    deleteSliderImageListener.onItemDeleted(id, type);
                } else {
                    deleteSliderImageListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                deleteSliderImageListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getNewArrivedProducts(final GetNewArrivalStockListener getNewArrivalStockListener, int offset) {
        Call<ProductResponse> call = apiServiceInterface.getNewArrivedProducts(offset);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    getNewArrivalStockListener.onNewArrivalStockLoaded(response.body().getProducts());
                } else {
                    getNewArrivalStockListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                getNewArrivalStockListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getBestSellerProducts(final GetBestSellerStockListener getBestSellerStockListener, int offset) {
        Call<ProductResponse> call = apiServiceInterface.getBestSellerProducts(offset);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    getBestSellerStockListener.onBestSellerStockLoaded(response.body().getProducts());
                } else {
                    getBestSellerStockListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                getBestSellerStockListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getMostPurchasedProducts(final GetMostPurchasedStockListener getMostPurchasedStockListener, int offset) {
        Call<ProductResponse> call = apiServiceInterface.getMostPurchasedProducts(offset);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    getMostPurchasedStockListener.onMostPurchasedStockLoaded(response.body().getProducts());
                } else {
                    getMostPurchasedStockListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                getMostPurchasedStockListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getMostSearchedProducts(final GetMostSearchedStockListener getMostSearchedStockListener, int offset) {
        Call<ProductResponse> call = apiServiceInterface.getMostSearchedProducts(offset);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1 && getMostSearchedStockListener != null) {
                    getMostSearchedStockListener.onMostSearchedStockLoaded(response.body().getProducts());
                } else {
                    if (getMostSearchedStockListener != null) {
                        getMostSearchedStockListener.onFailed(prepareFailedMessage(response));
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                if (getMostSearchedStockListener != null) {
                    getMostSearchedStockListener.onFailed(t.getMessage());
                }
            }
        });
    }

    @Override
    public void getFilteredProducts(final GetProductListener getStockListener, String filter_text, int offset) {
        Call<ProductResponse> call = apiServiceInterface.getFilteredProducts(filter_text, offset);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    if (response.body().getProducts().size() > 0) {
                        getStockListener.onProductLoaded(response.body().getProducts());
                    }
                } else {
                    getStockListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                getStockListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getCategorizedProducts(final GetProductListener getStockListener, String category_id, int offset) {
        Call<ProductResponse> call = apiServiceInterface.getCategorizedProducts(category_id, offset);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    getStockListener.onProductLoaded(response.body().getProducts());
                } else {
                    getStockListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                getStockListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getSettings(final GetSettingsListener getSettingsListener) {
        Call<SettingsResponse> call = apiServiceInterface.getSettings(userManager.getDeviceTOken());
        call.enqueue(new Callback<SettingsResponse>() {
            @Override
            public void onResponse(Call<SettingsResponse> call, Response<SettingsResponse> response) {

                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    getSettingsListener.onSettingsLoaded(response.body().getSettings());
                } else {
                    getSettingsListener.onFailed(prepareFailedMessage(response));
                }

            }

            @Override
            public void onFailure(Call<SettingsResponse> call, Throwable t) {
                getSettingsListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void changeSettings(final SettingsSaveListener settingsSaveListener, ArrayList<Settings> allSettings, File logo) {
        MultipartBody.Builder requestBodyBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        if (logo != null) {
            requestBodyBuilder.addFormDataPart("logo", logo.getName(),
                    RequestBody.create(MediaType.parse("image/jpeg"), logo));
        }
        String settings = new Gson().toJson(allSettings);
        requestBodyBuilder.addFormDataPart("settings", null, RequestBody.create(MediaType.parse("text/plain"), settings));

        Call<CommonResponse> call = apiServiceInterface.saveSettings(userManager.getUser().getToken(), requestBodyBuilder.build());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    settingsSaveListener.onSettingsSaved();
                } else {
                    settingsSaveListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                settingsSaveListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getDiscountedProducts(final GetProductListener getStockListener) {
        Call<ProductResponse> call = apiServiceInterface.getDiscountedProducts();
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    getStockListener.onProductLoaded(response.body().getProducts());
                } else {
                    getStockListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                getStockListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void deleteProductFromInventory(final DeleteItemListener deleteItemListener, final String productId) {
        Call<CommonResponse> call = apiServiceInterface.deleteProductFromInventory(productId, userManager.getUser().getToken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    deleteItemListener.onItemDeleted(productId);
                } else {
                    deleteItemListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                deleteItemListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void addProduct(final AddProductListener addProductListener, Product product, ArrayList<String> localImageList) {
        MultipartBody.Builder requestBodyBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        for (int i = 0; i < localImageList.size(); i++) {
            File imageFile = new File("" + localImageList.get(i));
            requestBodyBuilder.addFormDataPart("images[]", imageFile.getName(),
                    RequestBody.create(MediaType.parse("image/jpeg"), imageFile));
        }
        String productJson = new Gson().toJson(product);
        requestBodyBuilder.addFormDataPart("product", null, RequestBody.create(MediaType.parse("text/plain"), productJson));

        Call<AddProductResponse> call = apiServiceInterface.addProductInInventory(userManager.getUser().getToken(), requestBodyBuilder.build());
        call.enqueue(new Callback<AddProductResponse>() {
            @Override
            public void onResponse(Call<AddProductResponse> call, Response<AddProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    addProductListener.onProductAdded();
                } else {
                    addProductListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<AddProductResponse> call, Throwable t) {
                addProductListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void updateProduct(final UpdateProductListener updateProductListener, Product product, ArrayList<String> localImageList) {
        MultipartBody.Builder requestBodyBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        for (int i = 0; i < localImageList.size(); i++) {
            File imageFile = new File("" + localImageList.get(i));
            requestBodyBuilder.addFormDataPart("update_images[]", imageFile.getName(),
                    RequestBody.create(MediaType.parse("image/jpeg"), imageFile));
        }
        String productJson = new Gson().toJson(product);
        requestBodyBuilder.addFormDataPart("product", null, RequestBody.create(MediaType.parse("text/plain"), productJson));

        Call<AddProductResponse> call = apiServiceInterface.updateProductInInventory(product.getId(), userManager.getUser().getToken(), requestBodyBuilder.build());
        call.enqueue(new Callback<AddProductResponse>() {
            @Override
            public void onResponse(Call<AddProductResponse> call, Response<AddProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    updateProductListener.onProductUpdated();
                } else {
                    updateProductListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<AddProductResponse> call, Throwable t) {
                updateProductListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void deleteImageFromProduct(final DeleteItemListener deleteItemListener, String productId, final String imageId) {
        Call<CommonResponse> call = apiServiceInterface.deleteImageFromInventory(productId, imageId, userManager.getUser().getToken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    deleteItemListener.onItemDeleted(imageId);
                } else {
                    deleteItemListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                deleteItemListener.onItemDeleted(t.getMessage());
            }
        });
    }

    @Override
    public void upsertDiscount(final UpsertDiscountListener upsertDiscountListener, Product product) {
        Call<AddProductResponse> call = apiServiceInterface.upsertDiscountInProduct(product.getId(), userManager.getUser().getToken(), product.getDiscount().getDiscountType(), product.getDiscount().getDiscountAmount(), product.getDiscount().getDiscountText(), product.getDiscount().getEnablePush(), product.getDiscount().getPushText());
        call.enqueue(new Callback<AddProductResponse>() {
            @Override
            public void onResponse(Call<AddProductResponse> call, Response<AddProductResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    upsertDiscountListener.onDiscountUpdated();
                } else {
                    upsertDiscountListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<AddProductResponse> call, Throwable t) {
                upsertDiscountListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void deleteDiscount(final DeleteItemListener deleteItemListener, final String productId) {
        Call<CommonResponse> call = apiServiceInterface.deleteDiscountFromProduct(productId, userManager.getUser().getToken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    deleteItemListener.onItemDeleted(productId);
                } else {
                    deleteItemListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                deleteItemListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getAllCategories(final OnCategoriesLoadedListener onCategoriesLoadedListener, String key) {
        Call<AllCategoryResponse> call = apiServiceInterface.getAllCategories(key);
        call.enqueue(new Callback<AllCategoryResponse>() {
            @Override
            public void onResponse(Call<AllCategoryResponse> call, Response<AllCategoryResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    onCategoriesLoadedListener.onCategoriesLoaded(response.body().getCategories());
                } else {
                    onCategoriesLoadedListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<AllCategoryResponse> call, Throwable t) {
                onCategoriesLoadedListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void createCategory(final CreateCategoryListener createCategoryListener, String category_name, File image) {
        MultipartBody.Builder requestBodyBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        if (image != null) {
            requestBodyBuilder.addFormDataPart("image", image.getName(),
                    RequestBody.create(MediaType.parse("image/jpeg"), image));
        }
        requestBodyBuilder.addFormDataPart("name", null, RequestBody.create(MediaType.parse("text/plain"), category_name));

        Call<CreateCategoryResponse> call = apiServiceInterface.createCategory(userManager.getUser().getToken(), requestBodyBuilder.build());
        call.enqueue(new Callback<CreateCategoryResponse>() {
            @Override
            public void onResponse(Call<CreateCategoryResponse> call, Response<CreateCategoryResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    createCategoryListener.onCategoryCreated(response.body().getCategory());
                } else {
                    createCategoryListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CreateCategoryResponse> call, Throwable t) {
                createCategoryListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void updateCategory(final UpdateCategoryListener updateCategoryListener, Category category, File image) {
        MultipartBody.Builder requestBodyBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        if (image != null) {
            requestBodyBuilder.addFormDataPart("image", image.getName(),
                    RequestBody.create(MediaType.parse("image/jpeg"), image));
        }
        requestBodyBuilder.addFormDataPart("name", null, RequestBody.create(MediaType.parse("text/plain"), category.getName()));

        Call<CreateCategoryResponse> call = apiServiceInterface.updateCategory(category.getId(), userManager.getUser().getToken(), requestBodyBuilder.build());
        call.enqueue(new Callback<CreateCategoryResponse>() {
            @Override
            public void onResponse(Call<CreateCategoryResponse> call, Response<CreateCategoryResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    updateCategoryListener.onCategoryUpdated(response.body().getCategory());
                } else {
                    updateCategoryListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CreateCategoryResponse> call, Throwable t) {
                updateCategoryListener.onFailed(t.getMessage());
            }
        });
    }


    @Override
    public void deleteCategory(final String id, final DeleteItemListener deleteItemListener) {
        Call<CommonResponse> call = apiServiceInterface.deleteCategory(id, userManager.getUser().getToken(), "DELETE");
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    deleteItemListener.onItemDeleted(id);
                } else {
                    deleteItemListener.onFailed(prepareFailedMessage(response));
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                deleteItemListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getAllOrders(final OnOrderLoadedListener onOrderLoadedListener, int offset, String status) {
        Call<OrderListResponse> call = apiServiceInterface.getAllOrders(userManager.getUser().getToken(), offset, status);
        call.enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(Call<OrderListResponse> call, Response<OrderListResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    onOrderLoadedListener.onOrderLoaded(response.body().getOrders());
                } else {
                    onOrderLoadedListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<OrderListResponse> call, Throwable t) {
                onOrderLoadedListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getSchedule(final OnScheduleLoadedListener onScheduleLoadedListener) {
        Call<ScheduleResponse> call = apiServiceInterface.getSchedule();
        call.enqueue(new Callback<ScheduleResponse>() {
            @Override
            public void onResponse(Call<ScheduleResponse> call, Response<ScheduleResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    onScheduleLoadedListener.onScheduleLoaded(response.body().getSchedules());
                } else {
                    onScheduleLoadedListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<ScheduleResponse> call, Throwable t) {
                onScheduleLoadedListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void saveSchedule(final OnScheduleSavedListener scheduleSavedListener, String schedule) {
        Call<CommonResponse> call = apiServiceInterface.saveChangesSchedule(userManager.getUser().getToken(), schedule);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    scheduleSavedListener.onScheduleSaved();
                } else {
                    scheduleSavedListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                scheduleSavedListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void deletePackage(final DeletePackageListener deletePackageListener, String schedule_id, String id) {
        Call<CommonResponse> call = apiServiceInterface.deletePackageSchedule(schedule_id, id, userManager.getUser().getToken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    deletePackageListener.onPackageDeleted();
                } else {
                    deletePackageListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                deletePackageListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getPackages(final OnPackagesLoadedListener onPackagesLoadedListener, String products) {
        Call<PackagesResponse> call = apiServiceInterface.getPackages(products);
        call.enqueue(new Callback<PackagesResponse>() {
            @Override
            public void onResponse(Call<PackagesResponse> call, Response<PackagesResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    onPackagesLoadedListener.onPackagesLoaded(response.body());
                } else {
                    onPackagesLoadedListener.onFailed(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PackagesResponse> call, Throwable t) {
                onPackagesLoadedListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getAppUserInfo(final AppUserInfoLoadedListener appUserInfoLoadedListener) {
        Call<AppUserResponse> call = apiServiceInterface.getAppUserInfo();
        call.enqueue(new Callback<AppUserResponse>() {
            @Override
            public void onResponse(Call<AppUserResponse> call, Response<AppUserResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    appUserInfoLoadedListener.onAppUserLoaded(response.body());
                } else {
                    appUserInfoLoadedListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<AppUserResponse> call, Throwable t) {
                appUserInfoLoadedListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void updateAppUserInfo(final AppUserInfoUpdatedListener appUserInfoUpdatedListener, AppUser appUser) {
        Call<CommonResponse> call = apiServiceInterface.updateAppUserInfo(userManager.getUser().getToken(), appUser.getShop_name(), appUser.getOwner(), appUser.getAddress(),
                appUser.getZip_code(), appUser.getCity(), appUser.getPhone(), appUser.getEmail(), appUser.getMollie_id());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    appUserInfoUpdatedListener.onAppUserUpdated();
                } else {
                    appUserInfoUpdatedListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                appUserInfoUpdatedListener.onFailed(t.getMessage());
            }

        });
    }

    @Override
    public void continueToCheckout(final CheckoutListener checkoutListener, Order order) {
        String purchase_package = new Gson().toJson(order.getPurchase_package());
        String ordered_products = new Gson().toJson(order.getOrder_products());
        Call<OrderCreateResponse> call = apiServiceInterface.createOrder(order.getClient_name(), order.getPhone(), order.getEmail(), order.getComments(), order.getAddress(), ordered_products, order.getPayment(), purchase_package);
        call.enqueue(new Callback<OrderCreateResponse>() {
            @Override
            public void onResponse(Call<OrderCreateResponse> call, Response<OrderCreateResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    checkoutListener.onOrderCreated(response.body());
                } else {
                    checkoutListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<OrderCreateResponse> call, Throwable t) {
                checkoutListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getPurchaseList(final PurchaseListLoadedListener purchaseListLoadedListener) {
        Call<PurchaseListResponse> call = apiServiceInterface.getPurchaseList(userManager.getUser().getToken());
        call.enqueue(new Callback<PurchaseListResponse>() {
            @Override
            public void onResponse(Call<PurchaseListResponse> call, Response<PurchaseListResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    purchaseListLoadedListener.onPurchaseListLoaded(response.body().getPurchaseList());
                } else {
                    purchaseListLoadedListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<PurchaseListResponse> call, Throwable t) {
                purchaseListLoadedListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void updatePurchaseListStatus(final DeleteItemListener deleteItemListener, final String purchaseProductId) {
        Call<CommonResponse> call = apiServiceInterface.updatePurchaseStatus(purchaseProductId, userManager.getUser().getToken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    deleteItemListener.onItemDeleted(purchaseProductId);
                } else {
                    deleteItemListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                deleteItemListener.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void deliverOrder(final OrderDeliveryListener orderDeliveryListener, String order_id) {
        Call<CommonResponse> call = apiServiceInterface.deliverOrder(order_id, userManager.getUser().getToken());
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isResponseValid(response) && response.body().getSuccess() == 1) {
                    orderDeliveryListener.onOrderDelivered();
                } else {
                    orderDeliveryListener.onFailed(prepareFailedMessage(response));
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                orderDeliveryListener.onFailed(t.getMessage());
            }
        });
    }*/
}
