package ponno.app.data.server;

import java.util.ArrayList;

import ponno.app.model.Product;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public interface APIInteractor {
//    void userLogin(LoginListener loginListener, String email, String password);
//
//    void logOut(LogOutListener logOutListener);

//    void getAllProducts(GetProductListener getStockListener, int offset);

    void getAllProducts(GetProductListener getProductListener);

    /*void getSliders(GetSliderListener getSlidersListener);

    void getMiniSliders(GetMiniSliderListener getMiniSlidersListener);

    void addImageInSlider(SliderImageAddedListener sliderImageAddedListener, ArrayList<String> localImageList, String type);

    void addImageInSlider(SliderImageAddedListener sliderImageAddedListener, ArrayList<String> localImageList, String type, String ref_product_id);

    void deleteImageFromSlider(DeleteSliderImageListener deleteSliderImageListener, String type, String id);

    void getNewArrivedProducts(GetNewArrivalStockListener getNewArrivalStockListener, int offset);

    void getBestSellerProducts(GetBestSellerStockListener getBestSellerStockListener, int offset);

    void getMostPurchasedProducts(GetMostPurchasedStockListener getMostPurchasedStockListener, int offset);

    void getMostSearchedProducts(GetMostSearchedStockListener getMostSearchedStockListener, int offset);

    void getFilteredProducts(GetProductListener getStockListener, String filter_text, int offset);

    void getCategorizedProducts(GetProductListener getStockListener, String category_id, int offset);

    void getSettings(GetSettingsListener getSettingsListener);

    void changeSettings(SettingsSaveListener settingsSaveListener, ArrayList<Settings> allSettings, File logo);

    void getDiscountedProducts(GetProductListener getStockListener);

    void addProduct(AddProductListener addProductListener, Product product, ArrayList<String> localImageList);

    void updateProduct(UpdateProductListener updateProductListener, Product product, ArrayList<String> localImageList);

    void deleteProductFromInventory(DeleteItemListener deleteItemListener, String productId);

    void deleteImageFromProduct(DeleteItemListener deleteItemListener, String productId, String imageId);

    void upsertDiscount(UpsertDiscountListener upsertDiscountListener, Product product);

    void deleteDiscount(DeleteItemListener deleteItemListener, String productId);

    void getAllCategories(OnCategoriesLoadedListener onCategoriesLoadedListener, String key);

    void createCategory(CreateCategoryListener createCategoryListener, String category_name, File image);

    void updateCategory(UpdateCategoryListener updateCategoryListener, Category category, File image);

    void deleteCategory(String id, DeleteItemListener deleteItemListener);

    void getAllOrders(OnOrderLoadedListener onOrderLoadedListener, int offset, String status);

    void getSchedule(OnScheduleLoadedListener onScheduleLoadedListener);

    void saveSchedule(OnScheduleSavedListener scheduleSavedListener, String schedule);

    void deletePackage(DeletePackageListener deletePackageListener, String schedule_id, String id);

    void getPackages(OnPackagesLoadedListener onPackagesLoadedListener, String products);

    void getAppUserInfo(AppUserInfoLoadedListener appUserInfoLoadedListener);

    void updateAppUserInfo(AppUserInfoUpdatedListener appUserInfoUpdatedListener, AppUser appUser);

    void continueToCheckout(CheckoutListener checkoutListener, Order order);

    void getPurchaseList(PurchaseListLoadedListener purchaseListLoadedListener);

    void updatePurchaseListStatus(DeleteItemListener deleteItemListener, String purchaseProductId);

    void deliverOrder(OrderDeliveryListener orderDeliveryListener, String order_id);


    interface LoginListener extends FailedListener {
        void onLoginSuccess(User user);
    }

    interface LogOutListener extends FailedListener {
        void onLogOutSuccess();
    }

    interface GetSliderListener extends FailedListener {
        void onSliderLoaded(SliderResponse sliderResponse);
    }

    interface GetMiniSliderListener extends FailedListener {
        void onMiniSliderLoaded(SliderResponse sliderResponse);
    }

    interface SliderImageAddedListener extends FailedListener {
        void onImageAdded(String type);
    }*/

    interface GetProductListener extends FailedListener {
        void onProductLoaded(ArrayList<Product> products);
    }

    /*interface GetNewArrivalStockListener extends FailedListener {
        void onNewArrivalStockLoaded(ArrayList<Product> products);
    }

    interface GetBestSellerStockListener extends FailedListener {
        void onBestSellerStockLoaded(ArrayList<Product> products);
    }

    interface GetMostPurchasedStockListener extends FailedListener {
        void onMostPurchasedStockLoaded(ArrayList<Product> products);
    }

    interface GetMostSearchedStockListener extends FailedListener {
        void onMostSearchedStockLoaded(ArrayList<Product> products);
    }

    interface GetSettingsListener extends FailedListener {
        void onSettingsLoaded(ArrayList<Settings> settingsArrayList);
    }

    interface SettingsSaveListener extends FailedListener {
        void onSettingsSaved();
    }

    interface AddProductListener extends FailedListener {
        void onProductAdded();
    }

    interface UpdateProductListener extends FailedListener {
        void onProductUpdated();
    }

    interface UpsertDiscountListener extends FailedListener {
        void onDiscountUpdated();
    }

    interface OnCategoriesLoadedListener extends FailedListener {
        void onCategoriesLoaded(ArrayList<Category> categories);
    }

    interface CreateCategoryListener extends FailedListener {
        void onCategoryCreated(Category category);
    }

    interface UpdateCategoryListener extends FailedListener {
        void onCategoryUpdated(Category category);
    }

    interface OnOrderLoadedListener extends FailedListener {
        void onOrderLoaded(ArrayList<Order> orders);
    }

    interface OnScheduleLoadedListener extends FailedListener {
        void onScheduleLoaded(ArrayList<Schedule> schedules);
    }

    interface OnScheduleSavedListener extends FailedListener {
        void onScheduleSaved();
    }

    interface DeletePackageListener extends FailedListener {
        void onPackageDeleted();
    }

    interface OnPackagesLoadedListener extends FailedListener {
        void onPackagesLoaded(PackagesResponse packagesResponse);
    }

    interface AppUserInfoLoadedListener extends FailedListener {
        void onAppUserLoaded(AppUserResponse appUserResponse);
    }

    interface AppUserInfoUpdatedListener extends FailedListener {
        void onAppUserUpdated();
    }

    interface CheckoutListener extends FailedListener {
        void onOrderCreated(OrderCreateResponse orderCreateResponse);
    }

    interface PurchaseListLoadedListener extends FailedListener{
        void onPurchaseListLoaded(ArrayList<PurchaseList> purchaseLists);
    }

    interface DeleteSliderImageListener extends FailedListener {
        void onItemDeleted(String id, String type);
    }

    interface OrderDeliveryListener extends FailedListener{
        void onOrderDelivered();
    }*/

    interface DeleteItemListener extends FailedListener {
        void onItemDeleted(String id);
    }

    interface FailedListener {
        void onFailed(String message);
    }
}
