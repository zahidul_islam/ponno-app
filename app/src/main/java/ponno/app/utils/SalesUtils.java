package ponno.app.utils;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import ponno.app.data.local.DatabaseHelper;
import ponno.app.model.DailySale;
import ponno.app.model.DueBook;
import ponno.app.model.Product;
import ponno.app.model.Sale;

/**
 * Created by iGeorge on 22/12/17.
 */

public class SalesUtils {
    private Context context;
    private DatabaseHelper databaseHelper;


    private SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.getDefault());

    public static final long A_DAY = 24 * 60 * 60 * 1000;
    public static final long SEVEN_DAY = 7 * A_DAY;
    public static final long A_MONTH = 30 * A_DAY;
    public static final String START_TIME_OF_DAY = " 00:00:00";
    public static final String END_TIME_OF_DAY = " 23:59:59";

    public SalesUtils(Context context) {
        this.context = context;
        databaseHelper = DatabaseHelper.getInstance(context);
    }

    /**
     * getTotalSaleDuring method returns total sale within a given duration startDT and endDT
     *
     * @param startDT represents starting dateTime of the duration
     * @param endDT   represents ending dateTime of the duration
     * @return totalSale within the duration in double format
     */
    public double getTotalSaleDuring(String startDT, String endDT) {
        ArrayList<Sale> saleWithinRange = getSalesDuring(startDT, endDT);
        return getTotalSale(saleWithinRange);
    }

    public ArrayList<Sale> getSalesDuring(String startDT, String endDT) {
        return databaseHelper.getSalesDuring(context, startDT, endDT);
    }

    public ArrayList<DailySale> getDailySalesDuring(String startDT, String endDT) {
        ArrayList<DailySale> dailySales = new ArrayList<>();
        ArrayList<String> dates = ApplicationUtils.getUniqueDates(databaseHelper.getSaleDatesDuring(startDT, endDT, context));
        for (String aDate :
                dates) {
            ArrayList<Sale> sales = getSalesDuring(aDate + START_TIME_OF_DAY, aDate + END_TIME_OF_DAY);
            DailySale dailySale = new DailySale(aDate, getTotalSaleDuring(aDate + START_TIME_OF_DAY, aDate + END_TIME_OF_DAY), sales.size());
            dailySales.add(dailySale);
        }
        return dailySales;
    }

    public static double getTotalSale(ArrayList<Sale> sales) {
        double totalSale = 0;
        for (Sale aSale :
                sales) {
            double cost;
            if (aSale.getStatus().equalsIgnoreCase("in_due"))
                cost = aSale.getCashPaid();
            else
                cost = (aSale.getDiscountedCost() > 0.0) ? aSale.getDiscountedCost() : aSale.getTotalCost();

            totalSale += cost;
        }
        return totalSale;
    }

    public static double getTotalDue(ArrayList<Sale> sales) {
        double totalDue = 0;
        for (Sale aSale :
                sales) {
            totalDue += aSale.getPaymentDue();
        }
        return totalDue;
    }

    public static double getTotalSaleDuring(ArrayList<DailySale> dailySales) {
        double totalSale = 0;
        for (DailySale dailySale :
                dailySales) {
            totalSale += dailySale.getTotal_cost();
        }
        return totalSale;
    }

    public void updatePurchaseCounter(ArrayList<Product> products) {
        for (Product product :
                products) {
            Product purchasedProduct = databaseHelper.getProduct(product.getId());
            if (purchasedProduct != null) {
                int newCounter = purchasedProduct.getPurchase_count() + ApplicationUtils.convertToInt(product.getQuantity());
                databaseHelper.updatePurchaseCount(newCounter, purchasedProduct.getId());
            }
        }
    }

    public ArrayList<String> dueMobileNumbers(ArrayList<DueBook> dueBooks) {
        ArrayList<String> dueNumbers = new ArrayList<>();
        for (DueBook dueBook :
                dueBooks) {
            dueNumbers.add(dueBook.getMobile().replaceFirst("88", "").replaceFirst("0088", "").replaceFirst("/+88", ""));
        }
        return dueNumbers;
    }

    public ArrayList<String> dueNames(ArrayList<DueBook> dueBooks) {
        ArrayList<String> dueNames = new ArrayList<>();
        for (DueBook dueBook :
                dueBooks) {
            dueNames.add(dueBook.getName());
        }
        return dueNames;
    }

    public ArrayList<DueBook> getDueBooks() {
        return databaseHelper.getDueBook();
    }
}