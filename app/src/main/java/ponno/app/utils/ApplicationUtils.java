package ponno.app.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.Build;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import ponno.app.R;
import ponno.app.data.utils.ConnectivityInterceptor;
import ponno.app.fragments.DailySalesFragment;
import ponno.app.fragments.DueBookFragment;
import ponno.app.fragments.DueListFragment;
import ponno.app.fragments.SaleDetailsFragment;
import ponno.app.fragments.SaleInfoFragment;
import ponno.app.fragments.SalesFragment;
import ponno.app.model.DailySale;

import static android.content.Intent.ACTION_SENDTO;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public class ApplicationUtils {

    public static void setTextColor(TextView tvText, Context context, int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvText.setTextColor(context.getColor(resId));
        } else {
            tvText.setTextColor(context.getResources().getColor(resId));
        }
    }

    public static void setTextColor(TextInputEditText textInputEditText, Context context, int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textInputEditText.setTextColor(context.getColor(resId));
        } else {
            textInputEditText.setTextColor(context.getResources().getColor(resId));
        }
    }

    public static void setSeparateTextColor(TextView tvText, String color, String text, String coloredText) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvText.setText(Html.fromHtml(text + "<font color='" + color + "'>" + coloredText + "</font>", 0));
        } else {
            tvText.setText(Html.fromHtml(text + "<font color='" + color + "'>" + coloredText + "</font>"));
        }
    }

    public static void setHintTextColor(TextView tvText, Context context, int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvText.setHintTextColor(context.getColor(resId));
        } else {
            tvText.setHintTextColor(context.getResources().getColor(resId));
        }
    }

    public static void setHintTextColor(TextInputEditText tvText, Context context, int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvText.setHintTextColor(context.getColor(resId));
        } else {
            tvText.setHintTextColor(context.getResources().getColor(resId));
        }
    }

    public static Typeface getTypeface(int style, Context context) {
        switch (style) {
            case Typeface.BOLD:
                return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
            case Typeface.ITALIC:
                return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Italic.ttf");
            case Typeface.NORMAL:
                return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
            default:
                return Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");

        }
    }

    public static Animation loadZoomInOutAnimation(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.zoom_in_out);
    }

    public static Animation loadScaleAnimation(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.scale_product_auto_complete);
    }

    public static Animation loadSlideUpAnimation(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.slide_up);
    }

    public static Animation loadTranslateAnimation(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.translate_up);
    }

    public static OkHttpClient getClient(final Context context) {

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient
                .Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        if (ApplicationUtils.checkInternet(context)) {
                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build();
                        } else {
                            request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build();
                        }
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(new ConnectivityInterceptor(context))
                .addInterceptor(httpLoggingInterceptor)
                .build();
        return client;
    }

    private static Cache getCache(Context context) {
        File httpCacheDir;
        Cache cache = null;
        try {
            httpCacheDir = new File(context.getCacheDir(), "http");
            httpCacheDir.setReadable(true);
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            HttpResponseCache.install(httpCacheDir, httpCacheSize);
            cache = new Cache(httpCacheDir, httpCacheSize);
            Log.i("HTTP Caching", "HTTP response cache installation success");
        } catch (IOException e) {
            Log.i("HTTP Caching", "HTTP response cache installation failed:" + e);
        }
        return cache;
    }

    public static boolean checkInternet(Context context) {
        ConnectivityManager check = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = check.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    /**
     * Hides the keyboard.
     *
     * @param activity the Activity
     */
    public static void hideKeyboard(final Activity activity) {
        try {
            if (activity != null) {
                final InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                final View view = activity.getCurrentFocus();
                if (view != null) {
                    final IBinder binder = view.getWindowToken();
                    imm.hideSoftInputFromWindow(binder, 0);
                    imm.showSoftInputFromInputMethod(binder, 0);
                }
            }
        } catch (final Exception e) {
            Log.d(ApplicationUtils.class.getSimpleName(), "Exception to hide keyboard", e);
        }
    }

    /**
     * @param message          Alert message
     * @param context          Context of the alert dialog
     * @param positiveText     Positive Button's text
     * @param negativeText     Negative Button's text
     * @param positiveCallback Positive Button's listener
     * @param negativeCallback Negative Button's listener
     */

    public static void showAlertDialog(String message, Context context, String positiveText, String negativeText,
                                       DialogInterface.OnClickListener positiveCallback, DialogInterface.OnClickListener negativeCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(true);

        builder.setPositiveButton(positiveText, positiveCallback);
        if (!TextUtils.isEmpty(negativeText)) {
            builder.setNegativeButton(negativeText, negativeCallback);
        }

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void showMessageDialog(String message, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void callThisNumber(String number, Context context) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + number));
        TinyDB tinyDB = new TinyDB(context);
        if (checkCallPermission(context)) {
            context.startActivity(callIntent);
            tinyDB.putString(StaticData.DUE_NUMBER, "");
        } else {
            requestCallPermission((Activity) context);
            tinyDB.putString(StaticData.DUE_NUMBER, number);
        }
    }

    public static boolean checkPermission(Activity activity) {
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkCallPermission(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public static void requestCallPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, StaticData.PERMISSION_CALL_PHONE_REQUEST_CODE);
    }

    public static boolean checkMediaPermission(Context context) {
        int result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        int result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkStoragePermission(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    public static void requestMediaPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, StaticData.PERMISSION_MEDIA_REQUEST_CODE);
    }

    public static void requestLocationPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, StaticData.PERMISSION_LOCATION_REQUEST_CODE);
//        }
    }

    public static Intent getSendEmailIntent(String mailTo) {
        Intent intent = new Intent(ACTION_SENDTO);
        // intent.setType("text/plain");
        intent.setType("message/rfc822");
        if (mailTo == null) {
            mailTo = "";
        }
        intent.setData(Uri.parse("mailto:" + mailTo));
        return intent;
    }

    public static SpannableString getUnderlinedString(String text) {
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        return content;
    }

    public static String checkStringAndSet(String checkText, String defaultText) {
        if (checkText == null || TextUtils.isEmpty(checkText)) {
            return defaultText;
        } else {
            return checkText;
        }
    }

    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment);
        transaction.commit();
    }

    public static void addFragmentToActivityWithBackStack(@NonNull FragmentManager fragmentManager,
                                                          @NonNull Fragment fragment, int frameId, String title) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment);
        transaction.addToBackStack(title);
        transaction.commit();
    }

    public static double convertToDouble(String value) {
        double intValue;
        try {
            intValue = Double.parseDouble(value);
        } catch (NumberFormatException ex) {
            intValue = -0.00;
        } catch (NullPointerException ex) {
            intValue = -0.00;
        }
        return intValue;
    }

    public static int convertToInt(String value) {
        int intValue = 0;
        try {
            intValue = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            intValue = 0;
        } catch (NullPointerException ex) {
            intValue = 0;
        }
        return intValue;
    }

    public static String getRealPathFromURI(Uri uri, Context context) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static void createToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static String getStartingDateTime(String dateTime) {
        return dateTime.split(" ")[0] + " 00:00:00";
    }

    public static String getEndingDateTime(String dateTime) {
        return dateTime.split(" ")[0] + " 23:59:59";
    }


//    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
//        return dateFormat.format(System.currentTimeMillis()) + " 00:00:00";

    public static ArrayList<String> getUniqueDates(ArrayList<String> saleDatesDuring) {
        ArrayList<String> dates = new ArrayList<>();
        for (String aDateTime :
                saleDatesDuring) {
            if (!dates.contains(aDateTime.split(" ")[0])) {
                dates.add(aDateTime.split(" ")[0]);
            }
        }
        return dates;
    }

    public static boolean isToday(String dateTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String today = dateFormat.format(System.currentTimeMillis());
        if (today.equalsIgnoreCase(dateTime.split(" ")[0]))
            return true;
        return false;
    }

    public static Locale getBangleLocale() {
        return new Locale("bn", "BD");
    }

    public static String currencyFormat(double value) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(getBangleLocale());
        String localeValue = numberFormat.format(value);
        localeValue = localeValue.replace(".০০", "");
        return localeValue;
    }

    public static String getBengaliNumber(String stringValue) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(getBangleLocale());
        int value = convertToInt(stringValue);
        String localeValue = numberFormat.format(value);
        return localeValue;
    }

    public static String getTimeInBengali(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        try {
            long dateTime = dateFormat.parse(date).getTime();
            SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm", getBangleLocale());
            return timeFormat.format(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.split(" ")[1];
    }

    public static String getDateInBengali(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        try {
            long dateTime = dateFormat.parse(date).getTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", getBangleLocale());
            return simpleDateFormat.format(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.split(" ")[0];
    }

    public static ArrayList<Object> sortDescending(Object[] objects) {
        Arrays.sort(objects, Collections.reverseOrder());
        return null;
    }

    public static void removeFragments(FragmentManager fragmentManager) {
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            int rootCounter = 0;
            for (Fragment fragment : fragments) {
                if (fragment instanceof DailySalesFragment ||
                        fragment instanceof DueBookFragment ||
                        fragment instanceof DueListFragment ||
                        fragment instanceof SaleDetailsFragment ||
                        fragment instanceof SalesFragment
                        ) {
                    fragmentManager.beginTransaction().remove(fragment).commit();
                }else if(fragment instanceof SaleInfoFragment){
                    rootCounter++;
                    if(rootCounter>1)
                        fragmentManager.beginTransaction().remove(fragment).commit();
                    Log.e("rootCounter","Root counted = "+rootCounter);
                }
            }
        }
        fragmentManager.popBackStack();
    }

    public static String addCountryPrefix(String number) {

        if (number != null && android.text.TextUtils.isDigitsOnly(number)) {
            if (number.length() > 2) {
                if (number.substring(0, 2).equals("88")) {
                    return number;
                } else
                    return "88" + number;
            } else
                return "88";
        } else
            return "88";

    }
}
