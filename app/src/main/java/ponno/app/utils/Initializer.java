package ponno.app.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public class Initializer {

    public static EditText initialize(EditText editText, int resId, Activity activity) {
        editText = (EditText) activity.findViewById(resId);
        editText.addTextChangedListener(getTextWatcher(editText));
        return editText;
    }

    public static Button initialize(Button button, int resId, Activity activity) {
        button = (Button) activity.findViewById(resId);
        return button;
    }


    private static TextWatcher getTextWatcher(final EditText editText) {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        return textWatcher;
    }

    public static TextView initialize(TextView textView, int resId, Activity activity) {
        textView = (TextView) activity.findViewById(resId);
        return textView;
    }

    public static ProgressBar initialize(ProgressBar progressBar, int resId, Activity activity) {
        progressBar = (ProgressBar) activity.findViewById(resId);
        return progressBar;
    }

    public static Toolbar initialize(Toolbar toolbar, int resId, Activity activity) {
        toolbar = (Toolbar) activity.findViewById(resId);
        return toolbar;
    }

    public static TextView initialize(int resId, Toolbar toolbar) {
        TextView textView = (TextView) toolbar.findViewById(resId);
        return textView;
    }

    public static LinearLayout initialize(int resId, Activity activity) {
        LinearLayout linearLayout = (LinearLayout) activity.findViewById(resId);
        return linearLayout;
    }

    public static RecyclerView initialize(int resId, Activity activity, boolean isGrid, int column, int orientation) {
        RecyclerView recyclerView = (RecyclerView) activity.findViewById(resId);
        recyclerView.setHasFixedSize(true);
        if (isGrid) {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, column);
            recyclerView.setLayoutManager(gridLayoutManager);
            return recyclerView;
        } else {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
            linearLayoutManager.setOrientation(orientation);
            recyclerView.setLayoutManager(linearLayoutManager);
            return recyclerView;
        }
    }

    public static RecyclerView initialize(int resId, View view, Context context, boolean isGrid, int column, int orientation) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(resId);
        recyclerView.setHasFixedSize(true);
        if (isGrid) {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(context, column);
            recyclerView.setLayoutManager(gridLayoutManager);
            return recyclerView;
        } else {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(orientation);
            recyclerView.setLayoutManager(linearLayoutManager);
            return recyclerView;
        }
    }

}
