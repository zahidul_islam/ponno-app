package ponno.app.utils;

/**
 * Created by George on 27-July-2016.
 */
public class StaticData {

    //    public static final String BASE_URL_IMAGE = "http://fangoo.montorlabs.com/";
    public static final String BASE_URL_IMAGE = "http://fangoo.starlineevents.nl/";

    /**
     * These here is the must change static data for each application
     **/

    public static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    //Session
    public static final String PREFERENCE_NAME = "app_preference";
    public static final String LOGIN_STATE = "login_state";

    //User
    public static final String USER = "USER";
    public static final String USER_LIST = "user_list";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "email";
    public static final String USER_ROLE = "role";
    public static final String USER_PRIVILEGE = "privilege";
    public static final String USER_TOKEN = "token";
    public static final String USER_DEFAULT_PRIVILEGE_LIST = "default_privilege_list";
    public static final String CATEGORIES = "categories";
    public static final String PAYMENT_URL = "payment_url";
    public static final String REDIRECT_URL = "redirect_url";
    public static String PRODUCTS = "products";

    public static final String IS_DELIVERY = "is_delivery";

    //Inventory Management
    public static final String INVENTORY_OR_DISCOUNT_KEY = "inventory_or_discount_key";
    public static final String INVENTORY_VALUE = "inventory";
    public static final String DISCOUNT_VALUE = "discount";
    public static final String PURCHASE_VALUE = "purchase";

    //Firebase
    public static final String FCM_TOKEN = "fcm_token";
    public static final String NOTIFICATION_MESSAGE = "message";
    public static final String TITLE = "title";

    //Settings item names
    public static final String TYPE_MAIN_SLIDER = "main_slider";
    public static final String TYPE_SECONDARY_SLIDER = "secondary_slider";
    public static final String TYPE_NEW_ARRIVAL = "new_arrivals";
    public static final String TYPE_BEST_SELLER = "best_sellers";
    public static final String TYPE_MOST_PURCHASED = "most_purchased";
    public static final String TYPE_MOST_SEARCHED = "most_searched";
    public static final String TYPE_LOGO = "logo";
    public static final String TYPE_CART_SETTINGS = "cart_settings";

    //Common activity utils
    public static final String IS_SEARCH = "is_search";

    //Orders
    public static final int PERMISSION_CALL_PHONE_REQUEST_CODE = 12;
    public static final int PERMISSION_LOCATION_REQUEST_CODE = 13;
    public static final int PERMISSION_MEDIA_REQUEST_CODE = 14;
    public static final int PERMISSION_CAMERA_REQUEST_CODE = 15;
    public static final int PERMISSION_GALLERY_REQUEST_CODE = 16;
    public static final String DUE_NUMBER = "due_number";

    //PRODUCT
    public static final String PRODUCT_KEY = "product_to_display";
}
