package ponno.app.utils;

/**
 * Created by Fahim Ahmed on 14-12-16.
 */

public interface SwipeListener {
    void onSwipeRight(int position);

    void onSwipeLeft(int position);

}
