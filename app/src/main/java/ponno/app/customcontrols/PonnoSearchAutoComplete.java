package ponno.app.customcontrols;

import android.content.Context;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;

/**
 * Created by Zahidul Islam on 19-11-16.
 */
public class PonnoSearchAutoComplete extends AppCompatAutoCompleteTextView {
    public PonnoSearchAutoComplete(Context context) {
        super(context);
    }

    public PonnoSearchAutoComplete(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
    }

    public PonnoSearchAutoComplete(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
    }

    // this is how to disable AutoCompleteTextView filter
    @Override
    protected void performFiltering(final CharSequence text, final int keyCode) {
        String filterText = "";
        super.performFiltering(filterText, keyCode);
    }

    /*
     * after a selection we have to capture the new value and append to the existing text
     */
    @Override
    protected void replaceText(final CharSequence text) {
        super.replaceText(text);
    }

}
