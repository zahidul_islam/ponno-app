package ponno.app.customcontrols;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;

import java.util.ArrayList;

import ponno.app.SaleActivity;
import ponno.app.adapters.ProductAutoCompleteAdapter;
import ponno.app.data.local.DatabaseHelper;
import ponno.app.model.Product;
import ponno.app.view.SaleView;

/**
 * Created by iGeorge on 12/2/18.
 */

public class PonnoAutoCompleteTextChangeListener implements TextWatcher {

    Context context;
    SaleView saleView;
    DatabaseHelper databaseHelper;
    long TIME = 1 * 2000;

    public PonnoAutoCompleteTextChangeListener(Context context) {
        this.context = context;
        saleView = (SaleView) context;
        databaseHelper = DatabaseHelper.getInstance(context);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence != null && charSequence.length() > 0) {
            saleView.checkSearchProgress(true);
            new Thread(new Runnable() {
                @Override
                public void run() {
//                    ArrayList<Product> suggestions = searchProductFromCSV(searchKey);
                    final ArrayList<Product> suggestions = databaseHelper.getProductsByKey(charSequence.toString());
//                    filterResults.values = suggestions;
//                    filterResults.count = suggestions.size();
                    ((SaleActivity) saleView).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            saleView.hideProgress();
                            saleView.setAutoCompleteAdapter(new ProductAutoCompleteAdapter(context, suggestions));
                        }
                    });
                }
            }).start();
        }

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
