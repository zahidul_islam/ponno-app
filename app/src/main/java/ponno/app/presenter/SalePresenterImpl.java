package ponno.app.presenter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;

import ponno.app.R;
import ponno.app.SaleActivity;
import ponno.app.adapters.ProductAutoCompleteAdapter;
import ponno.app.adapters.ProductSaleAdapter;
import ponno.app.data.local.DatabaseHelper;
import ponno.app.fragments.QuantityPriceDialogFragment;
import ponno.app.fragments.SaleFragment;
import ponno.app.model.Product;
import ponno.app.model.Sale;
import ponno.app.utils.ApplicationUtils;
import ponno.app.view.SaleView;

/**
 * Created by iGeorge on 9/21/17.
 */

public class SalePresenterImpl implements SalePresenter {
    private Context context;
    private SaleView saleView;
    //    private APIInteractor apiInteractor;
    private DatabaseHelper databaseHelper;
    private FragmentManager fragmentManager;
    private SaleFragment saleFragment;

    private ArrayList<Product> mostPurchasedProducts = new ArrayList<>();
    private ArrayList<Product> recentSaledProducts = new ArrayList<>();
    private ArrayList<Product> saleProducts = new ArrayList<>();
    private double total;

    private ProductSaleAdapter productSaleAdapter;

    public SalePresenterImpl(Context context, SaleView saleView, FragmentManager fragmentManager) {
        this.context = context;
        this.saleView = saleView;
//        apiInteractor = new APIInteractorImpl(context);
        databaseHelper = DatabaseHelper.getInstance(context);
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void initializePresenter() {
        saleView.initUI();
        saleView.initActionbar();
        saleView.setListeners();
        saleView.showProgress(context.getString(R.string.loading_progress));
        if (!databaseHelper.checkProductsTable()) {
//            apiInteractor.getAllProducts(this);
            new ConvertCSVToSQLiteTask().execute();
        } else {
            new LoadSuggestionsTask().execute();
        }
    }

    private class ConvertCSVToSQLiteTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            databaseHelper.insertDataFromCSV(context);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new LoadSuggestionsTask().execute();
        }
    }

    private class LoadSuggestionsTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
//            mostPurchasedProducts = databaseHelper.getPurchasedProducts();
            ArrayList<Sale> recentSales = databaseHelper.getRecentSales();
            if (recentSales != null && recentSales.size() > 0)
                for (Sale sale :
                        recentSales) {
                    recentSaledProducts.addAll(sale.getProducts());
                }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            saleView.hideProgress();
            saleView.setSuggestions(recentSaledProducts, mostPurchasedProducts);
            saleView.setAutoCompleteAdapter(new ProductAutoCompleteAdapter(context, new ArrayList<Product>()));
        }
    }
//    private Thread convertCSVFileInSQLite = new Thread(new Runnable() {
//        @Override
//        public void run() {
//
//            if (saleView != null)
//                ((SaleActivity) saleView).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        loadSuggestionProducts.start();
////                            homeView.setAutoCompleteAdapter(new ProductAutoCompleteAdapter(context, new ArrayList<Product>()));
//                    }
//                });
//        }
//    });
//
//    private Thread loadSuggestionProducts = new Thread(new Runnable() {
//        @Override
//        public void run() {
//            mostPurchasedProducts = databaseHelper.getPurchasedProducts();
//            ArrayList<Sale> recentSales = databaseHelper.getRecentSales();
//            if (recentSales != null && recentSales.size() > 0)
//                for (Sale sale :
//                        recentSales) {
//                    recentSaledProducts.addAll(sale.getProducts());
//                }
//            if (saleView != null)
//                ((SaleActivity) saleView).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                    }
//                });
//        }
//    });

    @Override
    public void productSelected(Product product) {
        saleView.showSuggestions(false);
        product.setQuantity("10");
        if (saleFragment == null) {
            saleFragment = SaleFragment.newInstance(product, this);
            ApplicationUtils.addFragmentToActivity(fragmentManager, saleFragment, R.id.fl_sale_fragments);
        } else {
            saleFragment.openQuantityAndPriceSelectionDialog(product);
        }
    }

    @Override
    public void openQuantityAndPriceSelectionDialog(Product product) {
        QuantityPriceDialogFragment quantityPriceDialogFragment = QuantityPriceDialogFragment.newInstance(product, this);
        quantityPriceDialogFragment.setCancelable(true);
        quantityPriceDialogFragment.show(fragmentManager, QuantityPriceDialogFragment.class.getSimpleName());    }

    @Override
    public void addProduct(Product product) {
        if (saleProducts == null) {
            saleProducts = new ArrayList<>();
            saleProducts.add(product);
        } else {
            boolean isAdded = false;
            for (Product aProduct :
                    saleProducts) {
                if (aProduct.getId().equalsIgnoreCase(product.getId())) {
                    aProduct.setQuantity(product.getQuantity());
                    aProduct.setUnit_price(product.getUnit_price());
                    isAdded = true;
                    break;
                }
            }
            if (!isAdded)
                saleProducts.add(product);
        }
        if (productSaleAdapter != null) {
            productSaleAdapter.notifyDataSetChanged();
        } else {
            productSaleAdapter = new ProductSaleAdapter(saleProducts, context, this);
            saleView.setSaleAdapter(productSaleAdapter);
        }
        calculateAndSetTotalCost();
    }

    @Override
    public void completeSale(final Sale sale) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (databaseHelper.checkInvoice(sale.getInvoiceID()))
                    databaseHelper.updateSale(sale);
                else
                    databaseHelper.createASale(sale);
                ((SaleActivity) saleView).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        saleView.gotoInvoice(sale.getInvoiceID());
                    }
                });
            }
        }).start();
    }

    @Override
    public void calculateAndSetTotalCost() {
        total = 0.0;
        for (Product product :
                saleProducts) {
            double productTotal = ApplicationUtils.convertToInt(product.getQuantity()) * ApplicationUtils.convertToDouble(product.getUnit_price());
            total += productTotal;
        }
        saleView.updateTotalCost(ApplicationUtils.currencyFormat(total));
    }

    /**
     * This section is not used for now
     **/

    @Override
    public void onProductLoaded(ArrayList<Product> products) {
        for (int i = 0; i < products.size(); i++) {
            databaseHelper.insertProductInCart(products.get(i));
        }
        saleView.hideProgress();
        saleView.setAutoCompleteAdapter(new ProductAutoCompleteAdapter(context, new ArrayList<Product>()));
    }

    @Override
    public void onFailed(String message) {
        saleView.hideProgress();
        ApplicationUtils.createToastMessage(context, message);
    }

    /** **/

    @Override
    public void removeInvoice() {
//        ApplicationUtils.addFragmentToActivity(fragmentManager, SaleInfoFragment.newInstance(this), R.id.fl_sale_fragments);
//        saleFragment = null;
        saleView.showSuggestions(true);
        if (saleFragment != null) {
            fragmentManager.beginTransaction().remove(saleFragment).commit();
            saleFragment = null;
        }
    }

}

