package ponno.app.presenter;

import ponno.app.data.server.APIInteractor;
import ponno.app.model.Product;
import ponno.app.model.Sale;

/**
 * Created by iGeorge on 9/21/17.
 */

public interface InvoicePresenter extends CommonActivityPresenter {

    void completeInvoice(String cash, String back);

    void updateDueBook(String name, String mobile);

    void setDiscount(int discount, String discountType);

    void updateSaleInfo(double cashPaid);
}
