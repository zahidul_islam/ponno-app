package ponno.app.presenter;

import ponno.app.data.server.APIInteractor;
import ponno.app.model.Product;
import ponno.app.model.Sale;

/**
 * Created by iGeorge on 9/21/17.
 */

public interface SalePresenter extends CommonActivityPresenter, APIInteractor.GetProductListener {
    void productSelected(Product product);

    void openQuantityAndPriceSelectionDialog(Product product);

    void addProduct(Product product);

    void removeInvoice();

    void completeSale(Sale sale);

    void calculateAndSetTotalCost();
}
