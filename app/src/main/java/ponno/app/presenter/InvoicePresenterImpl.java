package ponno.app.presenter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import ponno.app.InvoiceActivity;
import ponno.app.R;
import ponno.app.adapters.FragmentViewPagerAdapter;
import ponno.app.data.local.DatabaseHelper;
import ponno.app.fragments.CashFragment;
import ponno.app.fragments.DueFragment;
import ponno.app.model.DueBook;
import ponno.app.model.Sale;
import ponno.app.utils.ApplicationUtils;
import ponno.app.utils.SalesUtils;
import ponno.app.view.InvoiceView;

/**
 * Created by iGeorge on 10/31/17.
 */

public class InvoicePresenterImpl implements InvoicePresenter {
    private Context context;
    private InvoiceView invoiceView;
    private Sale sale;
    private DatabaseHelper databaseHelper;
    private FragmentManager fragmentManager;
    private SalesUtils salesUtils;

    private CashFragment cashFragment;
    private DueFragment dueFragment;

    public InvoicePresenterImpl(Context context, InvoiceView invoiceView, String invoiceID, FragmentManager fragmentManager) {
        this.context = context;
        this.invoiceView = invoiceView;
        this.fragmentManager = fragmentManager;
        databaseHelper = DatabaseHelper.getInstance(context);
        sale = databaseHelper.getSale(invoiceID);
        salesUtils = new SalesUtils(context);
        cashFragment = CashFragment.newInstance(sale, this);
        dueFragment = DueFragment.newInstance(sale, this);
    }

    @Override
    public void initializePresenter() {
        invoiceView.initUI();
        invoiceView.initActionbar();
        invoiceView.setListeners();
        FragmentViewPagerAdapter fragmentViewPagerAdapter = new FragmentViewPagerAdapter(fragmentManager);
        fragmentViewPagerAdapter.addFragment(cashFragment, context.getString(R.string.cash_payment));
        fragmentViewPagerAdapter.addFragment(dueFragment, context.getString(R.string.due_payment));
        invoiceView.setSale(sale, fragmentViewPagerAdapter);
    }

    /**
     * @param cash represents money paid by customer
     * @param back represents money to return
     *             <p>
     *             completeInvoice method completes an invoice.
     *             It calculates due subtracting cash from total cost or discounted cost (if available).
     *             When due is available pager will navigate to Due Fragment
     *             Unless Invoice will be saved with status completed
     */
    @Override
    public void completeInvoice(String cash, String back) {
        if (TextUtils.isEmpty(cash)) {
            sale.setCashPaid(sale.getDiscount() != null ? sale.getDiscountedCost() : sale.getTotalCost());
            sale.setCashBack(0.0);
        } else {
            if (sale.getCashPaid() > 0)
                sale.setCashPaid(sale.getCashPaid() + ApplicationUtils.convertToDouble(cash.replace(" ৳", "")));
            else
                sale.setCashPaid(ApplicationUtils.convertToDouble(cash.replace(" ৳", "")));
            sale.setCashBack(ApplicationUtils.convertToDouble(back.replace(" ৳", "")));
        }
        double due = (sale.getDiscountedCost() > 0.0) ? sale.getDiscountedCost() - sale.getCashPaid() : sale.getTotalCost() - sale.getCashPaid();
        sale.setPaymentDue(due);
        if (due > 0.5) {
            sale.setStatus(context.getString(R.string.sale_on_due));
            dueFragment.updateSale(sale);
            invoiceView.openDueForm();
        } else {
            sale.setStatus(context.getString(R.string.sale_completed));
            invoiceView.showProgress(context.getString(R.string.completing_sale));
            updateSaleThread.start();
        }
        invoiceView.updateSaleInfo(sale.getDiscountedCost() > 0 ? sale.getDiscountedCost() : sale.getTotalCost(), sale.getCashPaid(), sale.getPaymentDue());

    }

    @Override
    public void updateDueBook(String name, String mobile) {
        if (TextUtils.isEmpty(sale.getCustomerMobileNumber())) {
            sale.setCustomerName(processName(name));
            sale.setCustomerMobileNumber(processNumber(mobile));
        }
        updateSaleThread.start();
    }

    private String processName(String name) {
        if (name != null && !TextUtils.isEmpty(name))
            return name;
        else
            return context.getString(R.string.anonymous);
    }

    private String processNumber(String mobile) {
        if (mobile == null || TextUtils.isEmpty(mobile))
            return "";
        else
            return ApplicationUtils.addCountryPrefix(mobile);
    }

    private Thread updateSaleThread = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                databaseHelper.updateSale(sale);
                salesUtils.updatePurchaseCounter(sale.getProducts());
                DueBook dueBook = databaseHelper.getDue(sale.getCustomerMobileNumber(), sale.getCustomerName());
                if (dueBook != null) {
                    dueBook.setPaymentDue(databaseHelper.getDueAmount(sale.getCustomerMobileNumber()));
                    databaseHelper.updateDue(dueBook);
                } else {
                    if (sale.getStatus().equalsIgnoreCase(context.getString(R.string.sale_on_due)))
                        databaseHelper.addSaleToDueTable(sale);
                }
                if (context != null && invoiceView != null)
                    ((InvoiceActivity) invoiceView).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            invoiceView.hideProgress();
                            invoiceView.completeInvoice();
                        }
                    });
            } catch (Exception ex) {
                ((InvoiceActivity) invoiceView).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        invoiceView.hideProgress();
                        ApplicationUtils.showMessageDialog(context.getString(R.string.update_failed), context);
                    }
                });
            }
        }
    });

    @Override
    public void setDiscount(int discount, String discountType) {
        switch (discountType) {
            case "৳":
                sale.setDiscountedCost(sale.getTotalCost() - discount);
                break;
            case "%":
                sale.setDiscountedCost(calculateDiscountedCost(discount));
                break;
        }
        sale.setDiscount(ApplicationUtils.getBengaliNumber(discount + "") + " " + discountType);
        invoiceView.updateTotalCost(sale.getDiscountedCost());
        if (cashFragment != null) {
            cashFragment.reviewSaleInfo();
        }
    }
//
//    @Override
//    public void updateSale(Sale sale) {
//        this.sale = sale;
//        invoiceView.updateSaleInfo(sale);
//    }

    @Override
    public void updateSaleInfo(double cash) {
        double totalCost, totalDue, totalPaid;
        if (sale.getCashPaid() > 0)
            totalPaid = sale.getCashPaid() + cash;
        else
            totalPaid = cash;
        if (TextUtils.isEmpty(sale.getDiscount())) {
            totalCost = sale.getTotalCost();
        } else {
            totalCost = sale.getDiscountedCost();
        }
        if (totalPaid < totalCost)
            totalDue = totalCost - totalPaid;
        else
            totalDue = 0;
        invoiceView.updateSaleInfo(totalCost, totalPaid, totalDue);
        if (totalPaid > totalCost)
            cashFragment.updateCashback(ApplicationUtils.currencyFormat(totalPaid - totalCost));
    }

    private double calculateDiscountedCost(int discount) {
        double costWithoutDiscount = sale.getTotalCost();
        double discountedAmount = (costWithoutDiscount * discount) / 100;
        double discountedCost = sale.getTotalCost() - discountedAmount;
        return discountedCost;
    }
}
