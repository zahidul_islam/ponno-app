package ponno.app.presenter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import ponno.app.HomeActivity;
import ponno.app.R;
import ponno.app.data.local.DatabaseHelper;
import ponno.app.fragments.DailySalesFragment;
import ponno.app.fragments.DueBookFragment;
import ponno.app.fragments.DueListFragment;
import ponno.app.fragments.SaleDetailsFragment;
import ponno.app.fragments.SaleInfoFragment;
import ponno.app.fragments.SalesFragment;
import ponno.app.model.DailySale;
import ponno.app.model.DueBook;
import ponno.app.model.Product;
import ponno.app.model.Sale;
import ponno.app.utils.ApplicationUtils;
import ponno.app.utils.SalesUtils;
import ponno.app.view.HomeView;

/**
 * Created by iGeorge on 9/21/17.
 */

public class HomePresenterImpl implements HomePresenter {
    private Context context;
    private HomeView homeView;
    //    private APIInteractor apiInteractor;
    private SalesUtils salesUtils;
    private DatabaseHelper databaseHelper;
    private FragmentManager fragmentManager;
    private SalesFragment searchSaleFragment;

    public HomePresenterImpl(Context context, HomeView homeView, FragmentManager fragmentManager) {
        this.context = context;
        this.homeView = homeView;
//        apiInteractor = new APIInteractorImpl(context);
        salesUtils = new SalesUtils(context);
        databaseHelper = DatabaseHelper.getInstance(context);
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void initializePresenter() {
        homeView.initUI();
        homeView.setListeners();
        homeView.showProgress(context.getString(R.string.loading_progress));
        if (!databaseHelper.checkProductsTable()) {
//            apiInteractor.getAllProducts(this);
            convertCSVIntoSQLite.start();
        } else {
            homeView.hideProgress();
//            homeView.setAutoCompleteAdapter(new ProductAutoCompleteAdapter(context, new ArrayList<Product>()));
            ApplicationUtils.addFragmentToActivity(fragmentManager, SaleInfoFragment.newInstance(this), R.id.fl_sale_fragments);
        }
    }

    private Thread convertCSVIntoSQLite = new Thread(new Runnable() {
        @Override
        public void run() {
            databaseHelper.insertDataFromCSV(context);
            ((HomeActivity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (homeView != null) {
                        homeView.hideProgress();
                        ApplicationUtils.addFragmentToActivity(fragmentManager, SaleInfoFragment.newInstance(HomePresenterImpl.this), R.id.fl_sale_fragments);
//                            homeView.setAutoCompleteAdapter(new ProductAutoCompleteAdapter(context, new ArrayList<Product>()));
                    }
                }
            });
        }
    });

    @Override
    public void onProductLoaded(ArrayList<Product> products) {
        for (int i = 0; i < products.size(); i++) {
            databaseHelper.insertProductInCart(products.get(i));
        }
        homeView.hideProgress();
//        homeView.setAutoCompleteAdapter(new ProductAutoCompleteAdapter(context, new ArrayList<Product>()));
    }

    @Override
    public void onFailed(String message) {
        homeView.hideProgress();
        ApplicationUtils.createToastMessage(context, message);
    }

    @Override
    public void showSaleAt(String dateTime) {
        ArrayList<Sale> saleToday = salesUtils.getSalesDuring(ApplicationUtils.getStartingDateTime(dateTime), ApplicationUtils.getEndingDateTime(dateTime));
        if (saleToday != null && saleToday.size() > 0) {
            String tagLine;
            if (ApplicationUtils.isToday(dateTime)) {
                tagLine = context.getString(R.string.sale_today);
            } else {
                tagLine = ApplicationUtils.getDateInBengali(dateTime);
            }
            Collections.reverse(saleToday);
            ApplicationUtils.addFragmentToActivityWithBackStack(fragmentManager, SalesFragment.newInstance(this, tagLine, saleToday), R.id.fl_sale_fragments, context.getString(R.string.sales_list));
        } else {
            ApplicationUtils.showMessageDialog(context.getString(R.string.no_sale_found), context);
        }
    }

    @Override
    public void showSalesDuring(String startDT, String endDT, String tagLine) {
        ArrayList<DailySale> dailySales = salesUtils.getDailySalesDuring(startDT, endDT);
        if (dailySales != null && dailySales.size() > 0) {
            Collections.reverse(dailySales);
            ApplicationUtils.addFragmentToActivityWithBackStack(fragmentManager, DailySalesFragment.newInstance(this, tagLine, dailySales), R.id.fl_sale_fragments, context.getString(R.string.sale_today));
        } else {
            ApplicationUtils.showMessageDialog(context.getString(R.string.no_sale_found), context);
        }
    }

    @Override
    public void viewSales(Sale sale) {
        ApplicationUtils.addFragmentToActivityWithBackStack(fragmentManager, SaleDetailsFragment.newInstance(sale, this), R.id.fl_sale_fragments, context.getString(R.string.sale_details));
    }

    @Override
    public void searchInvoice(CharSequence query) {
        homeView.checkSearchProgress(true);
        new SearchInvoiceTask().execute(query);
    }

    @Override
    public void openDueBook() {
        ApplicationUtils.removeFragments(fragmentManager);
        homeView.showProgress(context.getString(R.string.loading_progress));
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final ArrayList<DueBook> dueBooks = databaseHelper.getDueBook();
                    if (context != null)
                        ((HomeActivity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                homeView.hideProgress();
                                if (dueBooks != null & dueBooks.size() > 0)
                                    ApplicationUtils.addFragmentToActivityWithBackStack(fragmentManager, DueBookFragment.newInstance(dueBooks, HomePresenterImpl.this), R.id.fl_sale_fragments, context.getString(R.string.due_book));
                                else
                                    ApplicationUtils.showMessageDialog(context.getString(R.string.no_dues), context);
                            }
                        });

                } catch (Exception ignored) {
                    if (context != null)
                        ((HomeActivity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                homeView.hideProgress();
                                ApplicationUtils.showMessageDialog(context.getString(R.string.no_dues), context);
                            }
                        });
                }
            }
        }).start();
    }

    @Override
    public void showDues(final DueBook dueBook) {
        homeView.showProgress(context.getString(R.string.loading_progress));
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final ArrayList<Sale> salesOnDue = databaseHelper.getSaleOnDue(dueBook);
                    if (context != null)
                        ((HomeActivity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                homeView.hideProgress();
                                if (salesOnDue != null & salesOnDue.size() > 0)
                                    ApplicationUtils.addFragmentToActivityWithBackStack(fragmentManager, DueListFragment.newInstance(salesOnDue, HomePresenterImpl.this), R.id.fl_sale_fragments, context.getString(R.string.due_list));
                                else
                                    ApplicationUtils.showMessageDialog(context.getString(R.string.cannot_find_dues), context);
                            }
                        });

                } catch (Exception ignored) {
                    if (context != null)
                        ((HomeActivity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ApplicationUtils.showMessageDialog(context.getString(R.string.cannot_find_dues), context);
                            }
                        });
                }
            }
        }).start();
    }

    private class SearchInvoiceTask extends AsyncTask<CharSequence, Void, ArrayList<Sale>> {

        @Override
        protected ArrayList<Sale> doInBackground(CharSequence... charSequences) {
            String queryText = charSequences[0].toString();
            return databaseHelper.getSales(context, queryText);
        }

        @Override
        protected void onPostExecute(ArrayList<Sale> sales) {
            super.onPostExecute(sales);
            homeView.checkSearchProgress(false);
            if (searchSaleFragment == null) {
                Collections.sort(sales, new Comparator<Sale>() {
                    @Override
                    public int compare(Sale sale1, Sale sale2) {
                        try {
                            SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
                            return simpleDateTimeFormat.parse(sale1.getDate()).compareTo(simpleDateTimeFormat.parse(sale2.getDate()));
                        } catch (ParseException e) {
                            return 0;
                        }
                    }
                });
                searchSaleFragment = SalesFragment.newInstance(HomePresenterImpl.this, context.getString(R.string.possible_sales), sales);
                ApplicationUtils.addFragmentToActivityWithBackStack(fragmentManager, searchSaleFragment, R.id.fl_sale_fragments, context.getString(R.string.sale_today));
            } else {
                searchSaleFragment.updateSales(sales);
            }

        }
    }

}
