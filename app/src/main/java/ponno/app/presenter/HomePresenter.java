package ponno.app.presenter;

import ponno.app.data.server.APIInteractor;
import ponno.app.model.DueBook;
import ponno.app.model.Product;
import ponno.app.model.Sale;

/**
 * Created by iGeorge on 9/21/17.
 */

public interface HomePresenter extends CommonActivityPresenter, APIInteractor.GetProductListener {

    void showSaleAt(String date);

    void showSalesDuring(String startDT, String endDT, String tagLine);

    void viewSales(Sale sale);

    void searchInvoice(CharSequence query);

    void openDueBook();

    void showDues(DueBook dueBook);

}
