package ponno.app.model;

import java.util.ArrayList;

/**
 * Created by iGeorge on 11/1/17.
 */

public class Sale {
    private String invoiceID;
    private ArrayList<Product> products;
    private double totalCost;
    private String discount;
    private double discountedCost;
    private double cashPaid;
    private double cashBack;
    private double paymentDue;
    private String customerName;
    private String customerMobileNumber;
    private String status;
    private String date;


    public Sale(String invoiceID, ArrayList<Product> products, double totalCost, String status, String date) {
        this.invoiceID = invoiceID;
        this.products = products;
        this.totalCost = totalCost;
        this.status = status;
        this.date = date;
    }

    public Sale(String invoiceID, ArrayList<Product> products, double totalCost, String discount, double discountedCost, double cashPaid, double cashBack, double paymentDue, String status, String date) {
        this.invoiceID = invoiceID;
        this.products = products;
        this.totalCost = totalCost;
        this.discount = discount;
        this.discountedCost = discountedCost;
        this.cashPaid = cashPaid;
        this.cashBack = cashBack;
        this.paymentDue = paymentDue;
        this.status = status;
        this.date = date;
    }

    public Sale(String invoiceID, ArrayList<Product> products) {
        this.invoiceID = invoiceID;
        this.products = products;
    }

    public Sale() {

    }

    public Sale(String invoiceID, ArrayList<Product> products, double totalCost, String discount, double discountedCost, double cashPaid, double cashBack, double paymentDue, String customerName, String customerMobileNumber, String status, String date) {
        this.invoiceID = invoiceID;
        this.products = products;
        this.totalCost = totalCost;
        this.discount = discount;
        this.discountedCost = discountedCost;
        this.cashPaid = cashPaid;
        this.cashBack = cashBack;
        this.paymentDue = paymentDue;
        this.customerName = customerName;
        this.customerMobileNumber = customerMobileNumber;
        this.status = status;
        this.date = date;
    }

    public Sale(String invoiceID, double paymentDue, String customerName, String customerMobileNumber) {
        this.invoiceID = invoiceID;
        this.paymentDue = paymentDue;
        this.customerName = customerName;
        this.customerMobileNumber = customerMobileNumber;
    }

    public Sale(String invoiceID, String status, String date, double totalCost, ArrayList<Product> products) {
        this.invoiceID = invoiceID;
        this.status = status;
        this.date = date;
        this.totalCost = totalCost;
        this.products = products;
    }

    public Sale(String invoiceId, String status, String date, double totalCost, double discountedCost) {
        this.invoiceID = invoiceId;
        this.status = status;
        this.date = date;
        this.totalCost = totalCost;
        this.discountedCost = discountedCost;
    }

    public String getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(String invoiceID) {
        this.invoiceID = invoiceID;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public double getDiscountedCost() {
        return discountedCost;
    }

    public void setDiscountedCost(double discountedCost) {
        this.discountedCost = discountedCost;
    }

    public double getCashPaid() {
        return cashPaid;
    }

    public void setCashPaid(double cashPaid) {
        this.cashPaid = cashPaid;
    }

    public double getCashBack() {
        return cashBack;
    }

    public void setCashBack(double cashBack) {
        this.cashBack = cashBack;
    }

    public double getPaymentDue() {
        return paymentDue;
    }

    public void setPaymentDue(double paymentDue) {
        this.paymentDue = paymentDue;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
