package ponno.app.model;

/**
 * Created by iGeorge on 23/12/17.
 */

public class DailySale {
    private String date;
    private double total_cost;
    private int number_of_invoice;

    public DailySale(String date, double total_cost, int number_of_invoice) {
        this.date = date;
        this.total_cost = total_cost;
        this.number_of_invoice = number_of_invoice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(double total_cost) {
        this.total_cost = total_cost;
    }

    public int getNumber_of_invoice() {
        return number_of_invoice;
    }

    public void setNumber_of_invoice(int number_of_invoice) {
        this.number_of_invoice = number_of_invoice;
    }
}
