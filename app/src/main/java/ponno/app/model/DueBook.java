package ponno.app.model;

/**
 * Created by iGeorge on 7/2/18.
 */

public class DueBook {
    private String id;
    private String name;
    private String mobile;
    private double paymentDue;

    public DueBook() {
    }

    public DueBook(String id, String name, String mobile, double paymentDue) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.paymentDue = paymentDue;
    }

    /** Start from getting sale on due.
     * Create table duebook with column name, mobile, mobile as primary
     * insert in duebook when a sale with due is created
     * **/


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public double getPaymentDue() {
        return paymentDue;
    }

    public void setPaymentDue(double paymentDue) {
        this.paymentDue = paymentDue;
    }
}
