package ponno.app.model;

/**
 * Created by iGeorge on 9/21/17.
 */

public class Product {
    private String id;
    private String category_id;
    private String vendor_id;
    private String medicine_package_id;
    private String medicine_dosage_id;
    private String medicine_strength_id;
    private String medicine_id;
    private String category;
    private String company_code;
    private String company;
    private String dosage_code;
    private String dosage;
    private String package_code;
    private String package_size;
    private String strength;
    private String generic_name;
    private String medicine_name;
    private String dar_no;

    private String quantity;
    private String price;
    private String unit_price;
    private int purchase_count;

    public Product() {
    }

    public Product(String generic_name, String unit_price) {
        this.generic_name = generic_name;
        this.unit_price = unit_price;
    }

    public Product(String company_code, String company, String medicine_name, String generic_name, String strength, String dosage) {
        this.company_code = company_code;
        this.company = company;
        this.medicine_name = medicine_name;
        this.generic_name = generic_name;
        this.strength = strength;
        this.dosage = dosage;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getMedicine_package_id() {
        return medicine_package_id;
    }

    public void setMedicine_package_id(String medicine_package_id) {
        this.medicine_package_id = medicine_package_id;
    }

    public String getMedicine_dosage_id() {
        return medicine_dosage_id;
    }

    public void setMedicine_dosage_id(String medicine_dosage_id) {
        this.medicine_dosage_id = medicine_dosage_id;
    }

    public String getMedicine_strength_id() {
        return medicine_strength_id;
    }

    public void setMedicine_strength_id(String medicine_strength_id) {
        this.medicine_strength_id = medicine_strength_id;
    }

    public String getMedicine_id() {
        return medicine_id;
    }

    public void setMedicine_id(String medicine_id) {
        this.medicine_id = medicine_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDosage_code() {
        return dosage_code;
    }

    public void setDosage_code(String dosage_code) {
        this.dosage_code = dosage_code;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getPackage_code() {
        return package_code;
    }

    public void setPackage_code(String package_code) {
        this.package_code = package_code;
    }

    public String getPackage_size() {
        return package_size;
    }

    public void setPackage_size(String package_size) {
        this.package_size = package_size;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getGeneric_name() {
        return generic_name;
    }

    public void setGeneric_name(String generic_name) {
        this.generic_name = generic_name;
    }

    public String getMedicine_name() {
        return medicine_name;
    }

    public void setMedicine_name(String medicine_name) {
        this.medicine_name = medicine_name;
    }

    public String getDar_no() {
        return dar_no;
    }

    public void setDar_no(String dar_no) {
        this.dar_no = dar_no;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public int getPurchase_count() {
        return purchase_count;
    }

    public void setPurchase_count(int purchase_count) {
        this.purchase_count = purchase_count;
    }
}
