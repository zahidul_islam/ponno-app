package ponno.app.base;

import android.app.Application;

import ponno.app.utils.TinyDB;

/**
 * Created by iGeorge on 9/21/17.
 */

public class PonnoApplication extends Application {
    public static final String TAG = PonnoApplication.class.getSimpleName();

    private static PonnoApplication PonnoApplication;

    @Override
    public void onCreate() {
        super.onCreate();

        PonnoApplication = this;
    }

    public static PonnoApplication getApplication(){
        return PonnoApplication;
    }

//    public TinyDB getTinyDB(){
//        return new TinyDB(getApplicationContext());
//    }
}
