package ponno.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.adapters.FragmentViewPagerAdapter;
import ponno.app.customcontrols.SwipeControlledViewPager;
import ponno.app.model.Sale;
import ponno.app.presenter.InvoicePresenter;
import ponno.app.presenter.InvoicePresenterImpl;
import ponno.app.utils.ApplicationUtils;
import ponno.app.view.InvoiceView;

public class InvoiceActivity extends AppCompatActivity implements InvoiceView {
    @BindView(R.id.tv_total_cost)
    TextView tv_total_cost;
    @BindView(R.id.tv_invoice_number)
    TextView tv_invoice_number;
    @BindView(R.id.tv_total_paid)
    TextView tv_total_paid;
    @BindView(R.id.tv_total_due)
    TextView tv_total_due;
    @BindView(R.id.tv_cash_back)
    TextView tv_cash_back;
    @BindView(R.id.rl_invoice_detail)
    RelativeLayout rl_invoice_detail;
    @BindView(R.id.tl_invoice)
    TabLayout tl_invoice;
    @BindView(R.id.vp_invoice)
    SwipeControlledViewPager vp_invoice;

    private Context context;
    private InvoicePresenter invoicePresenter;
    private String invoiceId;

    private ProgressDialog progressDialog;

    public static final String INVOICE_ID = "invoice_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        ButterKnife.bind(this);
        context = this;
        invoiceId = getIntent().getStringExtra(INVOICE_ID);
        invoicePresenter = new InvoicePresenterImpl(context, this, invoiceId, getSupportFragmentManager());
        invoicePresenter.initializePresenter();
    }

    @Override
    public void initUI() {
        tl_invoice.setupWithViewPager(vp_invoice);
        tv_invoice_number.setText(invoiceId);
    }

    @Override
    public void initActionbar() {

    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setSale(Sale sale, FragmentViewPagerAdapter fragmentViewPagerAdapter) {
        vp_invoice.setAdapter(fragmentViewPagerAdapter);
        if (sale.getDiscountedCost() > 0)
            tv_total_cost.setText(getString(R.string.total_cost) + " " + ApplicationUtils.currencyFormat(sale.getDiscountedCost()));
        else
            tv_total_cost.setText(getString(R.string.total_cost) + " " + ApplicationUtils.currencyFormat(sale.getTotalCost()));
        tv_total_paid.setText(getString(R.string.total_paid) + " " + ApplicationUtils.currencyFormat(sale.getCashPaid()));
        tv_total_due.setText(getString(R.string.total_due) + " " + ApplicationUtils.currencyFormat(sale.getPaymentDue()));
        tv_cash_back.setText(getString(R.string.cash_back) + " " + ApplicationUtils.currencyFormat(sale.getCashBack()));
        if (sale.getPaymentDue() > 0) {
            rl_invoice_detail.setBackgroundColor(Color.parseColor("#FFF4F4"));
        } else {
            rl_invoice_detail.setBackgroundColor(Color.parseColor("#EFEFEF"));
        }
    }

    @Override
    public void updateSaleInfo(double totalCost, double total_paid, double total_due) {
        tv_total_cost.setText(getString(R.string.total_cost) + " " + ApplicationUtils.currencyFormat(totalCost));
        tv_total_paid.setText(getString(R.string.total_paid) + " " + ApplicationUtils.currencyFormat(total_paid));
        tv_total_due.setText(getString(R.string.total_due) + " " + ApplicationUtils.currencyFormat(total_due));
        if (total_paid > totalCost)
            tv_cash_back.setText(getString(R.string.cash_back) + " " + ApplicationUtils.currencyFormat(total_paid - totalCost));
        if (total_due > 0) {
            rl_invoice_detail.setBackgroundColor(Color.parseColor("#FFF4F4"));
        } else {
            rl_invoice_detail.setBackgroundColor(Color.parseColor("#EFEFEF"));
        }
    }

    @Override
    public void updateTotalCost(double totalCost) {
        tv_total_cost.setText(getString(R.string.total_cost) + " " + ApplicationUtils.currencyFormat(totalCost));
    }

    @Override
    public void completeInvoice() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void openDueForm() {
        vp_invoice.setCurrentItem(1, true);
        vp_invoice.setPagingEnabled(false);
        tl_invoice.setEnabled(false);
    }

    @Override
    public void showProgress(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && context != null)
            progressDialog.dismiss();
    }
}
