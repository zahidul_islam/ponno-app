package ponno.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import ponno.app.adapters.ProductAutoCompleteAdapter;
import ponno.app.customcontrols.InstantAutoComplete;
import ponno.app.model.Product;
import ponno.app.presenter.HomePresenter;
import ponno.app.presenter.HomePresenterImpl;
import ponno.app.view.HomeView;

import static android.view.View.INVISIBLE;

public class HomeActivity extends AppCompatActivity implements HomeView {
    private Context context;
    private HomePresenter homePresenter;

    @BindView(R.id.dl_main)
    DrawerLayout dl_main;
    @BindView(R.id.nv_drawer)
    NavigationView nvDrawer;
    ActionBarDrawerToggle mDrawerToggle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_cancel_selection)
    ImageView iv_cancel_selection;
    @BindView(R.id.auto_complete_search)
    InstantAutoComplete auto_complete_search;
    @BindView(R.id.fab_start_sale)
    FloatingActionButton fab_start_sale;

    RelativeLayout rlNavHeaderLoginContent, rlNavHeaderUserContent;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        context = this;
        homePresenter = new HomePresenterImpl(context, this, getSupportFragmentManager());
        homePresenter.initializePresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void initUI() {
        auto_complete_search.setThreshold(3);
        mDrawerToggle = setupDrawerToggle();
        mDrawerToggle.syncState();
        dl_main.setDrawerListener(mDrawerToggle);
        nvDrawer.setItemIconTintList(null);
        View headerView = nvDrawer.getHeaderView(0);
        rlNavHeaderLoginContent = headerView.findViewById(R.id.rlNavHeaderLoginContent);
        rlNavHeaderUserContent = headerView.findViewById(R.id.rlNavHeaderUserContent);
        setupDrawerHeader();
    }

    public void setupDrawerHeader() {
        rlNavHeaderLoginContent.setVisibility(View.VISIBLE);
        rlNavHeaderUserContent.setVisibility(View.GONE);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, dl_main, toolbar, R.string.txt_nav_open, R.string.txt_nav_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void initActionbar() {
        setSupportActionBar(toolbar);
    }

    @Override
    public void setListeners() {
        iv_cancel_selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auto_complete_search.invalidate();
                auto_complete_search.setText("");
            }
        });
        auto_complete_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                checkSearchProgress(true);
                dl_main.closeDrawers();
            }

            @Override
            public void onTextChanged(CharSequence query, int start, int before, int count) {
                if (query != null && query.length() > 0 && iv_cancel_selection.getVisibility() == INVISIBLE) {
                    iv_cancel_selection.setVisibility(View.VISIBLE);
                    homePresenter.searchInvoice(query);
                } else if (query == null || query.length() == 0) {
                    iv_cancel_selection.setVisibility(INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        auto_complete_search.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                homePresenter.productSelected((Product) parent.getItemAtPosition(position));
//                auto_complete_search.setText("");
            }
        });
        fab_start_sale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Start Sale
                Intent intent = new Intent(context, SaleActivity.class);
                startActivity(intent);
            }
        });
        nvDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectDrawerItem(item);
                return true;
            }
        });
    }

    private void selectDrawerItem(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_due_book:
                homePresenter.openDueBook();
                dl_main.closeDrawers();
                break;
            default:
                break;
        }
    }

    @Override
    public void setAutoCompleteAdapter(ArrayAdapter<String> autoCompleteAdapter) {
        auto_complete_search.setAdapter(new ProductAutoCompleteAdapter(context, new ArrayList<Product>()));
    }

    @Override
    public void checkSearchProgress(boolean showProgress) {
        if (showProgress) {
            iv_cancel_selection.setImageResource(R.drawable.progress_animation);
        } else {
            iv_cancel_selection.setImageResource(R.drawable.ic_action_cancel_selector);
        }
    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == INVOICE_ACTIVITY_RESULT_CODE) {
//            if (resultCode == RESULT_OK) {
//                if (homePresenter != null) {
//                    homePresenter.removeInvoice();
//                }
//            }
//        }
//    }

    //    @Override
//    public void openSelectionFragment(Product product) {
//        fl_container.setVisibility(View.VISIBLE);
//        ApplicationUtils.addFragmentToActivity(getSupportFragmentManager(), ProductSelectionFragment.newInstance(product), R.id.fl_container);
//    }

    @Override
    public void showProgress(final String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    @Override
    public void hideProgress() {
        if (progressDialog != null && context != null)
            progressDialog.dismiss();
    }

}
