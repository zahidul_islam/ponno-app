package ponno.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.model.Product;
import ponno.app.presenter.SalePresenter;
import ponno.app.utils.ApplicationUtils;
import ponno.app.view.SaleFragmentView;

/**
 * Created by Zahidul_Islam_George on 27-Apr-16.
 */
public class ProductSaleAdapter extends RecyclerView.Adapter<ProductSaleAdapter.ProductSaleViewHolder> {

    private ArrayList<Product> products;
    private ArrayList<Product> itemsPendingRemoval;

    private Context context;
    private SalePresenter salePresenter;

    public ProductSaleAdapter(ArrayList<Product> products, Context context, SalePresenter salePresenter) {
        this.products = products;
        itemsPendingRemoval = new ArrayList<>();
        this.context = context;
        this.salePresenter = salePresenter;
    }

    @Override
    public ProductSaleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_product_sale_row, parent, false);
        return new ProductSaleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProductSaleViewHolder productSaleViewHolder, final int position) {
        final Product product = products.get(position);
        if (itemsPendingRemoval.contains(product)) {
            productSaleViewHolder.ll_product_info.setVisibility(View.GONE);
            productSaleViewHolder.rl_delete.setVisibility(View.VISIBLE);
            productSaleViewHolder.tv_delete.setVisibility(View.VISIBLE);
        } else {
            productSaleViewHolder.ll_product_info.setVisibility(View.VISIBLE);
            productSaleViewHolder.rl_delete.setVisibility(View.GONE);
            productSaleViewHolder.tv_delete.setVisibility(View.GONE);
            productSaleViewHolder.tv_product_name.setText(product.getMedicine_name() + " - " + product.getStrength());
            productSaleViewHolder.tv_unit_price.setText(ApplicationUtils.currencyFormat(ApplicationUtils.convertToDouble(product.getUnit_price())));
            if (product.getQuantity() != null) {
                productSaleViewHolder.tv_quantity.setText(ApplicationUtils.getBengaliNumber(product.getQuantity()));
                productSaleViewHolder.tv_price.setText(getTotalPrice(ApplicationUtils.convertToInt(product.getQuantity()), ApplicationUtils.convertToDouble(product.getUnit_price())));
            } else {
                productSaleViewHolder.tv_quantity.setText("1");
                productSaleViewHolder.tv_price.setText(getTotalPrice(1, ApplicationUtils.convertToDouble(product.getUnit_price())));
            }
        }

//                productSaleViewHolder.tv_dosage.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        salePresenter.setDosage(product, position);
//                    }
//                });
//
//                productSaleViewHolder.tv_strength.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        salePresenter.setStrength(product, position);
//                    }
//                });
//
//                productSaleViewHolder.tv_total_sale.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        salePresenter.openPriceQuantityDialog(product, position);
//                    }
//                });
//
//                productSaleViewHolder.tv_invoice_number.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        salePresenter.openPriceQuantityDialog(product, position);
//                    }
//                });

        productSaleViewHolder.iv_quantity_dec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = ApplicationUtils.convertToInt(product.getQuantity());
                if (quantity > 1) {
                    quantity = quantity - 1;
                    product.setQuantity(quantity + "");
                    salePresenter.calculateAndSetTotalCost();
                    notifyItemChanged(position);
                } else if (quantity == 1) {
//                            salePresenter.removeProductFromSale(product);
                    remove(position);
                }
            }
        });

        productSaleViewHolder.iv_quantity_inc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = 1;
                if (product.getQuantity() != null) {
                    quantity = ApplicationUtils.convertToInt(product.getQuantity());
                }
                quantity++;
                product.setQuantity(quantity + "");
                salePresenter.calculateAndSetTotalCost();
                notifyItemChanged(position);
            }
        });

        productSaleViewHolder.ll_product_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salePresenter.openQuantityAndPriceSelectionDialog(product);
            }
        });

        productSaleViewHolder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                        salePresenter.removeProductFromSale(product);
                remove(position);
            }
        });
        productSaleViewHolder.tv_undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelOperation(product);
            }
        });

    }

    private void cancelOperation(Product product) {
        itemsPendingRemoval.remove(product);
        // this will rebind the row in "normal" state
        notifyItemChanged(products.indexOf(product));
    }

    public void remove(int position) {
        Product product = products.get(position);
        if (itemsPendingRemoval.contains(product)) {
            itemsPendingRemoval.remove(product);
        }
        if (products.contains(product)) {
            products.remove(product);
            if (products == null || products.size() == 0) {
                salePresenter.removeInvoice();
            } else {
                salePresenter.calculateAndSetTotalCost();
            }
            notifyItemRemoved(position);
            notifyDataSetChanged();
        }
    }

    public void pendingRemoval(final int position) {
        final Product product = products.get(position);
        if (!itemsPendingRemoval.contains(product)) {
            itemsPendingRemoval.add(product);
            notifyItemChanged(position);
        }
    }

    public boolean isPendingRemoval(int position) {
        Product product = products.get(position);
        return itemsPendingRemoval.contains(product);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public String getTotalPrice(int quantity, double unitPrice) {
        double total = quantity * unitPrice;
        return ApplicationUtils.currencyFormat(total);
    }

    public static class ProductSaleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_product_name)
        TextView tv_product_name;
        @BindView(R.id.tv_quantity)
        TextView tv_quantity;
        @BindView(R.id.tv_price)
        TextView tv_price;
        @BindView(R.id.tv_unit_price)
        TextView tv_unit_price;
        @BindView(R.id.tv_delete)
        TextView tv_delete;
        @BindView(R.id.tv_undo)
        TextView tv_undo;
        @BindView(R.id.iv_quantity_dec)
        ImageView iv_quantity_dec;
        @BindView(R.id.iv_quantity_inc)
        ImageView iv_quantity_inc;
        @BindView(R.id.ll_product_info)
        LinearLayout ll_product_info;
        @BindView(R.id.rl_delete)
        RelativeLayout rl_delete;

        public ProductSaleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
