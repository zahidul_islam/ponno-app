package ponno.app.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import ponno.app.R;
import ponno.app.SaleActivity;
import ponno.app.data.local.DatabaseHelper;
import ponno.app.model.Product;
import ponno.app.view.SaleView;

/**
 * Created by iGeorge on 9/24/17.
 */

public class ProductAutoCompleteAdapter extends ArrayAdapter<Product> {
    private ArrayList<Product> suggestions;
    private DatabaseHelper databaseHelper;
    private SaleView saleView;
    private Context context;

    public ProductAutoCompleteAdapter(@NonNull Context context, @NonNull ArrayList<Product> products) {
        super(context, R.layout.autocomplete_dropdown_item, products);
        databaseHelper = DatabaseHelper.getInstance(context);
        this.suggestions = new ArrayList<>(products);
        this.context = context;
        saleView = (SaleView) context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Product product = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.autocomplete_dropdown_item, parent, false);
        }
        TextView tv_product_name = convertView.findViewById(R.id.tv_product_name);
        TextView tv_product_info = convertView.findViewById(R.id.tv_product_info);
        if (tv_product_name != null && tv_product_info != null && product != null) {
            tv_product_name.setText(product.getMedicine_name());
            tv_product_info.setText(product.getGeneric_name() + "-" + product.getStrength() + "-" + product.getDosage() + "\n" + product.getCompany());
        }

        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return productFilter;
    }

    Filter productFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            Product product = (Product) resultValue;
            return product.getMedicine_name() + "-" + product.getGeneric_name() + "-" + product.getStrength() + "-" + product.getDosage() + "-" + product.getCompany();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                final String searchKey = constraint.toString().toLowerCase();
                suggestions.clear();
                final FilterResults filterResults = new FilterResults();
                if (searchKey.length() > 2) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            suggestions = databaseHelper.getProductsByKey(searchKey);
//                            suggestions = searchProductFromCSV(searchKey);
                            ((SaleActivity) saleView).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    filterResults.values = suggestions;
                                    filterResults.count = suggestions.size();
                                    clear();
                                    for (Product prod : suggestions) {
                                        add(prod);
                                        notifyDataSetChanged();
                                    }
                                    saleView.checkSearchProgress(false);
                                }
                            });
                        }
                    }).start();
                }
                /*
                else if (searchKey.length() > 0 && searchKey.length() % 2 == 1) {
                    suggestions = tempProducts;
                }
                for (Product product : tempProducts) {
                    if (product.getMedicine_code().toLowerCase().startsWith(constraint.toString().toLowerCase()) || product.getCompany().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(product);
                    }
                }*/
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        class SearchProductTask extends AsyncTask<CharSequence, Void, ArrayList<Product>> {

            @Override
            protected ArrayList<Product> doInBackground(CharSequence... charSequences) {
                String queryText = charSequences[0].toString();
                return databaseHelper.getProductsByKey(queryText);
            }

            @Override
            protected void onPostExecute(ArrayList<Product> sales) {
                super.onPostExecute(sales);
                clear();
                addAll(suggestions);
//                                    for (Product prod : suggestions) {
//                                        add(prod);
//                                    }
                notifyDataSetChanged();
                saleView.checkSearchProgress(false);
            }
        }

        public ArrayList<Product> searchProductFromCSV(String key) {
            try {
                ArrayList<Product> searchResults = new ArrayList<>();
                InputStream is = context.getAssets().open("drug_list_edited.csv");
                BufferedReader buffer = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                while (true) {
                    if (buffer.readLine() == null) {
                        break;
                    }
                    String line = buffer.readLine();
                    if (!TextUtils.isEmpty(line)) {
                        try {
                            line = line.replace("\"", "").replace("'", "''");
                            String[] str = line.split(",");
                            if (str[1].startsWith(key) || str[2].startsWith(key) || str[3].startsWith(key)) {
                                Product product = new Product(str[0], str[1], str[2], str[3], str[4], str[5]);
                                searchResults.add(product);
                            }
                        } catch (ArrayIndexOutOfBoundsException e) {
                            //nothing
                        }
                    }
                }
                return searchResults;
            } catch (IOException e) {
                e.printStackTrace();
                return new ArrayList<>();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.count > 0) {
//                saleView.checkSearchProgress(false);
//                clear();
//                for (Product prod : suggestions) {
//                    add(prod);
//                    notifyDataSetChanged();
//                }
            }
        }

    };
}
