package ponno.app.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.model.Sale;
import ponno.app.presenter.HomePresenter;
import ponno.app.utils.ApplicationUtils;

/**
 * Created by Zahidul_Islam_George on 27-Apr-16.
 */
public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.SalesViewHolder> {

    private ArrayList<Sale> sales;
    private Context context;
    private HomePresenter homePresenter;

    public SalesAdapter(ArrayList<Sale> sales, Context context, HomePresenter homePresenter) {
        this.sales = sales;
        this.context = context;
        this.homePresenter = homePresenter;
    }

    @Override
    public SalesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.sale_row, parent, false);
        return new SalesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SalesViewHolder salesViewHolder, final int position) {
        final Sale sale = sales.get(position);
        salesViewHolder.tv_date.setText(ApplicationUtils.getTimeInBengali(sale.getDate()));
        salesViewHolder.tv_invoice_number.setText(sale.getInvoiceID());
        if (sale.getDiscount() == null)
            salesViewHolder.tv_total_sale.setText(ApplicationUtils.currencyFormat(sale.getTotalCost()));
        else
            salesViewHolder.tv_total_sale.setText(ApplicationUtils.currencyFormat(sale.getDiscountedCost()));
        if (sale.getStatus().equalsIgnoreCase(context.getString(R.string.sale_on_due))) {
            salesViewHolder.tv_total_due.setVisibility(View.VISIBLE);
            salesViewHolder.tv_total_sale.setVisibility(View.GONE);
            salesViewHolder.tv_total_due.setText(context.getString(R.string.due) + ": " + ApplicationUtils.currencyFormat(sale.getPaymentDue()));
            salesViewHolder.cb_container.setCardBackgroundColor(Color.parseColor("#FFF4F4"));
        } else {
            salesViewHolder.tv_total_due.setVisibility(View.GONE);
            salesViewHolder.tv_total_sale.setVisibility(View.VISIBLE);
            salesViewHolder.cb_container.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        salesViewHolder.cb_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.viewSales(sale);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sales.size();
    }

    public static class SalesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.tv_invoice_number)
        TextView tv_invoice_number;
        @BindView(R.id.tv_total_sale)
        TextView tv_total_sale;
        @BindView(R.id.tv_total_due)
        TextView tv_total_due;
        @BindView(R.id.cb_container)
        CardView cb_container;

        public SalesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
