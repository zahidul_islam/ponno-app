package ponno.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import ponno.app.R;

/**
 * Created by Zahidul_Islam_George on 27-Apr-16.
 */
public class RadioButtonAdapter extends RecyclerView.Adapter<RadioButtonAdapter.RadioButtonViewHolder> {

    private ArrayList<String> options;
    private String key;

    private int checkedPosition = -1;

    public RadioButtonAdapter(ArrayList<String> options) {
        this.options = options;
    }

    @Override
    public RadioButtonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_option_row, parent, false);
        return new RadioButtonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RadioButtonViewHolder holder, final int position) {
        if (options != null) {
            holder.rb_option.setText(options.get(position));
            if (checkedPosition != -1 && position == checkedPosition) {
                holder.rb_option.setChecked(true);
            } else {
                holder.rb_option.setChecked(false);
            }
            holder.layout_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkedPosition = position;
                    notifyDataSetChanged();
//                    if(productSelectionView == null) {
//                        radioOptionView.optionSelected(options.get(position));
//                    }else{
//                        productSelectionView.optionSelected(options.get(position), key);
//                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (options != null) {
            return options.size();
        } else {
            return 0;
        }
    }

    public static class RadioButtonViewHolder extends RecyclerView.ViewHolder {

        protected RadioButton rb_option;
        protected RelativeLayout layout_container;

        public RadioButtonViewHolder(View itemView) {
            super(itemView);
            rb_option = (RadioButton) itemView.findViewById(R.id.rb_option);
            layout_container = (RelativeLayout) itemView.findViewById(R.id.layout_container);
        }
    }
}
