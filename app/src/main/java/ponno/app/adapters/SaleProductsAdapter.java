package ponno.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.model.Product;
import ponno.app.utils.ApplicationUtils;

/**
 * Created by Zahidul_Islam_George on 27-Apr-16.
 */
public class SaleProductsAdapter extends RecyclerView.Adapter<SaleProductsAdapter.SaleProductsViewHolder> {

    private ArrayList<Product> products;

    private Context context;

    public SaleProductsAdapter(ArrayList<Product> products, Context context) {
        this.products = products;
        this.context = context;
    }

    @Override
    public SaleProductsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_sale_products_row, parent, false);
        return new SaleProductsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SaleProductsViewHolder saleProductsViewHolder, final int position) {
        final Product product = products.get(position);
        saleProductsViewHolder.tv_quantity.setText(context.getString(R.string.quantity) + ": " + ApplicationUtils.getBengaliNumber(product.getQuantity()));
        saleProductsViewHolder.tv_total_cost.setText(context.getString(R.string.total) + ": " + getTotalPrice(ApplicationUtils.convertToInt(product.getQuantity()), ApplicationUtils.convertToDouble(product.getUnit_price())));
        saleProductsViewHolder.tv_product_name.setText(product.getMedicine_name() + " - " + product.getStrength());
        saleProductsViewHolder.tv_unit_price.setText(context.getString(R.string.unit_price) + ": " + ApplicationUtils.currencyFormat(ApplicationUtils.convertToDouble(product.getUnit_price())));

    }

    public String getTotalPrice(int quantity, double unitPrice) {
        double total = quantity * unitPrice;
        return ApplicationUtils.currencyFormat(total);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class SaleProductsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_product_name)
        TextView tv_product_name;
        @BindView(R.id.tv_quantity)
        TextView tv_quantity;
        @BindView(R.id.tv_total_cost)
        TextView tv_total_cost;
        @BindView(R.id.tv_unit_price)
        TextView tv_unit_price;

        public SaleProductsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
