package ponno.app.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.model.Product;
import ponno.app.model.Sale;
import ponno.app.presenter.SalePresenter;

/**
 * Created by Zahidul_Islam_George on 27-Apr-16.
 */
public class SuggestionsAdapter extends RecyclerView.Adapter<SuggestionsAdapter.SuggestionsViewHolder> {

    private ArrayList<Product> products;
    private SalePresenter salePresenter;

    public SuggestionsAdapter(ArrayList<Product> products, SalePresenter salePresenter) {
        this.products = products;
        this.salePresenter = salePresenter;
    }

    @Override
    public SuggestionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.suggestion_row, parent, false);
        return new SuggestionsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SuggestionsViewHolder suggestionsViewHolder, final int position) {
        final Product product = products.get(position);
        suggestionsViewHolder.tv_product_name.setText(product.getMedicine_name());
        suggestionsViewHolder.tv_product_info.setText(product.getStrength() + "-" + product.getDosage() + "\n" + product.getCompany());

        suggestionsViewHolder.cb_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salePresenter.productSelected(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class SuggestionsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_product_name) TextView tv_product_name;
        @BindView(R.id.tv_product_info) TextView tv_product_info;
        @BindView(R.id.cb_container) CardView cb_container;

        public SuggestionsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
