package ponno.app.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.model.DailySale;
import ponno.app.presenter.HomePresenter;
import ponno.app.utils.ApplicationUtils;

/**
 * Created by Zahidul_Islam_George on 27-Apr-16.
 */
public class DailySalesAdapter extends RecyclerView.Adapter<DailySalesAdapter.SalesViewHolder> {

    private ArrayList<DailySale> dailySales;
    private Context context;
    private HomePresenter homePresenter;

    public DailySalesAdapter(ArrayList<DailySale> dailySales, Context context, HomePresenter homePresenter) {
        this.dailySales = dailySales;
        this.context = context;
        this.homePresenter = homePresenter;
    }

    @Override
    public SalesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.daily_sale_row, parent, false);
        return new SalesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SalesViewHolder salesViewHolder, final int position) {
        final DailySale dailySale = dailySales.get(position);
        salesViewHolder.tv_date.setText(ApplicationUtils.getDateInBengali(dailySale.getDate()+" 00:00:00"));
        salesViewHolder.tv_invoice_number.setText(context.getString(R.string.number_of_invoice) + ": " + dailySale.getNumber_of_invoice() + "");
        salesViewHolder.tv_total_sale.setText(ApplicationUtils.currencyFormat(dailySale.getTotal_cost()));

        salesViewHolder.cb_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.showSaleAt(dailySale.getDate() + " 00:00:00");
            }
        });
    }

    @Override
    public int getItemCount() {
        return dailySales.size();
    }

    public static class SalesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.tv_invoice_number)
        TextView tv_invoice_number;
        @BindView(R.id.tv_total_sale)
        TextView tv_total_sale;
        @BindView(R.id.cb_container)
        CardView cb_container;

        public SalesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
