package ponno.app.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ponno.app.R;
import ponno.app.model.DueBook;
import ponno.app.presenter.HomePresenter;
import ponno.app.utils.ApplicationUtils;

/**
 * Created by Zahidul_Islam_George on 27-Apr-16.
 */
public class DueBookAdapter extends RecyclerView.Adapter<DueBookAdapter.DueBookViewHolder> {

    private ArrayList<DueBook> dueBooks;
    private Context context;
    private HomePresenter homePresenter;

    public DueBookAdapter(ArrayList<DueBook> dueBooks, Context context, HomePresenter homePresenter) {
        this.dueBooks = dueBooks;
        this.context = context;
        this.homePresenter = homePresenter;
    }

    @Override
    public DueBookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.due_book_row, parent, false);
        return new DueBookViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DueBookViewHolder dueBookViewHolder, final int position) {
        final DueBook dueBook = dueBooks.get(position);
        dueBookViewHolder.tv_customer_name.setText(processName(dueBook.getName()));
        dueBookViewHolder.tv_mobile_number.setText(processMobileNumber(dueBook.getMobile()));
        dueBookViewHolder.tv_total_due.setText(ApplicationUtils.currencyFormat(dueBook.getPaymentDue()));

        dueBookViewHolder.cb_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePresenter.showDues(dueBook);
            }
        });
    }

    private String processName(String name){
        if(name != null && !TextUtils.isEmpty(name)){
            return name;
        }else{
            return context.getString(R.string.anonymous);
        }
    }

    private String processMobileNumber(String mobile) {
        if(mobile!=null && !TextUtils.isEmpty(mobile)){
            if(mobile.contains("+88")||mobile.contains("0088")){
                return mobile;
            }else{
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a", ApplicationUtils.getBangleLocale());
                    return dateFormat.format(Long.parseLong(mobile));
                }catch (Exception exception){
                    return context.getString(R.string.anonymous);
                }
            }
        }else {
            return context.getString(R.string.anonymous);
        }
    }

    @Override
    public int getItemCount() {
        return dueBooks.size();
    }

    public static class DueBookViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_customer_name) TextView tv_customer_name;
        @BindView(R.id.tv_mobile_number) TextView tv_mobile_number;
        @BindView(R.id.tv_total_due) TextView tv_total_due;
        @BindView(R.id.cb_container) CardView cb_container;

        public DueBookViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
