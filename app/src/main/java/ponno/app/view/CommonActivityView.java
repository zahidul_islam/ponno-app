package ponno.app.view;

/**
 * Created by Fahim Ahmed on 11-12-16.
 */

public interface CommonActivityView {
    void initUI();

    void initActionbar();

    void setListeners();
}
