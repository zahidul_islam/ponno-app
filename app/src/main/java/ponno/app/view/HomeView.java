package ponno.app.view;

import android.widget.ArrayAdapter;

import ponno.app.adapters.ProductAutoCompleteAdapter;
import ponno.app.adapters.ProductSaleAdapter;
import ponno.app.utils.SwipeListener;

/**
 * Created by iGeorge on 9/21/17.
 */

public interface HomeView extends CommonActivityView, ProgressView {

    void setAutoCompleteAdapter(ArrayAdapter<String> autoCompleteAdapter);

    void checkSearchProgress(boolean showProgress);
}
