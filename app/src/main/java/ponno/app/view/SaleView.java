package ponno.app.view;

import java.util.ArrayList;

import ponno.app.adapters.ProductAutoCompleteAdapter;
import ponno.app.adapters.ProductSaleAdapter;
import ponno.app.adapters.SuggestionsAdapter;
import ponno.app.model.Product;
import ponno.app.utils.SwipeListener;

/**
 * Created by iGeorge on 9/21/17.
 */

public interface SaleView extends CommonActivityView, ProgressView, SwipeListener {

    void setAutoCompleteAdapter(ProductAutoCompleteAdapter autoCompleteAdapter);

    void gotoInvoice(String invoiceNumber);

    void checkSearchProgress(boolean showProgress);

    void showSuggestions(boolean show);

    void setSuggestions(ArrayList<Product> recentSales, ArrayList<Product> mostPurchased);

    void setSaleAdapter(ProductSaleAdapter productSaleAdapter);

    void updateTotalCost(String totalCost);
}
