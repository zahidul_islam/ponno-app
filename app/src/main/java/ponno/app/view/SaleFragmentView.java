package ponno.app.view;

import ponno.app.model.Product;
import ponno.app.utils.SwipeListener;

/**
 * Created by iGeorge on 22/12/17.
 */

public interface SaleFragmentView extends SwipeListener {
    void openQuantityAndPriceSelectionDialog(Product product);

    void addProduct(Product product);

    void calculateAndSetTotalCost();

    void removeInvoice();
}
