package ponno.app.view;

/**
 * Created by iGeorge on 9/24/17.
 */

public interface ProgressView {
    void showProgress(String message);

    void hideProgress();
}
