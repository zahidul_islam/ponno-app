package ponno.app.view;

import ponno.app.adapters.FragmentViewPagerAdapter;
import ponno.app.adapters.ProductAutoCompleteAdapter;
import ponno.app.adapters.ProductSaleAdapter;
import ponno.app.model.Sale;
import ponno.app.utils.SwipeListener;

/**
 * Created by iGeorge on 9/21/17.
 */

public interface InvoiceView extends CommonActivityView, ProgressView {

    void setSale(Sale sale, FragmentViewPagerAdapter fragmentViewPagerAdapter);

    void updateSaleInfo(double totalCost, double total_paid, double total_due);

    void updateTotalCost(double totalCost);

    void completeInvoice();

    void openDueForm();
}
