package ponno.app.view;

import android.view.View;

/**
 * Created by Zahidul_Islam_George on 08-November-2016.
 */
public interface CommonFragmentView {
    void initUI(View view);

    void setListeners();
}
